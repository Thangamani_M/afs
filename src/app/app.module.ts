import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AsyncPipe, DatePipe, HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './modules/login/login.component';
import { SignUpComponent } from './modules/sign-up/sign-up.component';
import { EncrDecrService } from './services/common/enc-dec.service';
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { AuthLayoutComponent } from './shared/layout/auth-layout/auth-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpInterceptorService } from './services/common/http-interceptor.service';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ToastrModule } from 'ngx-toastr';
import { JwtInterceptor } from './services/common/jwt.interceptor';
import { PreLoaderComponent } from './shared/components/pre-loader/pre-loader.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { ForgotPasswordComponent } from './modules/forgot-password/forgot-password.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ActivitiesComponent } from './modules/activities/activities.component';
import { ProfileComponent } from './modules/profile/profile.component';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';

// import { initializeApp } from "firebase/app";

// import { getAnalytics } from "firebase/analytics";
// import { environment } from '../environments/environment';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { NgxOtpInputModule } from 'ngx-otp-input';
import { FeedbackComponent } from './modules/feedback/feedback.component';
import { FaqComponent } from './modules/faq/faq.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ChatOrCallComponent } from './modules/chat-or-call/chat-or-call.component';
import { VerifySignUpComponent } from './modules/sign-up/verify-signup/verify-signup.component';
import { VerifForgotComponent } from './modules/forgot-password/verif-forgot/verif-forgot.component';
import { FileTypeValidator } from './services/common/filetype-validation';
import { MatPaginatorModule } from '@angular/material/paginator';
import { environment } from 'src/environments/environment';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireModule } from '@angular/fire/compat';
import { MessagingService } from './services/messaging.service';
import { initializeApp } from "firebase/app";
import { getMessaging } from "firebase/messaging/sw";
import { CommonService } from './services/common/common.service';
import { OffersComponent } from './modules/offers/offers.component';
import { MarketinginfoComponent } from './modules/login/marketingInfo/marketinginfo.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { TermsConditionsComponent } from './modules/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { UserIdleModule } from 'angular-user-idle';
import { GetotherinfoComponent } from './modules/login/getotherinfo/getotherinfo.component';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import {WebcamModule} from 'ngx-webcam';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CaptchaModule } from "primeng/captcha";
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
//import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
// const app = initializeApp(environment.firebaseConfig);
// const analytics = getAnalytics(app);
const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};



// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
const firebaseApp = initializeApp(environment.firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
// const messaging = getMessaging(firebaseApp);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    VerifySignUpComponent,
    NotificationsComponent,
    AuthLayoutComponent,
    PreLoaderComponent,
    PageNotFoundComponent,
    ForgotPasswordComponent,
    VerifForgotComponent,
    ActivitiesComponent,
    ProfileComponent,
    FeedbackComponent,
    FaqComponent,
    ChatOrCallComponent,
    OffersComponent,
    MarketinginfoComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    GetotherinfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    FormsModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxMaskModule.forRoot(maskConfigFunction),
    SocialLoginModule,
    NgDynamicBreadcrumbModule,
    BsDatepickerModule.forRoot(),
    NgxOtpInputModule,
    NgxSkeletonLoaderModule.forRoot(),
    MatPaginatorModule,
    UserIdleModule.forRoot({idle: 900, timeout: 1, ping: 5}),
    // ServiceWorkerModule.register('firebase-messaging-sw.js', {
    //   enabled: environment.production,
    //   registrationStrategy: 'registerImmediately'
    // }),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireMessagingModule,
    NgxIntlTelInputModule,
    AutocompleteLibModule,
    // BsDatepickerModule.forRoot(),
    // MatGoogleMapsAutocompleteModule,
    DatepickerModule.forRoot(),
    GooglePlaceModule,
    WebcamModule,
    NgbModule,
    RecaptchaFormsModule,
    RecaptchaModule,
    CaptchaModule
  ],
  providers: [MessagingService, AsyncPipe, EncrDecrService, CommonService, DatePipe,
    // { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              // '514667599748-kb3jd1leri5m04ttgld98d65987pn6gg.apps.googleusercontent.com'
              '514667599748-pip7j0d4r3r839s5g3u4dilebqtot5bt.apps.googleusercontent.com'
            )
          }, {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(
            //  '295569705344046',
              // '268942215407811'
             // '308384181278283',
             '662973341325433'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }, {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: environment.recaptcha.siteKey,
      } as RecaptchaSettings,
    }, FileTypeValidator
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


