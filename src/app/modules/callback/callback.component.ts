import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit ,OnDestroy{
  paymentstatus;
  transactionId;
  amount
  constructor(private apihttp: ApiHttpService, private modalService: NgbModal, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) {
    console.log('Called Constructor');
    this.route.queryParams.subscribe(params => {
      this.paymentstatus = params['PaymentStatus']
      this.transactionId = params['TransactionId']
      this.amount = params['Amount']
      console.log(params['TransactionId'])
    });
  }
 

  ngOnInit(): void {
    this.route.params.subscribe((paramid: Params) => {
     console.log(paramid['TransactionId'])
    })
    let id =localStorage.getItem('loan-precloser')
    if(id){
      const formData = new FormData();
      formData.append("id", id);
      this.http.post(this.api.PRECLOSURESUCESS, formData).subscribe(
        (res: any) => {
          console.log('Preclosure', res)
          localStorage.removeItem('loan-precloser')
        },
        (error: any) => {
          console.log("Error", error);
          localStorage.removeItem('loan-precloser')
        }
      );
    }
  }
   formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }
  formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.
    if(!input) return ''
    // get input value
    var input_val = input;
    console.log(input)
    // don't validate empty input
    if (input_val === "") { return; }
  
    // check for decimal
    if (input_val.indexOf(".") >= 0) {
      var decimal_pos = input_val.indexOf(".");
      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);
  
      // add commas to left side of number
      left_side = this.formatNumber(left_side);
  
      // validate right side
      right_side = this.formatNumber(right_side);
  
      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side += "00";
      }
  
      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);
  
      // join number by .
      input_val = "$" + left_side + "." + right_side;
  
    } else {
      input_val = this.formatNumber(input_val);
      input_val = "$" + input_val;
  
      // final formatting
      if (blur === "blur") {
        input_val += ".00";
      }
    }
 return input_val
  }

  back(){
    this.router.navigate(['my-loans']);
  }
  private getQueryParameter(key: string) {
    const parameters = new URLSearchParams(window.location.search);
    return parameters.get(key);
  }
  ngOnDestroy(): void {
    localStorage.removeItem('loan-precloser')
  }
  
}
