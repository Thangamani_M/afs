import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SocialAuthService } from 'angularx-social-login';
import { CommonService } from 'src/app/services/common/common.service';
import { UtilityService } from 'src/app/services/common/utility.service';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
  loading = false;
  floading = false;
  allOffers: any;

  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  pageInfo = {
    page: 1,
    offset: 5
  }
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  constructor(private apihttp: ApiHttpService, private commonData: CommonService, private socialAuthService: SocialAuthService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getAllOffers(1);
  }

  getAllOffers(t?) {
    if(t)
       this.loading = true
    if(!t)
      this.floading = true;
    const formData = new FormData();
    formData.append('page', this.pageInfo.page + '')
    formData.append('offset', this.pageInfo.offset + '')
    this.http.post(this.api.ALLOFFERS, formData).subscribe(
      (res: any) => {
        this.loading = false
        this.floading = false;
        this.allOffers = res.data.data;
        console.log('All Offers', this.allOffers)
        
        this.paginationSettings["length"] = res.data.totalCount;
        this.paginationSettings["enablePagination"] = true;
        this.paginationSettings["totalPages"] = res.data?.totalPages;
      },
      (error: any) => {
        this.loading = false
        this.floading = false;
        console.log("Error", error);
      }
    );
  }
  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.getAllOffers();
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
}
