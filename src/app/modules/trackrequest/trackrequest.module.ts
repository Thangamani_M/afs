import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatPaginatorModule } from '@angular/material/paginator';

import { TrackRequestComponent } from './trackrequest.component';
import { TracklistComponent } from './tracklist/tracklist.component';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';


const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: TrackRequestComponent },
      // { path: 'services', component: ServicesComponent }
    ]

  }
]

const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};

@NgModule({
  declarations: [TrackRequestComponent, TracklistComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    MatPaginatorModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(maskConfigFunction),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot()
  ],
  providers: [FileTypeValidator]
})
export class TrackRequestModule { }
