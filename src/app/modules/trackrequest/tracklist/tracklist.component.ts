import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator } from '@angular/material/paginator';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-tracklist',
  templateUrl: './tracklist.component.html',
  styleUrls: ['./tracklist.component.css']
})
export class TracklistComponent implements OnInit {
  loanId: any;
  trackData: any;
  loading: boolean = false;
  floading: boolean = false;
  empty: boolean;
  appointmentId;
  submitted = false
  rescheduleForm = new FormGroup({
    appointmentDate: new FormControl('', Validators.required),
    timeslot: new FormControl('', Validators.required),
  })
  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  pageInfo = {
    page: 1,
    offset: 5
  }
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  rloading: boolean = false;

  filterData;
  constructor(public datepipe: DatePipe, private apihttp: ApiHttpService, private modalService: NgbModal, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) {
    this.paginationSettings["enablePagination"] = false;
  }

  ngOnInit(): void {
    this.loading = true;
    this.getAppointment()
  }

  // getFilterAppointment(data: any) {
  //   console.log(data)
  //   this.getAppointment(data);
  // }

  getAppointment(searchData?) {
    // GETALLTRANSTACTIONS
    this.floading = true;
    this.paginationSettings["offset"] = 5;
    let formData = new FormData()
    if (searchData) {
      this.filterData = searchData
    }
    console.log("searchData",searchData)
    if (this.filterData) {
      if (this.filterData.enquiryDate) {
        // formData.append("fromdate", this.filterData.enquiryDate);
        let sortDate = this.datepipe.transform(this.filterData.enquiryDate, 'yyyy-MM-dd') + '';
        formData.append("date", sortDate);
      }
      if (this.filterData.enquiryType && this.filterData.enquiryType != '') {
        formData.append("status", this.filterData.enquiryType);
      }
    }
    formData.append('page', this.pageInfo.page + '')
    formData.append('offset', this.pageInfo.offset + '')
    this.http.post(this.api.GETTRACKREQUESTENQUIRY, formData).subscribe(
      (res: any) => {
        this.trackData = res.data.data;
        console.log(res)
        this.paginationSettings["length"] = res.data.totalCount;
        this.paginationSettings["enablePagination"] = true;
        this.paginationSettings["totalPages"] = res.data?.totalPages;
        this.loading = false;
        this.floading = false;
        // this.goTo(1)
      },
      (error: any) => {
        if(error.status==400){
          this.trackData=[]
          this.paginationSettings["enablePagination"] = false;
        }
        console.log("Error", error);
        // this.toastr.error(error)
        this.loading = false;
        this.floading = false;
      }
    );
  }
  getIcon(data){
    if(data.img_type=='pdf')
       return '../../../assets/img/pdficon.png'
     else   
       return data.icon_img?data.icon_img:'../../../assets/img/ico-default.png'

  }
  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.getAppointment()
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
  updateschedule(type, apid) {
    this.floading = true;
    let formData = new FormData()
    formData.append('AppointmentBookingId', apid)
    formData.append('AppointmentStatus', type)
    this.http.post(this.api.UPDATEAPPOINTMENT, formData).subscribe(
      (res: any) => {

        this.getAppointment()
      },
      (error: any) => {
        console.log("Error", error);
        this.floading = false;
      });

  }
  reschedule(id, date, time, content) {
    this.appointmentId = id
    this.rescheduleForm.patchValue({
      appointmentDate: date,
      timeslot: time.substring(0, 5)
    })
    this.modalService.open(content).result.then((res) => {
    }, (res) => {
      console.log(res)

    })

  }
  resuduleSubmit() {

    this.submitted = true
    if (this.rescheduleForm.invalid) return;
    this.rloading = true;
    let formData = new FormData()
    formData.append('AppointmentBookingId', this.appointmentId)
    formData.append('AppointmentStatus', '2')
    formData.append('AppointmentDate', this.rescheduleForm.value.appointmentDate)
    formData.append('AppointmentStartTime', this.rescheduleForm.value.timeslot)
    this.http.post(this.api.UPDATEAPPOINTMENT, formData).subscribe(
      (res: any) => {
        this.rloading = false;
        this.modalService.dismissAll()
        this.getAppointment()
      },
      (error: any) => {
        this.rloading = false;
        console.log("Error", error);
      });
  }
  localTime(dateString) {

    return new Date(dateString + ' UTC');
  }
}
