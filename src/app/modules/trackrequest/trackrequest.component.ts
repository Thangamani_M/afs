import { HttpClient } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { TracklistComponent } from './tracklist/tracklist.component';


@Component({
  selector: 'app-trackrequest',
  templateUrl: './trackrequest.component.html',
  styleUrls: ['./trackrequest.component.css']
})
export class TrackRequestComponent implements OnInit {
  sectionId = '1';
  selectedId = 2
  addbooking = false
  statusfilter = ''
  appointmentData: any = [];
  loading: boolean = false;
  submitted = false
  dropdowndatas: any = [];
  getLoanId: any;
  currentValue: any;
  userInfo: any;
  today = new Date();

  @ViewChild(TracklistComponent) child: TracklistComponent;

  public trackEnquiryForm = new FormGroup({
    enquiryType: new FormControl('', [Validators.required]),
    enquiryDate: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)])
  })


  constructor(private fb: FormBuilder, private http: HttpClient, private api: ApiEndpointsService, private toastr: ToastrService) {

  }

  tabsData = [{
    id: '1',
    name: 'View & Manage Billing Cycle'
  }, {
    id: '2',
    name: 'Desired Repayment Method'
  }, {
    id: '3',
    name: 'Raise an Enquiry'
  }
    , {
    id: '4',
    name: 'Book an Appoinment'
  }, {
    id: '5',
    name: 'Loan Pre-closure'
  }
  ]

  bookingForm = new FormGroup({
    appointmenttype: new FormControl('', Validators.required),
    BranchId: new FormControl('', Validators.required),
    appointmentDate: new FormControl('', Validators.required),
    timeslot: new FormControl('', Validators.required),
    reason: new FormControl('', Validators.required)
  })

  billingForm = new FormGroup({
    billingLoanId: new FormControl('', Validators.required),
    customerName: new FormControl(Validators.required),
    billingValue: new FormControl('', Validators.required),
    billPaymentType: new FormControl('', Validators.required)
  })

  ngOnInit(): void {
    let getUser = localStorage.getItem('user');
    if (getUser) {
      this.userInfo = JSON.parse(getUser)
    }
    console.log(this.userInfo);

    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      // console.log(this.dropdowndatas)
    }

    // Get Repayment Loan ID's
    this.http.get(this.api.REPAYMENTMASTERDATA).subscribe(
      (res: any) => {
        this.getLoanId = res.data.Loans;
        // console.log("All Loan Id's", this.getLoanId)
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );

    // SET Form Values

    this.billingForm.controls["customerName"].setValue(this.userInfo.userName);


  }

  changeInputValue() {
    console.log("this.trackEnquiryForm.value",this.trackEnquiryForm.value)
    this.child.getAppointment(this.trackEnquiryForm.value)
  }

  get billingControls() {
    return this.billingForm.controls
  }
  goTo(id) {
    this.sectionId = id
  }

  booking() {

    this.addbooking = true
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }
  submit() {
    this.submitted = true
    console.log(this.bookingForm.value)
    if (this.bookingForm.invalid) {
      return
    }
    this.loading = true
    const formdata = new FormData()
    formdata.append('BranchId', this.bookingForm.value.BranchId);
    formdata.append('AppointmentBookingTypeId', this.bookingForm.value.appointmenttype);
    formdata.append('AppointmentDate', this.bookingForm.value.appointmentDate);
    formdata.append('AppointmentStartTime', this.bookingForm.value.timeslot);
    formdata.append('Comments', this.bookingForm.value.reason);
    this.http.post(this.api.INSERTAPPOINTMENT, formdata).subscribe(
      (res: any) => {
        this.bookingForm.reset()
        this.submitted = false
        this.appointmentData = res.data.data;

        this.loading = false;
        this.addbooking = false
        // this.goTo(1)
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.loading = false;
      }
    );
  }
  cancelbooking() {
    this.addbooking = false
  }
  getCurrentValue(id: any) {
    // alert(id)
    const formdata = new FormData()
    formdata.append('LoanId', id);
    this.http.post(this.api.REPAYMENTCURRENT, formdata).subscribe(
      (res: any) => {
        this.currentValue = res.data.ReferenceValue;
        this.billingForm.controls["billingValue"].setValue(this.currentValue ? this.currentValue : '');
        console.log(this.currentValue)
      },
      (error: any) => {
        console.log("Error", error);
      }
    );
  }

  submittab(type) {
    if (type = '1') {
      this.billingFormSubmit()
    } else if (type = 2) {

    }

  }

  billingFormSubmit() {

    this.submitted = true

    if (this.billingForm.invalid) {
      return
    }
    const formdata = new FormData()
    formdata.append('Type', this.billingForm.value.billPaymentType);
    formdata.append('LoanId', this.billingForm.value.billingLoanId);
    this.http.post(this.api.REPAYMENTINSERT, formdata).subscribe(
      (res: any) => {
        console.log(res)
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
      },
      (error: any) => {
        console.log("Error", error);
      }
    );
  }


  cancelBillingForm() {

  }
}