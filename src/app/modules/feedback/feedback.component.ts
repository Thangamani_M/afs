import { HttpClient } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  dropdowndatas: any = [];
  submitted = false;
  loading = false;
  stRating = 1;

  fileValidation: boolean = false;
  currentdate;

  fieldname: any = [];
  imagePath: any;
  imageJson: any = [{ id: '', url: '' }];
  feedbackForm = new FormGroup({
    feedbackType: new FormControl('', Validators.required),
    branchName: new FormControl('0'),
    starRating: new FormControl('0'),
    feedbackMessage: new FormControl('', Validators.required),
    attachments: new FormControl('', [this.imagvalidator.validateImage]),
  })

  constructor(private fb: FormBuilder, private http: HttpClient, private imagvalidator: FileTypeValidator, private api: ApiEndpointsService, private toastr: ToastrService) {
  }

  ngOnInit(): void {

    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      console.log(this.dropdowndatas)
    }

    
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }
  get feedbackFormControls() {
    return this.feedbackForm.controls
  }

  submitFeedback() {
    this.submitted = true

    if (this.feedbackForm.invalid) {
      return
    }
    this.loading = true
    const formdata = new FormData()
    formdata.append('FeedbackTypeId', this.feedbackForm.value.feedbackType);
    formdata.append('BranchId', this.feedbackForm.value.branchName);
    formdata.append('Comments', this.feedbackForm.value.feedbackMessage);
    formdata.append('Stars', this.feedbackForm.value.starRating);
    formdata.append('Image', this.feedbackForm.value.attachments);
    this.http.post(this.api.INSERTFEEDBACK, formdata).subscribe(
      (res: any) => {
        console.log(res)
        this.submitted = false
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
        this.feedbackForm.reset();
        this.feedbackForm.controls.feedbackType.setValue('')
        this.feedbackForm.controls.branchName.setValue('0')
        this.feedbackForm.controls.starRating.setValue('0')
        this.feedbackForm.controls.attachments.setValue('')
        this.loading = false;

      },
      (error: any) => {
        console.log("Error", error);
        this.loading = false;
      }
    );
  }
  resetFbForm() {
    this.submitted = false
    this.feedbackForm.reset();
    this.feedbackForm.controls.feedbackType.setValue('')
    this.feedbackForm.controls.branchName.setValue('0')
        this.feedbackForm.controls.starRating.setValue('0')
        this.feedbackForm.controls.attachments.setValue('')
  }

  gender = null;
  filter_source_type(v: any) {
    alert(v)
  }
  reset_filter() {
    this.gender = null;
  }

  resetRating() {
    console.log(this.stRating)
    //this.stRating = null
  }

  fileChange(event: any, FormControl) {
    this.fileValidation = false;

    console.log("FormControl", FormControl)
    // debugger;

    if (event.target.files.length > 0) {
      const formData = new FormData();

      const file = event.target.files[0];
      console.log("Value", this.feedbackForm.controls)
      this.feedbackForm.controls[FormControl].setValue(file['name'])
      this.feedbackForm.controls[FormControl].markAsDirty()
      if (!this.imagvalidator.validateImage(this.feedbackForm.controls[FormControl] as FormControl)) {
        const max_size = 25097152;
        console.log(event.target.files[0].size, (event.target.files[0].size / (1024 * 1024)).toFixed(2))
        if (event.target.files[0].size / (1024 * 1024) > max_size / (1024 * 1024)) {
          console.log(max_size / (1024 * 1024) + 'Mb')
          this.feedbackForm.controls[FormControl].setErrors({ 'filleSizeincorrect': true });
          return
        } else {
          this.feedbackForm.controls[FormControl].setErrors(null);
        }

        formData.append("image", file, file['name']);

        this.fieldname.push(FormControl)

        this.http.post(this.api.UPLOADFILE, formData).subscribe(
          (res) => {
            this.fileValidation = false;
            this.feedbackForm.controls[FormControl].setValue(res['data']['file_name']);
            this.imagePath = res['data']['path'];
            this.imageJson.push({ id: res['data']['file_name'], url: res['data']['path'] })
            const index = this.fieldname.indexOf(FormControl, 0);

            if (index > -1) {
              this.fieldname.splice(index, 1);
            }

          },
          (err) => {
            console.log(err);
            this.fileValidation = true;
            const index = this.fieldname.indexOf(FormControl, 0);
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }
            this.feedbackForm.controls[FormControl].setValue('');
          }
        );
      }
    }
  }

  geturl(id) {
    let url = this.imageJson.filter(item => item.id == id)
    if (url.length > 0)
      return url[0].url
    return false
  }

}
