import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';


// declare const encrypt: any;


@Component({
  selector: 'app-getotherinfo',
  templateUrl: './getotherinfo.component.html',
  styleUrls: ['./getotherinfo.component.css']
})
export class GetotherinfoComponent implements OnInit {
  public otherForm: FormGroup;
  occupationType = 0;
  trnloader = false
  loading=false
  isClickedOnce = true;
  tnrerror = ''
  trnnum;
  isSumitted = false
  keyword = 'name';
  employerDatas: any = [];
  allEmployers = [];
  allContries: any = [];
  editable: boolean = true;
  maxDate: Date;
  year_: string;

  constructor(private apihttp: ApiHttpService,private EncrDecr :EncrDecrService , public activeModal: NgbActiveModal,private http: HttpClient,public datepipe: DatePipe, private fb: FormBuilder, private api: ApiEndpointsService, private modalService: NgbModal, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 10)
    this.getCurrentAgeYear18();
   this.getForm();
   this.http.get(this.api.COUNTRYDATAS).subscribe(data => {
    console.log('C:', data)
    this.allContries = data;
  });
  }



  getCurrentAgeYear18() {
    var date = new Date();
    var intYear = date.getFullYear() - 18;
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    this.year_ = intYear + "-" + month + "-" + day;
    // console.log(new Date(this.year_))
    this.maxDate = new Date(this.year_);
    console.log(this.maxDate)
  }

getForm(){

  this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
    console.log(masterdata);
    // this.employerData = masterdata['data'].;  
    masterdata['data'].forEach(element => {
      if (element.key == "Employer") {
        this.employerDatas = element.value;
        // console.log('Employers', this.employerDatas);
        this.allEmployers = this.employerDatas;
      }
    });
    // localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
  })

  this.otherForm = this.fb.group({
    dateOfBirth: ['', [Validators.required]],
    trnNo: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(11)]],
    occupation: ['', [Validators.required]],
    countryCode:['+1'],
    mobilenumber:['', [Validators.required,Validators.minLength(7)]]
  });
  this.otherForm.controls.occupation.valueChanges.subscribe(data => {
    if (data == 1) {
      this.otherForm.addControl('employerData', new FormControl('', [Validators.required]))
      this.occupationType = 1;
      if (this.otherForm.controls.businessName)
        this.otherForm.removeControl('businessName')
        if(this.tnrerror)
          this.otherForm.controls['trnNo'].setErrors({ 'incorrect': true }); 
          
    } else {
      this.otherForm.addControl('businessName', new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]))
      this.occupationType = 2;
      if (this.otherForm.controls.employerData)
        this.otherForm.removeControl('employerData')
      if(this.tnrerror)
        this.otherForm.controls['trnNo'].setErrors({ 'incorrect': true });   
    }
  })
}
get otherInfo() {
  return this.otherForm.controls
}
submit(){

  this.isSumitted = true
  if (this.otherForm.invalid) {
    return
  }

  this.loading=true
  const formData = new FormData();
  formData.append("occupation", this.otherForm.value.occupation);
  if (this.occupationType == 1)
    // formData.append("employer", this.registerForm.value.employerData.id);
    formData.append("employer", this.otherForm.value.employerData.name?this.otherForm.value.employerData.name:this.otherForm.value.employerData);
  else
    formData.append("business_name", this.otherForm.value.businessName);
  // formData.append("dob", this.registerForm.value.dateOfBirth);    
  let datOfBirth = this.datepipe.transform(this.otherForm.value.dateOfBirth, 'yyyy-MM-dd')
  // console.log('DOB', datOfBirth);
  formData.append('dob', datOfBirth + '');
  formData.append("trn_number", this.trnnum);
  let phoneNo = this.otherForm.value.countryCode != '' ? this.otherForm.value.countryCode + this.otherForm.value.mobilenumber : ('+1' + this.otherForm.value.mobilenumber);
  formData.append("mobilenumber", phoneNo);
  this.http.post(this.api.UPDATETRN, formData).subscribe(
    (res: any) => {
      console.log("Status:", res);
      let user = localStorage.getItem("userDetails");
      if (user) {
      let checkTrn = JSON.parse(user)
      let updateUser=Object.assign(checkTrn,{DateOfBirth:datOfBirth + '',trn_number: this.trnnum,occupation:this.otherForm.value.occupation})
      localStorage.setItem("userDetails",JSON.stringify(updateUser))
      }
      this.loading = false;
      this.isSumitted = false;
      this.isClickedOnce = true
      this.activeModal.close(1)
    },
    (error: any) => {
      this.loading = false;
      this.isSumitted = false;
      this.isClickedOnce = true;
      console.log("Error", error);
    }
  );


}



changeTnr(e?) {
  if (!this.otherForm.value.trnNo || this.otherForm.value.trnNo == '')
    return
  if (e)
    this.trnnum = e.target.value
  this.trnloader = true
  const formData = new FormData()
  formData.append('trn', this.trnnum);
  let datOfBirth = this.datepipe.transform(this.otherForm.value.dateOfBirth, 'yyyy-MM-dd')
  formData.append('dob', datOfBirth + '');
  this.http.post(this.api.TNRVALID, formData).subscribe(data => {
    console.log(data)
    if(data && data['data'])
    {
      if(this.EncrDecr.getCryptLibDecoding(data['data'].TRNValidatedEnc)=="Y"){
        this.otherForm.controls['trnNo'].setErrors(null);
      } else {
        this.otherForm.controls['trnNo'].setErrors({ 'incorrect': true });
        this.tnrerror = "Verify that your TRN and DOB are correct"
       } 
    } else {
      this.otherForm.controls['trnNo'].setErrors({ 'incorrect': true });
      this.tnrerror = "Verify that your TRN and DOB are correct"
     } 
    this.trnloader = false
  }, error => {
    this.otherForm.controls['trnNo'].setErrors({ 'incorrect': true });
    console.log(error)
    if (error.errorResponse.data) {
      if (typeof error.errorResponse.error == 'string')
        this.tnrerror = error.errorResponse.error
      else if (typeof error.errorResponse.error == 'object') {
        let errorMessage = ''
        Object.keys(error.errorResponse.error).map(function (key, index) {
          errorMessage = errorMessage + error.errorResponse.error[key];
        });
        if(Object.keys(error.errorResponse.error) && Object.keys(error.errorResponse.error).length>0){
          this.tnrerror = errorMessage;
          } else{
            this.tnrerror= error.message
          }
      }
    }
    this.trnloader = false
  })
  console.log(this.otherForm.value.trnNo)
}

closeDialog() {
  this.modalService.dismissAll()
}
keyPressNumbers(event) {
  var charCode = (event.which) ? event.which : event.keyCode;
  // Only Numbers 0-9
  if ((charCode < 48 || charCode > 57)) {
    event.preventDefault();
    return false;
  } else {
    return true;
  }
}

keyPressAlpha(event) {

  var inp = String.fromCharCode(event.keyCode);

  if (/[a-zA-Z\s._-]/.test(inp)) {
    return true;
  } else {
    event.preventDefault();
    return false;
  }
}
  forTrn(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
  customFilter = function(allEmployers: any[], query: string): any[] {
    return allEmployers.filter(x => x.name.toLowerCase().startsWith(query.toLowerCase()));
  };
}

