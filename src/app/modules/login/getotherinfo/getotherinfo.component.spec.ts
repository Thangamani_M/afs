import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GetotherinfoComponent } from './getotherinfo.component';



describe('GetotherinfoComponent', () => {
  let component: GetotherinfoComponent;
  let fixture: ComponentFixture<GetotherinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetotherinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetotherinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
