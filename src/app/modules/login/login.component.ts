import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { Router } from '@angular/router';
import { UtilityService } from '../../services/common/utility.service';
import * as CryptoJS from 'crypto-js';
// import * as CryptoJS from 'crypto-js/aes';
import { EncrDecrService } from '../../services/common/enc-dec.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VerifySignUpComponent } from '../sign-up/verify-signup/verify-signup.component';
import { MarketinginfoComponent } from './marketingInfo/marketinginfo.component';
import { GetotherinfoComponent } from './getotherinfo/getotherinfo.component';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { MessagingService } from 'src/app/services/messaging.service';
import { RecaptchaComponent } from 'ng-recaptcha';


// declare const encrypt: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [NgbCarouselConfig,SocialAuthService]
})
export class LoginComponent implements OnInit {
  title = 'google-places-autocomplete';
  @ViewChild('recaptcha')
  reCaptcha: ElementRef;
  userAddress: string = ''
  userLatitude: string = ''
  userLongitude: string = ''

  sclEncProfileId: any;
  sclEncEmail: any;
  encEmail: any;
  encPassword: any;
  plainText: string;
  socialUser: SocialUser;
  isLoggedin: boolean;
  showPword: boolean = false;
  loading: boolean = false;
  isSumitted = false
  isClickedOnce = true;
  isloading = false
  issocialload=false
  dropdowndatas = []
  dropdownmarketing = []
  sitekey=environment.recaptcha.siteKey
  scrollBlocks = [
    { title: 'A Faster Way', short: 'A faster way to apply for your personal and business loans.', src: "../../../../assets/img/scroll-1.png" },
    { title: 'Personal Loans', short: 'Personal loans are available for both private and public sector employees.', src: "../../../../assets/img/scroll-2.png" },
    // { title: 'Third Slide', short: 'Third Slide Short', src: "../../../../assets/img/Login-bg.png" }
  ];
  socialstatus='0';

  constructor(private apihttp: ApiHttpService, private messagingService: MessagingService, private modalService: NgbModal, private toastr: ToastrService, private http: HttpClient, private socialAuthService: SocialAuthService, private api: ApiEndpointsService, private EncrDecr: EncrDecrService, private router: Router, private util: UtilityService) { }

  public loginForm = new FormGroup({
    userEmail: new FormControl('', [Validators.required, Validators.pattern("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{7})+$")]),
    passWord: new FormControl('', [Validators.required,  Validators.maxLength(20)]), //Validators.minLength(6),
    token: new FormControl('', [Validators.required])
  })

  async ngOnInit() {
    // ZohoSalesIQ.unregisterVisitor(context);

    // try {
      //  window['$zoho'].salesiq.ready = function () {
      //   window['$zoho'].salesiq.reset();
      //  };
    // } catch (e) {
    //   console.log(e);
    // }
    await this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.getSocialRes()
    //this.EncrDecr.getCDecoding("eyJpdiI6Im5KWDJOaGFvbDZNcEJBT1QiLCJ2YWx1ZSI6IjNjaUJHWGk3ek1kVzVZeTBrVkJTZEE9PSJ9")
    console.log('EEEE')

    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
    }
    this.issocialload=true
    this.apihttp.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      this.issocialload=false
      if (masterd) {
       
        this.dropdowndatas = JSON.parse(atob(masterd));
        this.getSocialStatus("GeneralSettings")
        // console.log(this.dropdowndatas)
      }
      },error=>{
        this.issocialload=false
      })
    
    console.log(this.getdropdownList('GeneralSettings'))
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      console.log("INNER=0")
      this.isLoggedin = (user != null);
      if (this.socialUser) {
        console.log("INNER=1")
       
       console.log("ENCRIPt",this.EncrDecr.getEncoding(this.EncrDecr.set("sakthi147@mailinator.com")))
        this.sclEncEmail = this.EncrDecr.set(this.socialUser.email);
        this.sclEncProfileId = this.EncrDecr.set(this.socialUser.id);
        let email = this.EncrDecr.getEncoding(this.sclEncEmail)
        let profileid = this.EncrDecr.getEncoding(this.sclEncProfileId)
        console.log("this.socialUser", this.socialUser)
        const formData = new FormData()
        formData.append('name', this.socialUser.firstName)
        // formData.append('email', this.socialUser.email)
        // formData.append('profileid', this.socialUser.id)
        formData.append('lastname', this.socialUser.lastName)
        formData.append('email', email)
        formData.append('profileid', profileid)
        if (localStorage.getItem('socialLogin') && localStorage.getItem('socialLogin') == '1') {
          // setTimeout(()=>{
            console.log("INNER=6")
          // },100)
          this.isloading = true
          console.log("INNER=6")
          localStorage.setItem('socialLogin', '0')
          this.http.post(this.api.SOCIALLOGIN, formData, { observe: 'response' as 'body' }).subscribe(
            (res: any) => {
              localStorage.setItem('login', 'social')
              this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
                this.util.signIn(res, 2);
                localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
                this.getSocialRes(res)
                this.deviceUpdate(res.body.data.user.id)
              }, error => {
                
                this.isloading = false
                console.log("INNER=2")
              })
            },
            (error: any) => {
              this.isloading = false
              console.log("INNER=3")
              console.log("Error", error);
            }
          );
        } else {
          // this.isloading = false
          console.log("INNER=4")
        }
      } else {
        // this.isloading = false
        console.log("INNER=5")
      }

    });
  }
  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
  }
  getSocialRes(res?) {
    let user = localStorage.getItem("userDetails");
    if (user) {

      console.log(JSON.parse(user))
      let checkTrn = JSON.parse(user)
      console.log('data_:', checkTrn.LoanCount)
          if(checkTrn.passwordflag!="N")
            return
      if (!checkTrn.DateOfBirth || checkTrn.DateOfBirth == "" || !checkTrn.trn_number || checkTrn.trn_number == "") {
        this.modalService.open(GetotherinfoComponent, { size: '' }).result.then((data) => {
          if (data == 1) {
            setTimeout(() => {
              this.isloading = false;
            }, 200);
            // this.isloading = false
            let verify = localStorage.getItem("EmailVerification");
            console.log('USER_TOKEN:', verify)
            // this.modalService.open(MarketinginfoComponent)
            if (verify &&  this.EncrDecr.getCryptLibDecoding(verify) == "allow") {
              localStorage.setItem("logintime",JSON.stringify({date:new Date()}))
              if (checkTrn.LoanCount != 0)
                this.router.navigateByUrl('/my-loans');
              else
                this.router.navigateByUrl('/products');


              // this.router.navigateByUrl('/products');
              this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
                console.log("marketingdata",marketdata)
                localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))

                let marketing = localStorage.getItem('marketingdata')
                if (marketing) {
                  this.dropdownmarketing = JSON.parse(atob(marketing));
                }
               setTimeout(() => {
                if (checkTrn.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
                  const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
                  modalRef.componentInstance.loginType = 'AfterLogin';
                }
               }, 3000);
            })
            }
          } else {
            this.isloading = false
          }
        }).catch(() => {
          this.isloading = false
        })
      } else {


        setTimeout(() => {
          this.isloading = false;
        }, 200);
        let verify = localStorage.getItem("EmailVerification");
        console.log('USER_TOKEN:', verify)
        // this.modalService.open(MarketinginfoComponent)
        if (verify && this.EncrDecr.getCryptLibDecoding(verify) == "allow") {
          if (checkTrn.LoanCount != 0)
            this.router.navigateByUrl('/my-loans');
          else
            this.router.navigateByUrl('/products');
          // this.router.navigateByUrl('/products');
          this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
            localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))

            let marketing = localStorage.getItem('marketingdata')
            if (marketing) {
              this.dropdownmarketing = JSON.parse(atob(marketing));
            }
           setTimeout(() => {
            if (checkTrn.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
              const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
              modalRef.componentInstance.loginType = 'AfterLogin';
            }
           }, 3000);
        })
        }

      }
    }
  }
  loginWithGoogle(): void {
    localStorage.setItem('socialLogin', '1')
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  loginWithFacebook(): void {
    localStorage.setItem('socialLogin', '1')
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  logOut(): void {
    this.socialAuthService.signOut();
  }
  get logincontrols() {
    return this.loginForm.controls
  }
  showResponse(e){
    this.loginForm.controls.token.setValue(e?.response)
   console.log(e?.response)
  }
  login(captcha) {
    this.isSumitted = true
    if (this.loginForm.invalid) {
      //captcha?.reset()
     // this.loginForm.controls.token.setValue('')
      return
    }
    this.isClickedOnce = false;
    this.loading = true;
    let username = this.loginForm.value.userEmail
    let logintype = 'email'
    if (/([0-9]{7})+$/.test(username)) {
      username = '+1' + this.loginForm.value.userEmail
      logintype = 'phone'
    }
    console.log(username)
    this.encEmail = this.EncrDecr.set(username);
    this.encPassword = this.EncrDecr.set(this.loginForm.value.passWord);
    let email = this.EncrDecr.getEncoding(this.encEmail)
    let password = this.EncrDecr.getEncoding(this.encPassword)
    // console.log("Email_:", email)
    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
     formData.append("grecaptcharesponse", this.loginForm.value.token);
    this.http.post(this.api.LOGIN, formData, { observe: 'response' as 'body' }).subscribe(
      (res: any) => {
        // debugger;
        
        console.log('UserData_:', res)
        this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
          localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
          this.loading = false;
          this.isClickedOnce = true;
          this.isSumitted = false
          this.loginForm.controls.token.setValue('')
          captcha?.reset()
          document.getElementById('captcha')
          this.util.signIn(res, 1);
          let verify: any
          if (logintype == 'email')
            verify = localStorage.getItem("EmailVerification");
          else
            verify = localStorage.getItem("PhoneVerification");
          console.log('USER_TOKEN:', verify)
          // this.modalService.open(MarketinginfoComponent)
          if (verify && this.EncrDecr.getCryptLibDecoding(verify) == "allow") {
            localStorage.setItem("logintime",JSON.stringify({date:new Date()}))
            if (res.body.data.user.LoanCount != 0)
              this.router.navigateByUrl('/my-loans');
            else
              this.router.navigateByUrl('/products');
              this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
                localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))
                let marketing = localStorage.getItem('marketingdata')
                if (marketing) {
                  this.dropdownmarketing = JSON.parse(atob(marketing));
                }
          
                if (res.body.data.user.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
                  setTimeout(() => {
                    const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
                    modalRef.componentInstance.loginType = 'AfterLogin';
                   }, 3000);
                }
              })

              this.deviceUpdate(res.body.data.user.id)
          } else {
            const modalRef = this.modalService.open(VerifySignUpComponent);
            modalRef.componentInstance.verifyemail = username;
            modalRef.componentInstance.verifytype = logintype;
            modalRef.componentInstance.loginType = 'AfterLogin';
            if (logintype == 'email')
                modalRef.componentInstance.verifymode = "1";
                // modalRef.result.then(data=>{
                //   this.deviceUpdate(res.body.data.user.id)
                // })      
            //this.toastr.error("Your email id is not verified", '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
            
          }
         
        })

      },
      (error: any) => {
        this.loading = false;
        this.isClickedOnce = true;
        console.log("Error", error);
        this.loginForm.controls.token.setValue('')
        captcha?.reset()
        this.isSumitted = false
      }
    );
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdownmarketing.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
       if(dropdownlist.length>0){
         dropdownlist = dropdownlist.filter((item)=>item['ViewType'] == 'AfterLogin')
       }
    }
    return dropdownlist
  }
  getSocialStatus(key) {
    let dropdownlist:any=[]
    let filterdata:any = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value'].filter(item => item['name'] == "SocialMedia")
      if(dropdownlist.length>0){
        this.socialstatus=dropdownlist[0].status
      }
    }

  }
  deviceUpdate(uid) {
    let token = localStorage.getItem('device_token')
    if (token) {
      const formData = new FormData();
      formData.append("device_version", '1');
      formData.append("user_id", uid);
      formData.append("device_token", token);
      formData.append("device_type", '3');
      this.http.post(this.api.DEVICEUPDATE, formData).subscribe(masterdata => {
        console.log(masterdata)
      }, error => {
        console.log(error)
      })
    }
  }

  keyPressEmail(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9@._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  showPassword() {
    this.showPword = true;
  }

  hidePassword() {
    this.showPword = false;
  }
  forgotPassword() {
    this.router.navigateByUrl('/forgot-password');
  }
  openMarket() {
    this.modalService.open(MarketinginfoComponent)
  }

  ngOnDestroy() {
this.loginForm.reset()
this.isSumitted=false
  }

}

