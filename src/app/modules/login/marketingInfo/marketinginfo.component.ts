import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ApiHttpService } from 'src/app/services/common/api-http.service';


// declare const encrypt: any;


@Component({
  selector: 'app-marketinginfo',
  templateUrl: './marketinginfo.component.html',
  styleUrls: ['./marketinginfo.component.css']
})
export class MarketinginfoComponent implements OnInit {
  @Input() loginType = '';

  dropdowndatas = []
  tabsArray: any;
  positionIndex=1; 
  isloader=false
  constructor(private apihttp: ApiHttpService,private toastr: ToastrService, private api: ApiEndpointsService, private modalService: NgbModal, private router: Router, private route: ActivatedRoute) { }

  generalDetail: boolean = true;
  houseDetail: boolean = false;
  marketingInfo = new FormGroup({})
  public houseInfoForm = new FormGroup({})
  isClickedOnce = true;
  ngOnInit(): void {
    let masterdata = localStorage.getItem('marketingdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
    }
    console.log("inaaaa===")
    this.apihttp.post(this.api.MARKETINGDATA,{}).subscribe(masterdata => {

      localStorage.setItem('marketingdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('marketingdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
    console.log(this.getdropdownList('QuestionName'))
   let position = this.getdropdownList('QuestionName').filter((item)=>item['ViewType'] == this.loginType)
   if(position.length>0)
      this.getForm(position[0]['type'])
  }
  getForm(i?) {
    if(i)
    this.positionIndex=i;
    if (this.getdropdownList('QuestionName')) {
      console.log(this.getdropdownList('QuestionName'))
      this.getdropdownList('QuestionName').forEach((item) => {
        if (item['type'] == i && item['ViewType'] == this.loginType)
             this.marketingInfo.addControl(item['id'] + '', new FormControl(''))
      })
    }

    if( this.getdropdownList("MarketingQuestionTabs")){
      
     this.tabsArray = this.getdropdownList("MarketingQuestionTabs").filter((item: any) =>item.ViewType==  this.loginType); 
     console.log(this.tabsArray)
     }
  }
  getInt(e, i) {
    if (e.length == 0) {
      return false
    }
    return e == i
  }

  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }

  getdropdownAns(key) {
    let dropdownlist = []
    let filterdata = this.getdropdownList('AnswerName').filter(item => item['MarketingQuestionId'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata
    }
    return dropdownlist
  }
  get marketingformcontrols() {
    return this.marketingInfo.controls
  }
  get houseformcontrols() {
    return this.houseInfoForm.controls
  }
  openMarket(content) {
    this.modalService.open(content)
    //  .result.then(res=>{

    //  })
  }
  closeDialog() {
    this.modalService.dismissAll()
  }
  goTo(item: any) {
    // alert(id);
    localStorage.removeItem('draft_id');
    localStorage.setItem('repeatcustomer', item.LoanId);
    this.router.navigate(['products', 'loan-form', item.id]);
    // this.router.navigateByUrl('/loan-form', id);
  }
  setTab() {
     console.log(this.positionIndex)
    for(let i=0;i< this.tabsArray.length; i++) {
       if(this.tabsArray[i]["id"] ==this.positionIndex){
         if(i == (this.tabsArray.length-1)){
           this.submitData()
           break;
         } else {
      
           this.getForm(this.tabsArray[i+1]['id'])
         // this.positionIndex =this.tabsArray[i+1]['id']
          break;
         }
        }
     };
  }
  submitData() {
    this.isloader=true
    let reqDatas: any = {
      Answers: []
    }
    let flag=false
    this.getdropdownList('QuestionName').forEach((item) => {
      if(this.marketingInfo.value[item['id']])
          flag=true
      if (item['fieldtype'] == 'Dropdown' && item['ViewType'] == this.loginType) {
        reqDatas.Answers.push({
          "QuestionId": item['id'],
          "AnswerId": this.marketingInfo.value[item['id']],
          "Answer": ""
        })
            
      } else {
        if (item['ViewType'] == this.loginType) {
          reqDatas.Answers.push({
            "QuestionId": item['id'],
            "AnswerId": "",
            "Answer": this.marketingInfo.value[item['id']]
          })
        }
      }
      // formData.append('answer[]', this.marketingInfo.value[item['id']])
      // else
      // formData.append('answer[]', this.houseInfoForm.value[item['id']])
    })

    if(!flag) {
      this.modalService.dismissAll()
      this.isloader=false
      return
    }

    this.apihttp.post(this.api.MARKETINGINFO, JSON.stringify(reqDatas)).subscribe(
      (res: any) => {
        
        this.modalService.dismissAll()
        console.log("Marketing Loans:", res)
        this.isloader=false
      
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
      },
      (error: any) => {
        this.isloader=false
        console.log("Error", error);
      }
    );
  }
}

