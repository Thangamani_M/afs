import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MarketinginfoComponent } from './marketinginfo.component';



describe('MarketinginfoComponent', () => {
  let component: MarketinginfoComponent;
  let fixture: ComponentFixture<MarketinginfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketinginfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketinginfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
