import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  loading: boolean = true;
  descrption: any;

  constructor(private location: Location, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService) { }

  ngOnInit(): void {
    this.apihttp.get(this.api.PRIVACYPOLICY).subscribe(
      (res: any) => {
        this.loading = false;
        this.descrption = this.decodeEntities(res.data.InfoPageSettingValue);
        console.log("Data:", res.data);
      },
      (error: any) => {
        this.loading = false;
        console.log("Error", error);
      }
    );
  }

  goBack() {
    this.location.back()
  }

  decodeEntities(str: any) {
    const element = document.createElement('div');
    if (str && typeof str === 'string') {
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }
    return str;
  }
}
