import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  data: any;
  loading: boolean = true;

  constructor(private EncrDecr: EncrDecrService, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    let token;
    token = localStorage.getItem("accessTOKEN");
    console.log('Token', token)
    let httpHeaders = new HttpHeaders({
      "Content-Type": "text/plain",
      "Authorization": token,
      "Accept": "*/*"
    });
    this.loading=true
    this.http.get(this.api.FAQLIST, { headers: httpHeaders }).subscribe(
      (res: any) => {
        console.log("FAQ", res);
        this.data = res.data;
        this.loading = false;
      },
      (error: any) => {
        this.loading=false
        console.log("Error", error);
      }
    )

  }
  toggleAccordian(event, index) {
    const element = event.target;
    element.classList.toggle("active");
    if (this.data[index].isActive) {
      this.data[index].isActive = false;
    } else {
      this.data[index].isActive = true;
    }
    const panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }

}
