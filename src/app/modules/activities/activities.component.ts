import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { UtilityService } from 'src/app/services/common/utility.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {
  allActivities: any;
  loading: boolean = true;

  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  pageInfo = {
    page: 1,
    offset: 5
  }
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  constructor(private EncrDecr: EncrDecrService, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    console.log("URL:", this.api.ACTIVITIES)

    let userId;

    let getUser = localStorage.getItem('user');
    if (getUser) {
      userId = JSON.parse(getUser).userId
    }


    // const formData = new FormData();
    // formData.append("user_id", userId);

    // this.apihttp.get(this.api.ACTIVITIES + '?' + userId).subscribe(
    this.getAllActivities();
  }
  getAllActivities() {
    this.loading = true;
    let formData = new FormData()
    formData.append('page', this.pageInfo.page + '')
    formData.append('offset', this.pageInfo.offset + '')
    // this.apihttp.get(this.api.ACTIVITIES).subscribe(
      this.http.post(this.api.ACTIVITIES, formData).subscribe(
      (res: any) => {
        this.allActivities = res.data.data;
        console.log("All Activities:", this.allActivities);

        this.paginationSettings["length"] = res.data.totalCount;
        this.paginationSettings["enablePagination"] = true;
        this.paginationSettings["totalPages"] = res.data?.totalPages;
        this.loading = false;

      },
      (error: any) => {
        console.log("Error", error);
        this.loading = false;
        // this.toastr.error(error)
      }
    );
  }
  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.getAllActivities();
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }

  localTime(dateString){  
    return new Date(dateString + ' UTC');
 }
}
