import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from '../../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MarketinginfoComponent } from '../../login/marketingInfo/marketinginfo.component';


@Component({
  selector: 'app-personal-loan',
  templateUrl: './personal-loan.component.html',
  styleUrls: ['./personal-loan.component.css']
})
export class PersonalLoanComponent implements OnInit {
  products: any;

  loading: boolean = false;
  empty: boolean = false;
  generalDetail: boolean = true;
  houseDetail: boolean = false;
  isSumitted = false;
  isClickedOnce = true;
  dropdowndatas = []
  public marketingInfoForm = new FormGroup({
    genInfo1: new FormControl('', [Validators.required]),
    genInfo2: new FormControl('', [Validators.required]),
    genInfo3: new FormControl('', [Validators.required]),
    genInfo4: new FormControl('', [Validators.required]),
    genInfo5: new FormControl('', [Validators.required]),
    genInfo6: new FormControl('', [Validators.required])
  })
  public houseInfoForm = new FormGroup({})
  marketingInfo = new FormGroup({})
  constructor(private apihttp: ApiHttpService, private http: HttpClient, private modalService: NgbModal, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // this.modalService.open(MarketinginfoComponent, { size: 'lg' })

    this.loading = true;
    // console.log(this.loading)
    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
    }
    
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
    console.log(this.getdropdownList('QuestionName'))

    // debugger;    
    this.apihttp.get(this.api.TYPESOFLOANS).subscribe(
      (res: any) => {
        this.products = res.data;
        console.log("All Loans:", this.products)
        // this.toastr.success(res.message)
        // this.router.navigateByUrl('/products');
        // this.router.navigateByUrl(['/products', id]);
        // this.router.navigate(['/products', JSON.stringify(res)]);
        this.loading = false;

      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)

      }
    );
    this.getForm()
  }
  getForm() {
    if (this.getdropdownList('QuestionName')) {
      console.log(this.getdropdownList('QuestionName'))
      this.getdropdownList('QuestionName').forEach((item) => {
        if (item['type'] == '1')
          this.marketingInfo.addControl(item['id'] + '', new FormControl('', [Validators.required]))
        else
          this.houseInfoForm.addControl(item['id'] + '', new FormControl('', [Validators.required]))

      })
    }
  }

  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }

  getdropdownAns(key) {
    let dropdownlist = []
    let filterdata = this.getdropdownList('AnswerName').filter(item => item['MarketingQuestionId'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata
    }
    return dropdownlist
  }
  get marketingformcontrols() {
    return this.marketingInfo.controls
  }
  get houseformcontrols() {
    return this.houseInfoForm.controls
  }
  openMarket(content) {
    this.modalService.open(MarketinginfoComponent, { size: 'lg' })
    //  .result.then(res=>{

    //  })
  }

  goTo(item: any) {
    // alert(id);
    if (localStorage.getItem('continueLoan')) {
      localStorage.setItem('continueLoan', '2')
    }
    localStorage.removeItem('draft_id');
    localStorage.setItem('repeatcustomer', item.LoanId);
    this.router.navigate(['products', 'loan-form', item.id]);
    // this.router.navigateByUrl('/loan-form', id);
  }
  setTab(tab: any) {
    // alert(tab);
    if (tab == 'G') {
      this.houseDetail = false;
      this.generalDetail = true;
    } else if (tab == 'H') {
      this.generalDetail = false;
      this.houseDetail = true;
    }
  }
  submitData() {
    if (this.marketingInfo.invalid || this.houseInfoForm.invalid) {
      return
    }
    let formData = new FormData()
    this.getdropdownList('QuestionName').forEach((item) => {
      if (item['type'] == '1')
        formData.append('answer[]', this.marketingInfo.value[item['id']])
      else
        formData.append('answer[]', this.houseInfoForm.value[item['id']])
    })

    this.apihttp.post(this.api.MARKETINGINFO, formData).subscribe(
      (res: any) => {
        console.log("Marketing Loans:", res)
      },
      (error: any) => {
        console.log("Error", error);
      }
    );
  }
}
