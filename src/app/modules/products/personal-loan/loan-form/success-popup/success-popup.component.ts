import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MarketinginfoComponent } from 'src/app/modules/login/marketingInfo/marketinginfo.component';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ApiHttpService } from 'src/app/services/common/api-http.service';


// declare const encrypt: any;


@Component({
  selector: 'app-success-popup',
  templateUrl: './success-popup.component.html',
  styleUrls: ['./success-popup.component.css']
})
export class SuccessPopupComponent implements OnInit {
 
  @Input() split1Content=''
  @Input() popupid = ''
  @Input() split2Content=''
  dropdowndatas = []
  constructor(private apihttp: ApiHttpService,private toastr: ToastrService, private api: ApiEndpointsService, private modalService: NgbModal, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let masterdata = localStorage.getItem('marketingdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
    }
   
  }
  done() {
    this.modalService.dismissAll()
    if(this.getdropdownList('QuestionName').length>0){
    const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
    modalRef.componentInstance.loginType = 'AfterLoanSubmit';
    }
    this.router.navigate(['my-loans']);
  }
  track() {
    this.modalService.dismissAll()
    if(this.getdropdownList('QuestionName').length>0){
    const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
    modalRef.componentInstance.loginType = 'AfterLoanSubmit';
    }

    this.router.navigate(['my-loans'], { queryParams: { DraftId: atob(localStorage.getItem('draft_id') + '') } });
  }
  cancelpop() {
    this.modalService.dismissAll()
    this.router.navigate(['my-loans']);
  }
  cancelform() {
    this.router.navigate(['my-loans']);
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
      if(dropdownlist.length>0){
        dropdownlist = dropdownlist.filter((item)=>item['ViewType'] == 'AfterLoanSubmit')
      }
    }
    return dropdownlist
  }
}

