import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { UtilityService } from '../../../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { SignaturePad } from 'angular2-signaturepad';
import { SignpadpopupComponent } from '../signpadpopup/signpadpopup.component';
// import { SignaturePadComponent } from '@ng-plus/signature-pad';
import { base64ToFile } from 'ngx-image-cropper';
import {
  WebcamImage,
  WebcamInitError,
  WebcamComponent,
  WebcamMirrorProperties,
  WebcamUtil,
} from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { DatePipe } from '@angular/common';
import { MarketinginfoComponent } from 'src/app/modules/login/marketingInfo/marketinginfo.component';
import { SuccessPopupComponent } from './success-popup/success-popup.component';
declare var MediaRecorder: any;
// import { Options } from 'ngx-google-places-autocomplete/objects/options/options';

@Component({
  selector: 'app-loan-form',
  templateUrl: './loan-form.component.html',
  styleUrls: ['./loan-form.component.css'],
  providers: [DatePipe],
})
export class LoanFormComponent implements OnInit, OnDestroy {
  tabsData: any = [];
  filterFields: any = [];
  showForm = false;
  year_;
  filterForm: FormGroup | any;
  sectionId: any = 0;
  submitted1 = false;
  loanTypeId: any;
  uploadedFile: any;
  loading: boolean = false;
  fullPageLoader: boolean = false;
  empty: boolean = false;
  dropdowndatas = [];
  teamscondition = false;
  privacypolicy = false;
  fileLoader = false;
  imageJson = [
    {
      id: '',
      url: '',
    },
  ];
  @ViewChild('submitmodal') submitmodal: ElementRef;
  signatureImg: string;
  @ViewChild('signatureCanvas', { static: true }) signaturePad: SignaturePad;
  signaturePadOptions: Object = {
    minWidth: 5,
    canvasWidth: 500,
    canvasHeight: 300,
  };
  terms = '#';
  policy = '#';
  fieldname: any = [];
  closeModal: string;
  signformgroup;
  signfieldname: any;
  popupid = '';
  selectedId: any;
  commentStatus: any;
  tabName: any = '';
  commentloanId: any;
  allComments: any;
  commentId: any;
  comentLoading: boolean = false;
  loanTitle: any;
  signevent: any;
  repeatcustomerDraft;
  reapeatarray: any = [];
  alreadyApplied: boolean = false;
  appliedResponse: any;
  maxDate: Date;
  userAddress: string = '';
  userLatitude: string = '';
  userLongitude: string = '';
  livenessTestBlock: boolean = false;
  @ViewChild('placesRef') placesRef!: GooglePlaceDirective;
  keyword = 'description';
  //Record Video
  @ViewChild('recordedVideo') recordVideoElementRef: ElementRef;
  @ViewChild('video') videoElementRef: ElementRef;

  videoElement: HTMLVideoElement;
  recordVideoElement: HTMLVideoElement;
  mediaRecorder: any;
  recordedBlobs: Blob[];
  isRecording: boolean = false;
  downloadUrl: string;
  stream: MediaStream;
  statrrecord=false
  yearArraY = [];
  monthArraY = [];
  options = {
    componentRestrictions: { country: ['JM'] },
  } as unknown as Options;
  userInfo: any;
  livenesStatus: boolean = false;
  takeCamera = true;
  timerId;
  @ViewChild('addressInput')
  addressInput: ElementRef;
  listAddress: any = [];
  liveflage = 'N';
  constructor(
    private apihttp: ApiHttpService,
    private modalService: NgbModal,
    private imagvalidator: FileTypeValidator,
    private http: HttpClient,
    private api: ApiEndpointsService,
    private router: Router,
    private util: UtilityService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    public datepipe: DatePipe,
    private ref: ChangeDetectorRef
  ) {}

  async ngOnInit() {
    this.getdropdownListYear();
    this.getdropdownListMonth();
    this.util.setMasterData();
    //this.placesRef.options.componentRestrictions = { country: 'JM' }
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 10);
    this.getCurrentAgeYear18();
    this.loading = true;
    let masterdata = localStorage.getItem('masterdata');
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      // console.log(this.dropdowndatas)
    }
    const formData = new FormData();
    formData.append('input', 'mall');

    this.http.post(this.api.PLACEAUTOCOMPLETE, formData).subscribe((data) => {
      console.log(data);
    });

    this.http.get(this.api.MASTERDATA).subscribe((masterdata) => {
      localStorage.setItem(
        'masterdata',
        btoa(JSON.stringify(masterdata['data']))
      );
      let masterd = localStorage.getItem('masterdata');
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
    });

    if (localStorage.getItem('repeatcustomer'))
      this.repeatcustomerDraft = localStorage.getItem('repeatcustomer');
    this.route.params.subscribe((paramid: Params) => {
      if (paramid['id']) {
        this.selectedId = paramid['id'];
        // console.log('Draft_Id:', this.selectedId)
        this.getTaps(paramid['id'], 1);
      }
    });
    if (localStorage.getItem('draft_id')) {
      this.commentloanId = atob(localStorage.getItem('draft_id') + '');
      console.log('Draft_Id:', this.commentloanId);
    }
    let getUser = localStorage.getItem('userDetails');
    if (getUser) {
      this.userInfo = JSON.parse(getUser);
      // this.livenesStatus = this.userInfo.LivenessVerified;
      // this.commonData.updateGlobalVar(this.userInfo)
    }

    // For Liveness Test
    WebcamUtil.getAvailableVideoInputs().then(
      (mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      }
    );
  }
  ngAfterViewInit() {
    console.log('this.placesRef', this.placesRef);
    //this.placesRef.options.componentRestrictions = { country: 'JM' }
  }
  reformat(event) {
    if (event.data) {
      // We don't want it to trigger on delete so we make sure there is data in the event (the entered char)
      const two_chars_no_colons_regex = /([^:]{2}(?!:))/g;
      event.target.value = event.target.value.replace(
        two_chars_no_colons_regex,
        '$1:'
      );
    }
  }
  getTaps(pid, first) {
    if (!this.loading) {
      this.fullPageLoader = true;
    }
    const param = new FormData();
    param.append('id', pid);
    if (localStorage.getItem('draft_id')) {
      param.append('draftid', atob(localStorage.getItem('draft_id') + ''));
    }
    if (first != 1) {
      if (first != 3) {
        let tab = JSON.parse(atob(localStorage.getItem('selecttabs')!!));
        let updatetap = tab.data;

        updatetap.map((t) => {
          if (t.id == tab.data[this.sectionId].id) {
            t.status = '2';
          }
        });
        let assignData = Object.assign(tab, { data: updatetap });
        localStorage.setItem('selecttabs', btoa(JSON.stringify(assignData)));
        this.tabsData = updatetap;
        this.loanTitle = tab.LoanName;
        this.terms = tab.terms;
        this.policy = tab.policy;
        this.commentStatus = tab.comments;
        this.liveflage = tab.liveflag;
        console.log('Comment Status_:', this.commentStatus);
      }
      if (this.tabsData && this.tabsData.length > 0) {
        this.goTo(first == 3 ? this.sectionId : this.sectionId + 1);
      } else {
        this.loading = false;
        this.fullPageLoader = false;
      }
      this.ref.detectChanges();
      return;
    }
    localStorage.removeItem('selecttabs');
    this.http.post(this.api.LOANMASTERTABS, param).subscribe(
      (res: any) => {
        localStorage.setItem('selecttabs', btoa(JSON.stringify(res.data)));
        let tab = JSON.parse(atob(localStorage.getItem('selecttabs')!!));
        this.tabsData = tab.data;
        this.loanTitle = tab.LoanName;
        this.terms = tab.terms;
        this.policy = tab.policy;
        this.commentStatus = tab.comments;
        this.liveflage = tab.liveflag;
        // this.commentStatus = "Y";
        // console.log(this.tabsData)
        console.log('Comment Status_:', this.tabsData);
        let preurl = this.util.getPreviousUrl().split('/')[2];
        if (preurl == 'add-comment' || preurl == 'view-comment') {
          this.sectionId = this.tabsData?.length + 1;
          this.SelectedTab(this.tabsData?.length + 1);
          return;
        }
        if (first == 1 && this.tabsData) {
          this.sectionId = this.sectionId - 1;

          console.log('tabss', this.sectionId, this.tabsData.length);
          this.tabsData.forEach((element, i) => {
            if (localStorage.getItem('draft_id')) {
              this.reapeatarray.push(element.id);
            }
            if (this.util.getFormArrayStorage(element.id)) {
              // this.util.removeFormArrayStorage(element.id)
              //  this.util.removeFormGroupStorage(i+1)
            }
            if (localStorage.getItem('continueLoan')) {
              this.util.removeFormArrayStorage(element.id);
              if (localStorage.getItem('continueLoan') == '2') {
                localStorage.removeItem('continueLoan');
              }
            }
          });
        }
        if (this.tabsData && this.tabsData.length > 0) {
          this.goTo(first == 3 ? this.sectionId : this.sectionId + 1);
        } else {
          this.loading = false;
          this.fullPageLoader = false;
        }
        this.ref.detectChanges();
      },
      (error: any) => {
        console.log('Error', error);
        this.fullPageLoader = false;
        this.loading = false;
        if (error.status == 400) this.alreadyApplied = true;
        this.appliedResponse = error.message;
      }
    );
  }

  getCurrentAgeYear18() {
    var date = new Date();
    var intYear = date.getFullYear() - 18;
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    this.year_ = intYear + '-' + month + '-' + day;
    this.maxDate = new Date(this.year_);
  }
  getdropdownList(key) {
    let dropdownlist = [];
    let filterdata = this.dropdowndatas.filter((item) => item['key'] == key);
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value'];
    }
    return dropdownlist;
  }

  getdropdownListBankBranch(key, val) {
    let dropdownlist = [];
    let filterdata = this.dropdowndatas.filter((item) => item['key'] == key);
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value'];
      if (dropdownlist.length > 0) {
        dropdownlist = dropdownlist.filter((item) => item['bankmaxid'] == val);
      }
    }
    return dropdownlist;
  }

  showErrorMessage(errormessage, key) {
    let errorText = '';
    let filterdata = errormessage.filter((item) => item['key'] == key);
    if (filterdata.length > 0) {
      errorText = filterdata[0]['text'];
    }
    return errorText;
  }

  getdropdownEmp(key) {
    let dropdownlist: any = [];
    // console.log("dropdownlist",key)
    let filterdata: any = this.dropdowndatas.filter(
      (item) => item['key'] == key
    );
    if (filterdata.length > 0) {
      filterdata[0]['value'].forEach((element) => {
        dropdownlist.push(element.name);
      });

      //console.log("dropdownlist",dropdownlist)
    }

    return dropdownlist;
  }
  getdropdownListYear() {
    let dropdownlist: any = [];
    for (let i = 0; i < 100; i++) {
      if (i > 1) dropdownlist.push({ id: i, name: i + ' years' });
      else dropdownlist.push({ id: i, name: i + ' year' });
    }
    this.yearArraY = dropdownlist;
  }

  getdropdownListMonth() {
    let dropdownlist: any = [];
    for (let i = 0; i < 12; i++) {
      if (i > 1) dropdownlist.push({ id: i, name: i + ' Months' });
      else dropdownlist.push({ id: i, name: i + ' Month' });
    }

    this.monthArraY = dropdownlist;
  }
  goTo(id: any) {
    // debugger;
    let tabsectionId = this.tabsData[id].id;
    this.tabName = '';
    this.fullPageLoader = true;
    this.showForm = true;
    this.submitted1 = false;
    if (this.util.getFormArrayStorage(tabsectionId)) {
      this.sectionId = id;
      this.filterFields = JSON.parse(
        atob(this.util.getFormArrayStorage(tabsectionId)!!)
      );
      if (this.filterFields.length > 0) {
        this.filterFields.map((item, i) => {
          if (i == 0) {
            this.filterFields[i].isActive = true;
          } else {
            this.filterFields[i].isActive = false;
          }
        });
      }
      this.getFilnalError();
      this.fullPageLoader = false;
      let arraids: any = [];
      // if (localStorage.getItem('draft_id') || (this.repeatcustomerDraft && this.repeatcustomerDraft != "undefined")) {
      this.filterFields.forEach((sec) => {
        sec.forms.forEach((item) => {
          if (
            item.fieldType == 'Photo' ||
            item.fieldType == 'Signature' ||
            item.fieldType == 'docUpload'
          ) {
            if (item['value']) {
              arraids.push(item['value']);
            }
          }
        });
      });
      this.getAllUrl(arraids);

      // }
      this.getFormfilter();
      //this.filterForm = JSON.parse(atob(this.util.getFormGroupStorage(tabsectionId)!!))
      this.loading = false;
      this.ref.detectChanges();
      return;
    }

    const param = new FormData();
    param.append('id', tabsectionId);
    if (
      localStorage.getItem('draft_id') &&
      this.reapeatarray.includes(tabsectionId)
    )
      param.append('draftid', atob(localStorage.getItem('draft_id') + ''));
    else if (
      this.repeatcustomerDraft &&
      this.repeatcustomerDraft != 'undefined'
    )
      param.append('draftid', this.repeatcustomerDraft);
    this.popupid = atob(localStorage.getItem('draft_id') + '');

    this.http.post(this.api.CREATEFORMUI, param).subscribe(
      (res) => {
        this.sectionId = id;
        this.util.setFormArrayStorage(
          tabsectionId,
          btoa(
            unescape(
              encodeURIComponent(JSON.stringify(this.arrUnique(res['data'])))
            )
          )
        );
        this.filterFields = JSON.parse(
          atob(this.util.getFormArrayStorage(tabsectionId)!!)
        ); //this.arrUnique(res['data']);
        if (this.filterFields.length > 0) {
          this.filterFields.map((item, i) => {
            if (i == 0) {
              this.filterFields[i].isActive = true;
            } else {
              this.filterFields[i].isActive = false;
            }
          });
        }
        this.getFilnalError();
        this.fullPageLoader = false;
        let arraids: any = [];
        if (
          localStorage.getItem('draft_id') ||
          (this.repeatcustomerDraft && this.repeatcustomerDraft != 'undefined')
        ) {
          this.filterFields.forEach((sec) => {
            sec.forms.forEach((item) => {
              if (
                item.fieldType == 'Photo' ||
                item.fieldType == 'Signature' ||
                item.fieldType == 'docUpload'
              ) {
                if (item['value']) {
                  arraids.push(item['value']);
                }
              }
            });
          });
          this.getAllUrl(arraids);
        }
        console.log(this.filterFields);
        this.getFormfilter();
        this.loading = false;
        this.ref.detectChanges();
      },
      (error) => {
        console.log('Error', error);
        this.loading = false;
        this.fullPageLoader = false;
        // this.toastr.error(error.message)
      }
    );
  }
  getFilnalError() {
    let sessionError = sessionStorage.getItem('errorMessage');
    if (sessionError) {
      let finalError = JSON.parse(atob(sessionError));
      if (this.isArray(finalError)) {
        finalError.forEach((final) => {
          this.filterFields.forEach((element, i) => {
            if (final.id == element.id) {
              final.forms.forEach((finalform) => {
                element.forms.forEach((fieldform, j) => {
                  if (finalform.id == fieldform.id) {
                    this.filterFields[i]['forms'][j].finalerror = true;
                    this.filterFields[i]['forms'][j].finalerrormessage =
                      finalform.errormessage;
                  }
                });
              });
            }
          });
        });
      }
    }

    console.log('Form Fields:', this.filterFields);
  }

  changeError(id) {
    let sessionError = sessionStorage.getItem('errorMessage');
    if (sessionError) {
      let finalError = JSON.parse(atob(sessionError));
      if (this.isArray(finalError)) {
        finalError.forEach((final) => {
          this.filterFields.forEach((element, i) => {
            if (final.id == element.id) {
              final.forms.forEach((finalform) => {
                element.forms.forEach((fieldform, j) => {
                  if (finalform.id == id) {
                    this.filterFields[i]['forms'][j].finalerror = false;
                    this.filterFields[i]['forms'][j].finalerrormessage =
                      finalform.errormessage;
                  }
                });
              });
            }
          });
        });
      }
    }
  }
  isArray(arr) {
    return arr instanceof Array;
  }
  getFormfilter() {
    this.filterForm = this.generateFilterForm();

    this.filterFields.forEach((sec, i) => {
      this.filterFields[i].forms.forEach((element, j) => {
        if (element.fieldName == 'PaymentType') {
          this.generatePaymentFilterForm(sec.name, element.additionalFields, {
            target: {
              value:
                element.value != undefined && element.value != ''
                  ? element.value
                  : 1,
            },
          });
        }
      });
    });
    //validation dirty
    this.filterFields.map((item) => {
      item.forms.map((element) => {
        if (element.value)
          this.filterForm.controls[item.name].controls[
            element.fieldName
          ].markAsDirty();
      });
    });
  }

  arrUnique(arr) {
    var cleaned: any = [];
    arr.forEach((itm) => {
      var unique = true;
      cleaned.forEach((itm2) => {
        if (itm.name == itm2['name']) unique = false;
      });
      if (unique) cleaned.push(itm);
    });
    return cleaned;
  }
  selectAddress(e, formgroup, fieldName) {
    console.log('placeid===', e);
    const formData = new FormData();
    formData.append('placeid', e.place_id);
    this.http.post(this.api.PLACEDETAILS, formData).subscribe(
      (data: any) => {
        // console.log(data['data']['result'].geometry.location.lat)
        // this.listAddress =data['data']['predictions']
        this.filterFields.map((item) => {
          if (item.name == formgroup) {
            item.forms.map((element) => {
              if (fieldName == element.fieldName) {
                element.value = e.description;
                this.filterForm.controls[formgroup].controls[
                  fieldName
                ].setValue(e.description);
                this.filterForm.controls[formgroup].controls[
                  'Latitude'
                ].setValue(data['data']['result'].geometry.location.lat);
                this.filterForm.controls[formgroup].controls[
                  'Longitude'
                ].setValue(data['data']['result'].geometry.location.lng);
              }
            });
            console.log('item.forms', item.forms);
          }
        });
        this.util.setFormArrayStorage(
          this.tabsData[this.sectionId].id,
          btoa(JSON.stringify(this.filterFields))
        );
        console.log(this.filterFields);
      },
      (errors) => {
        this.filterFields.map((item) => {
          if (item.name == formgroup) {
            item.forms.map((element) => {
              if (fieldName == element.fieldName) {
                element.value = e.description;
                this.filterForm.controls[formgroup].controls[
                  fieldName
                ].setValue(e.description);
                this.filterForm.controls[formgroup].controls[
                  'Latitude'
                ].setValue('');
                this.filterForm.controls[formgroup].controls[
                  'Longitude'
                ].setValue('');
              }
            });
            console.log('item.forms', item.forms);
          }
        });
        this.util.setFormArrayStorage(
          this.tabsData[this.sectionId].id,
          btoa(JSON.stringify(this.filterFields))
        );
        console.log(this.filterFields);
      }
    );
  }
  changeInputValue(event, formgroup, fieldName, datetype?) {
    this.filterFields.map((item) => {
      if (item.name == formgroup) {
        item.forms.map((element) => {
          if (element.fieldName == fieldName) {
            if (datetype == 'Calendar') {
              console.log('id_date', event);
              element.value = event;
              this.filterForm.controls[formgroup].controls[
                element.fieldName
              ].setValue(event);
              //element.value = this.filterForm.controls[formgroup].controls[element.fieldName].value
            } else {
              element.value = event.target ? event.target.value : event;
            }
            if (datetype == 'Address') {
              const formData = new FormData();
              formData.append('input', element.value);
              this.http
                .post(this.api.PLACEAUTOCOMPLETE, formData)
                .subscribe((data) => {
                  console.log(data['data']);
                  this.listAddress = data['data']['predictions'];
                });
              //this.filterForm.controls[formgroup].controls[element.fieldName].setValue(event.target ? event.target.value : event)
            }
            if (datetype == 'Autocomplete') {
              this.filterForm.controls[formgroup].controls[
                element.fieldName
              ].setValue(event.target ? event.target.value : event);
            }
            if (
              (datetype && datetype == 'Amount') ||
              element.fieldName == 'HowLongInMonths' ||
              element.fieldName == 'HowLongMonths'
            ) {
              if (
                Number(event.target ? event.target.value : event) >
                element.maxValue
              ) {
                this.filterForm.controls[formgroup].controls[
                  fieldName
                ].setErrors({ maxVal: true });
              } else {
                this.removeError(
                  this.filterForm.controls[formgroup].controls[fieldName],
                  'maxVal'
                );
              }
              if (
                element.fieldName == 'ApplicantGrossSalary' ||
                element.fieldName == 'ApplicantNetSalary'
              ) {
                if (
                  Number(
                    this.filterForm.controls[formgroup].controls[
                      'ApplicantNetSalary'
                    ].value
                  ) >
                  Number(
                    this.filterForm.controls[formgroup].controls[
                      'ApplicantGrossSalary'
                    ].value
                  )
                ) {
                  if (element.fieldName == 'ApplicantGrossSalary')
                    this.filterForm.controls[formgroup].controls[
                      fieldName
                    ].setErrors({ grossLow: true });
                  else
                    this.filterForm.controls[formgroup].controls[
                      fieldName
                    ].setErrors({ grossHigh: true });
                } else {
                  this.removeError(
                    this.filterForm.controls[formgroup].controls[
                      'ApplicantGrossSalary'
                    ],
                    'grossLow'
                  );
                  this.removeError(
                    this.filterForm.controls[formgroup].controls[
                      'ApplicantNetSalary'
                    ],
                    'grossHigh'
                  );
                }
              }
            }
          }
        });
      }
    });
    this.util.setFormArrayStorage(
      this.tabsData[this.sectionId].id,
      btoa(JSON.stringify(this.filterFields))
    );
    console.log('filterFields', this.filterFields);
  }

  changeDateInputValue(event, formgroup, fieldName, datetype?) {
    console.log(
      event.target ? event.target.value : event,
      formgroup,
      fieldName
    );
    this.filterFields.map((item) => {
      if (item.name == formgroup) {
        item.forms.map((element) => {
          if (element.fieldName == fieldName) {
            let val = event.target ? event.target.value : event;
            // if (fieldName == 'HowLongAtCurrentAddress') {
            if (datetype == '1') {
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressY'
              ].setValue(val);
            } else {
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressM'
              ].setValue(val);
            }
            let year =
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressY'
              ].value;
            let month =
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressM'
              ].value;
            if (!year) {
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressY'
              ].setValue('0');
              year =
                this.filterForm.controls[formgroup].controls[
                  'HowLongAtCurrentAddressY'
                ].value;
            }
            if (!month) {
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressM'
              ].setValue('0');
              month =
                this.filterForm.controls[formgroup].controls[
                  'HowLongAtCurrentAddressM'
                ].value;
            }
            val = year + '/' + month;
            this.filterForm.controls[formgroup].controls[fieldName].setValue(
              val
            );
            element.value = val;

            if (
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressY'
              ].value &&
              this.filterForm.controls[formgroup].controls[
                'HowLongAtCurrentAddressM'
              ].value
            ) {
              if (year == 0 && month == 0)
                this.filterForm.controls[formgroup].controls[
                  fieldName
                ].setErrors({ YM: true });
              else
                this.removeError(
                  this.filterForm.controls[formgroup].controls[fieldName],
                  'YM'
                );
            }
          }
        });
        console.log(item.forms);
      }
    });
    this.util.setFormArrayStorage(
      this.tabsData[this.sectionId].id,
      btoa(JSON.stringify(this.filterFields))
    );
    console.log(this.filterFields);
  }

  handleAddressChange(event, formgroup, fieldName, dateval?) {
    let val: any = document.getElementById(dateval);

    this.userAddress = event.formatted_address;
    // this.userAddress = event
    this.userLatitude = event.geometry.location.lat();
    this.userLongitude = event.geometry.location.lng();

    this.filterFields.map((item) => {
      if (item.name == formgroup) {
        item.forms.map((element) => {
          if (fieldName == element.fieldName) {
            element.value = val.value;
            this.filterForm.controls[formgroup].controls[fieldName].setValue(
              val.value
            );
            this.filterForm.controls[formgroup].controls['Latitude'].setValue(
              event.geometry.location.lat()
            );
            this.filterForm.controls[formgroup].controls['Longitude'].setValue(
              event.geometry.location.lng()
            );
          }
        });
        console.log('item.forms', item.forms);
      }
    });
    this.util.setFormArrayStorage(
      this.tabsData[this.sectionId].id,
      btoa(JSON.stringify(this.filterFields))
    );
    console.log(this.filterFields);
  }

  removeError(control: AbstractControl, error: string) {
    const err = control.errors; // get control errors
    if (err) {
      delete err[error]; // delete your own error
      if (!Object.keys(err).length) {
        // if no errors left
        control.setErrors(null); // set control errors to null making it VALID
      } else {
        control.setErrors(err); // controls got other errors so set them back
      }
    }
  }

  generateFilterForm(): FormGroup {
    const baseForm = this.fb.group({});
    this.filterFields.forEach((field: { name: string }, i) => {
      baseForm.addControl(field.name, this.generateFormGroup(baseForm, field));
    });
    return baseForm;
  }
  generatePaymentFilterForm(formgroup, additionalFields, event, fieldName?) {
    if (fieldName) this.changeInputValue(event, formgroup, fieldName);

    console.log(formgroup, additionalFields, event, this.filterForm.controls);
    additionalFields[event.target.value - 1].forms.forEach((field) => {
      if (field.isMantadry == '1' && field.fieldType == 'Dropdown') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldType == 'RadioButton') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldName == 'Expiry') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
            this.validateExpiry.bind(this),
          ])
        );
      } else if (field.isMantadry == '1' && field.dataType == 'Amount') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value, [
            Validators.required,
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
            this.validateNumber.bind(this),
          ])
        ); //Validators.minLength(field.minLength != "" ? Number(field.minLength) : 0), Validators.maxLength(field.maxLength != "" ? Number(field.maxLength) : 0),
      } else if (
        field.isMantadry == '1' &&
        (field.fieldName == 'Address' || field.fieldName == 'EmployerAddress')
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
        this.filterForm.controls[formgroup].addControl(
          'Latitude',
          new FormControl('')
        );
        this.filterForm.controls[formgroup].addControl(
          'Longitude',
          new FormControl('')
        );
      } else if (
        field.isMantadry == '1' &&
        (field.fieldName == 'HowLongAtCurrentAddress' ||
          field.fieldType == 'MonthYearPicker')
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '0/0', [
            Validators.required,
          ])
        );
        this.filterForm.controls[formgroup].addControl(
          'HowLongAtCurrentAddressY',
          new FormControl(field.value ? field.value.split('/')[0] : '', [
            Validators.required,
          ])
        );
        this.filterForm.controls[formgroup].addControl(
          'HowLongAtCurrentAddressM',
          new FormControl(
            field.value && field.value.split('/').length == 2
              ? field.value.split('/')[1]
              : '',
            [Validators.required]
          )
        );
      } else if (field.isMantadry == '1' && field.dataType == 'Integer') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
            this.validateNumber.bind(this),
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldName == 'Email') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.email,
            Validators.email,
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldType == 'Calendar') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            isValidDate,
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldType == 'Autocomplete') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '1') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'Dropdown') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldName == 'HowLongAtCurrentAddress' ||
          field.fieldType == 'MonthYearPicker')
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '0/0', [
            Validators.required,
          ])
        );
        this.filterForm.controls[formgroup].addControl(
          'HowLongAtCurrentAddressY',
          new FormControl(field.value ? field.value.split('/')[0] : '', [
            Validators.required,
          ])
        );
        this.filterForm.controls[formgroup].addControl(
          'HowLongAtCurrentAddressM',
          new FormControl(
            field.value && field.value.split('/').length == 2
              ? field.value.split('/')[1]
              : '',
            [Validators.required]
          )
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'RadioButton') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldName == 'Address' || field.fieldName == 'EmployerAddress')
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
        this.filterForm.controls[formgroup].addControl(
          'Latitude',
          new FormControl('')
        );
        this.filterForm.controls[formgroup].addControl(
          'Longitude',
          new FormControl('')
        );
      } else if (
        field.isMantadry == '0' &&
        field.fieldType == 'TextField' &&
        field.fieldName == 'Email'
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'Calendar') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'Autocomplete') {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldType == 'Photo' ||
          field.fieldType == 'Signature' ||
          field.fieldType == 'docUpload')
      ) {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            this.imagvalidator.validate,
          ])
        );
      } else {
        this.filterForm.controls[formgroup].addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      }
      this.filterFields.forEach((sec, i) => {
        // if (sec.name == "Details") {
        console.log(field);
        let temp = false;
        sec.forms.forEach((two, j) => {
          if (field.fieldName == two.fieldName) {
            temp = true;
          }
        });
        if (!temp) this.filterFields[i].forms.push(field);
        // }
      });
    });

    additionalFields[event.target.value == 1 ? 1 : 0].forms.forEach((field) => {
      this.filterFields.forEach((sec, i) => {
        // if (sec.name == "Details") {
        this.filterFields[i].forms.forEach((element, j) => {
          if (element.fieldName == field.fieldName) {
            this.filterFields[i].forms.splice(j, 1);

            if (field.fieldName == 'Email') {
              this.filterForm.controls[formgroup].removeControl(
                field.fieldName
              );
            } else if (field.fieldType == 'Calendar') {
              this.filterForm.controls[formgroup].removeControl(
                field.fieldName
              );
            } else {
              this.filterForm.controls[formgroup].removeControl(
                field.fieldName
              );
            }
          }
        });
        // }
      });
    });
  }

  generateFormGroup(baseForm: FormGroup, field: any): FormGroup {
    if (field.forms) {
      const formGroup = this.fb.group({});

      field.forms.forEach((item: { fieldName: string }) => {
        formGroup.addControl(
          item.fieldName,
          this.generateFormGroup(formGroup, item)
        );
      });
      return formGroup;
    } else {
      if (field.isMantadry == '1' && field.fieldType == 'Dropdown') {
        if (field.fieldName == 'PaymentType') {
          baseForm.addControl(
            field.fieldName,
            new FormControl(
              field.value != undefined && field.value != '' ? field.value : '1',
              [Validators.required]
            )
          );
        } else {
          baseForm.addControl(
            field.fieldName,
            new FormControl(field.value != undefined ? field.value : '', [
              Validators.required,
            ])
          );
        }
      } else if (field.isMantadry == '1' && field.fieldName == 'Expiry') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value, [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
            this.validateExpiry.bind(this),
          ])
        );
      } else if (field.isMantadry == '1' && field.dataType == 'Amount') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value, [
            Validators.required,
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
            Validators.min(field.minValue ? Number(field.minValue) : 1),
            this.validateNumber.bind(this),
          ])
        ); //Validators.minLength(field.minLength != "" ? Number(field.minLength) : 0), Validators.maxLength(field.maxLength != "" ? Number(field.maxLength) : 0),
      } else if (field.isMantadry == '1' && field.dataType == 'Integer') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value, [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        ); //Validators.minLength(field.minLength != "" ? Number(field.minLength) : 0), Validators.maxLength(field.maxLength != "" ? Number(field.maxLength) : 0),
      } else if (field.isMantadry == '1' && field.fieldType == 'RadioButton') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
          ])
        );
      } else if (
        field.isMantadry == '1' &&
        (field.fieldName == 'HowLongAtCurrentAddress' ||
          field.fieldType == 'MonthYearPicker')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '0/0', [
            Validators.required,
          ])
        );
        baseForm.addControl(
          'HowLongAtCurrentAddressY',
          new FormControl(field.value ? field.value.split('/')[0] : '', [
            Validators.required,
          ])
        );
        baseForm.addControl(
          'HowLongAtCurrentAddressM',
          new FormControl(
            field.value && field.value.split('/').length == 2
              ? field.value.split('/')[1]
              : '',
            [Validators.required]
          )
        );
      } else if (
        field.isMantadry == '1' &&
        (field.fieldName == 'Address' || field.fieldName == 'EmployerAddress')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
        baseForm.addControl('Latitude', new FormControl(''));
        baseForm.addControl('Longitude', new FormControl(''));
      } else if (
        field.isMantadry == '1' &&
        field.fieldType == 'TextField' &&
        field.fieldName == 'Email'
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldType == 'Autocomplete') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (
        field.isMantadry == '1' &&
        field.fieldType == 'TextField' &&
        field.dataType == 'Phonenumber'
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.pattern('^(?:(?:[1-9][0-9]*)|0)$'),
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '1' && field.fieldType == 'Calendar') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            isValidDate,
          ])
        );
      } else if (
        field.isMantadry == '1' &&
        (field.fieldType == 'Photo' ||
          field.fieldType == 'Signature' ||
          field.fieldType == 'docUpload')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            this.imagvalidator.validate,
          ])
        );
      } else if (field.isMantadry == '1') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.required,
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        ); //
      } else if (field.isMantadry == '0' && field.fieldType == 'Dropdown') {
        if (field.fieldName == 'PaymentType') {
          baseForm.addControl(
            field.fieldName,
            new FormControl(
              field.value != undefined && field.value != '' ? field.value : '1'
            )
          );
        } else {
          baseForm.addControl(
            field.fieldName,
            new FormControl(field.value != undefined ? field.value : '')
          );
        }
      } else if (
        field.isMantadry == '0' &&
        field.fieldType == 'TextField' &&
        field.dataType == 'Phonenumber'
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.pattern('^(?:(?:[1-9][0-9]*)|0)$'),
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldName == 'HowLongAtCurrentAddress' ||
          field.fieldType == 'MonthYearPicker')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '0/0')
        );
        baseForm.addControl(
          'HowLongAtCurrentAddressY',
          new FormControl(field.value ? field.value.split('/')[0] : '')
        );
        baseForm.addControl(
          'HowLongAtCurrentAddressM',
          new FormControl(
            field.value && field.value.split('/').length == 2
              ? field.value.split('/')[1]
              : ''
          )
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'Autocomplete') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'RadioButton') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldName == 'Address' || field.fieldName == 'EmployerAddress')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
        baseForm.addControl(
          'Latitude',
          new FormControl(field.value != undefined ? field.value : '')
        );
        baseForm.addControl(
          'Longitude',
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (
        field.isMantadry == '0' &&
        field.fieldType == 'TextField' &&
        field.fieldName == 'Email'
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      } else if (field.isMantadry == '0' && field.fieldType == 'Calendar') {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '')
        );
      } else if (
        field.isMantadry == '0' &&
        (field.fieldType == 'Photo' ||
          field.fieldType == 'Signature' ||
          field.fieldType == 'docUpload')
      ) {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            this.imagvalidator.validate,
          ])
        );
      } else {
        baseForm.addControl(
          field.fieldName,
          new FormControl(field.value != undefined ? field.value : '', [
            Validators.minLength(
              field.minLength != '' ? Number(field.minLength) : 0
            ),
            Validators.maxLength(
              field.maxLength != '' ? Number(field.maxLength) : 0
            ),
          ])
        );
      }
    }

    return baseForm;
  }
  toggleAccordian(index) {
    console.log(index);
    if (this.filterFields.length <= 1) {
      return;
    }
    // const element = event.target;
    // element.classList.toggle("active");
    // if (this.filterFields[index].isActive) {
    //   this.filterFields[index].isActive = false;
    // } else {
    //   this.filterFields[index].isActive = true;
    // }
    this.filterFields.map((item, i) => {
      if (index == i) {
        this.filterFields[i].isActive = true;
      } else {
        this.filterFields[i].isActive = false;
      }
    });

    // const panel = element.nextElementSibling;
    // if (panel.style.maxHeight) {
    //   panel.style.maxHeight = null;
    // } else {
    //   panel.style.maxHeight = panel.scrollHeight + "px";
    // }
    console.log(this.filterFields[index]);
  }
  formSubmit() {
    console.log('Form Values_:', this.filterForm);
    // console.log("Section_Id:", this.filterForm);

    this.submitted1 = true;
    if (this.filterForm.invalid) {
      for (let i = 0; i < this.filterFields.length; i++) {
        if (this.filterForm.controls[this.filterFields[i].name].invalid) {
          this.toggleAccordian(i);
          break;
        }
      }
      return;
    }
    this.fullPageLoader = true;
    this.showForm = true;
    window.scroll(0, 0);

    const formData = new FormData();
    let data = JSON.stringify(this.filterForm.value);
    formData.append('loanjson', data);
    formData.append('loan_type_id', this.selectedId);
    formData.append('section_id', this.tabsData[this.sectionId].id);

    if (localStorage.getItem('draft_id')) {
      formData.append('DraftId', atob(localStorage.getItem('draft_id') + ''));
      this.apihttp.post(this.api.LOANDRAFTUPDATE, formData).subscribe(
        (res) => {
          // this.filterFields = res['data'] as JSON;
          // console.log(this.filterFields);
          this.reapeatarray.push(this.selectedId);
          if (this.sectionId != this.tabsData.length - 1) {
            this.getTaps(this.selectedId, 2);
            //  this.toastr.success(res['message'], '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          } else {
            let tab = JSON.parse(atob(localStorage.getItem('selecttabs')!!));
            let updatetap = tab.data;
            updatetap.map((t) => {
              if (t.id == tab.data[this.sectionId].id) {
                t.status = '2';
              }
            });
            this.tabsData = updatetap;
            let assignData = Object.assign(tab, { data: updatetap });
            localStorage.setItem(
              'selecttabs',
              btoa(JSON.stringify(assignData))
            );
            let formfinalsubmited = false;
            for (let i = 0; i < updatetap.length; i++) {
              if (
                updatetap[i]['status'] != '2' &&
                updatetap[i]['isMantadry'] != '0'
              ) {
                formfinalsubmited = true;
                break;
              }
            }

            if (!formfinalsubmited) {
              if (!this.livenesStatus && this.liveflage == 'N') {
                this.fullPageLoader = false;
                this.toastr.error('Please complete Liveness Test first', '', {
                  positionClass: 'toast-top-center',
                  closeButton: true,
                  enableHtml: true,
                });
                return;
              }
              let userId = JSON.parse(localStorage.getItem('user')!!).userId;

              const formfinalData = new FormData();
              // formfinalData.append("UserId", userId);
              // formfinalData.append("SectionId", this.selectedId);
              formfinalData.append(
                'draftid',
                atob(localStorage.getItem('draft_id') + '')
              );

              this.apihttp
                .post(this.api.LOANINSERTFINAL, formfinalData)
                .subscribe(
                  (res) => {
                    console.log(res);
                    this.fullPageLoader = false;
                    sessionStorage.removeItem('errorMessage');
                    setTimeout(() => {
                      this.submitModal(res['message']);
                      //  this.submitModal()
                    }, 100);
                  },
                  (error) => {
                    this.fullPageLoader = false;
                    if (error.errorResponse.status == 400) {
                      sessionStorage.setItem(
                        'errorMessage',
                        btoa(error.errorResponse.error.ErrorHandle)
                      );
                      if (error.errorResponse.message)
                        this.toastr.error(error.errorResponse.message, '', {
                          positionClass: 'toast-top-center',
                          closeButton: true,
                          enableHtml: true,
                        });
                    }
                    console.log('last final =', error);
                  }
                );
            } else {
              for (let i = 0; i < this.tabsData.length; i++) {
                if (this.tabsData[i]['status'] != '2') {
                  this.sectionId = i;
                  break;
                }
              }
              //  this.toastr.success(res['message'], '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
              this.getTaps(this.selectedId, 3);
            }
          }
          this.submitted1 = false;
        },
        (error) => {
          this.submitted1 = false;
          this.fullPageLoader = false;
          console.log('Error', error);
        }
      );
    } else {
      this.apihttp.post(this.api.LOANDRAFTINSERT, formData).subscribe(
        (res) => {
          // this.filterFields = res['data'];
          // console.log(this.filterFields);
          this.reapeatarray.push(this.selectedId);
          localStorage.setItem('draft_id', btoa(res['data']['draftId']));
          if (this.sectionId != this.tabsData.length - 1) {
            this.getTaps(this.selectedId, 2);
            //   this.toastr.success(res['message'], '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          } else {
            for (let i = 0; i < this.tabsData.length; i++) {
              if (this.tabsData[i]['status'] != '2') {
                this.sectionId = i;
                break;
              }
            }
            //  this.toastr.success(res['message'], '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
            this.getTaps(this.selectedId, 3);
            // this.submitModal(submitmodal)
            // this.fullPageLoader = false;
          }

          this.submitted1 = false;
        },
        (error: any) => {
          this.submitted1 = false;
          this.fullPageLoader = false;
          console.log('Error', error);
        }
      );
    }
  }

  getDownloadUrl(i, formgroup, feildname) {
    let formgurl = this.filterFields.filter((item) => item.name == formgroup);
    let url = formgurl[0].forms.filter((item) => item.fieldName == feildname);
    let durl = '0';
    if (url.length > 0) durl = url[0].sampleFile;
    return durl;
  }

  getAllUrl(ids) {
    if (ids.length > 0) {
      this.fileLoader = true;
      const formData = new FormData();
      formData.append('json', JSON.stringify(ids));

      this.apihttp.post(this.api.IMAGEMULTIUPLOAD, formData).subscribe(
        (res) => {
          this.fileLoader = false;
          if (res['data'].length > 0)
            res['data'].forEach((element) => {
              this.imageJson.push(element);
            });
        },
        (error) => {
          this.fileLoader = false;
        }
      );
    }
  }
  geturl(id) {
    let url = this.imageJson.filter((item) => item.id == id);
    if (url.length > 0) return url[0].url;
    return false;
  }

  // For Image Upload
  fileChange(event: any, formgroup, feildname, type) {
    if (type == 2 || (event.target && event.target.files.length > 0)) {
      const formData = new FormData();
      const file = type == 1 ? event.target.files[0] : event;
      this.filterForm.controls[formgroup].controls[feildname].setValue(
        file['name']
      );
      this.filterForm.controls[formgroup].controls[feildname].markAsDirty();
      formData.append('image', file, file['name']);
      if (
        !this.imagvalidator.validate(
          this.filterForm.controls[formgroup].controls[feildname]
        )
      ) {
        if (type == 1) {
          const max_size = 25097152;
          console.log(
            event.target.files[0].size,
            (event.target.files[0].size / (1024 * 1024)).toFixed(2)
          );
          if (
            event.target.files[0].size / (1024 * 1024) >
            max_size / (1024 * 1024)
          ) {
            console.log(max_size / (1024 * 1024) + 'Mb');
            this.filterForm.controls[formgroup].controls[feildname].setErrors({
              filleSizeincorrect: true,
            });
            return;
          } else {
            this.filterForm.controls[formgroup].controls[feildname].setErrors(
              null
            );
          }
        }
        this.fileLoader = true;
        this.fieldname.push(feildname);
        this.apihttp.post(this.api.UPLOADFILE, formData).subscribe(
          (res) => {
            console.log(res);
            const index = this.fieldname.indexOf(feildname, 0);
            this.imageJson.push({
              id: res['data']['file_name'],
              url: res['data']['path'],
            });
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }
            this.changeInputValue(
              res['data']['file_name'],
              formgroup,
              feildname
            );
            this.filterForm.controls[formgroup].controls[feildname].setValue(
              res['data']['file_name']
            );
            this.fileLoader = false;
          },
          (err) => {
            console.log(err);
            const index = this.fieldname.indexOf(feildname, 0);
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }
            this.filterForm.controls[formgroup].controls[feildname].setValue(
              ''
            );
            this.fileLoader = false;
          }
        );
      }
    }
  }

  saveImage(data, signformgroup, signfieldname) {
    const imageName = 'sign' + Date.now() + '.png';
    const imageFile = new File([data], imageName, { type: 'image/png' });
    this.fileChange(imageFile, signformgroup, signfieldname, 2);
    this.modalService.dismissAll();
  }

  triggerModal(formgroup, feildname, e) {
    this.signevent = e;
    this.signformgroup = formgroup;
    this.signfieldname = feildname;
    this.modalService
      .open(SignpadpopupComponent, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (res) => {
          console.log(res);
          const blob = base64ToFile(res);
          this.saveImage(blob, formgroup, feildname);
          this.closeModal = `Closed with: ${res}`;
        },
        (res) => {
          console.log(res);
          this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        }
      );
  }

  submitModal(content) {
    this.popupid = atob(localStorage.getItem('draft_id') + '');
    let splitmessage = content.split('#' + this.popupid);
    const modalRef = this.modalService.open(SuccessPopupComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    modalRef.componentInstance.split1Content = splitmessage[0];
    modalRef.componentInstance.popupid = '#' + this.popupid;
    modalRef.componentInstance.split2Content = splitmessage[1];
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  cancelform() {
    // this.router.navigate(['my-loans']);
    this.filterFields.map((item) => {
      item.forms.map((element) => {
        element.value = '';
      });
    });
    this.util.removeFormArrayStorage(this.tabsData[this.sectionId].id);
    //this.util.setFormArrayStorage(this.tabsData[this.sectionId].id, btoa(JSON.stringify(this.filterFields)))
    if (this.tabsData && this.tabsData.length > 0) {
      this.goTo(this.sectionId);
    }
    // this.getFormfilter()
  }

  getMaxLength(len) {
    return Number(len) + 7;
  }
  SelectedTab(tab: any) {
    this.sectionId = tab;
    this.showForm = false;
    this.comentLoading = true;
    const formData = new FormData();
    formData.append('loan_id', this.commentloanId);
    this.apihttp.post(this.api.LOANCOMMENTS, formData).subscribe(
      (res: any) => {
        this.allComments = res.data;
        this.comentLoading = false;
      },
      (error: any) => {
        this.comentLoading = false;
      }
    );
  }

  addComment() {
    this.router.navigate(['my-loans', 'add-comment', this.commentloanId]);
  }
  viewComment(id: any) {
    this.commentId = id;
    this.router.navigate([
      'my-loans',
      'view-comment',
      this.commentId,
      this.commentloanId,
    ]);
  }
  goBack() {
    this.router.navigate(['my-loans', 'my-loans']);
  }
  getInt(e, i) {
    if (e.length == 0) {
      return false;
    }
    return e == i;
  }

  validateNumber(control: FormControl) {
    //revised to reflect null as an acceptable value
    if (!control.value || control.value > 0) return null;

    // check to see if the control value is no a number
    if (control.value == 0) {
      return { zeroAbove: true };
    }

    return null;
  }

  validateExpiry(control: FormControl) {
    let year = new Date().getFullYear();
    let date = new Date().getMonth() + 1;
    console.log('date', date);
    //revised to reflect null as an acceptable value
    if (control.value && control.value.length > 6) {
      let controlyear = control.value.split('/')[1];
      let controldate = control.value.split('/')[0];
      if (year > controlyear) {
        return { aboveYear: true };
      }
      if (year == controlyear && date >= controldate) {
        return { aboveDate: true };
      }

      return null;
    }

    return null;
  }
  getAmountMask(len, strlen) {
    let count = '1';
    for (let i = 0; i < len - 1; i++) {
      count += '1';
    }
    //  console.log(strlen)
    return this.numberWithCommas(count, strlen);
  }
  numberWithCommas(x, sl) {
    let e = x.substring(0, sl).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    let t = e + x.substring(sl, x.length);
    return t.replaceAll('1', '0');
  }

  downloadMyFile(hlink) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute(
      'href',
      'http://designs.mydeievents.com/jq-3d-flip-book/books/pdf/aqua_imagica_visit_learnings.pdf'
    );
    link.setAttribute('download', 'aqua_imagica_visit_learnings.pdf');
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  ngOnDestroy(): void {
    sessionStorage.removeItem('errorMessage');
    localStorage.removeItem('draft_id');
    // localStorage.removeItem('continueLoan')
  }
  transform(value: string): string {
    let first = value.substr(0, 1).toUpperCase();
    let str2 = value.slice(1).toLowerCase();
    return first + str2;
  }

  // For Web Camera
  async pickLivenessTest() {
    this.livenessTestBlock = true;
    // this.toggleWebcam()
    //Record Video
    // navigator.mediaDevices
    //   .getUserMedia({
    //     video: {
    //       width: 360
    //     }
    //   })
    //   .then(stream => {
    //     this.videoElement = this.videoElementRef.nativeElement;
    //     this.recordVideoElement = this.recordVideoElementRef.nativeElement;
    //     this.stream = stream;
    //     this.videoElement.srcObject = this.stream;
    //   });
  }

  async startpickLivenessTest() {
    this.statrrecord=true
    await navigator.mediaDevices
      .getUserMedia({
        video: {
          width: 360,
        },
      })
      .then((stream) => {
        this.videoElement = this.videoElementRef.nativeElement;
        this.recordVideoElement = this.recordVideoElementRef.nativeElement;
        this.stream = stream;
        this.videoElement.srcObject = this.stream;
        this.starRec();
      }).catch(err=>{
        this.statrrecord =false
        console.log("errFi--",err);
      });
  }

  starRec() {
    this.recordedBlobs = [];
    let options: any = { mimeType: 'video/webm' };
    this.statrrecord =false
    try {
      this.mediaRecorder = new MediaRecorder(this.stream, options);
    

    this.mediaRecorder.start(); // collect 100ms of data
    this.isRecording = !this.isRecording;
    this.onDataAvailableEvent();
    this.onStopRecordingEvent();
    this.timerId = setTimeout(() => {
      this.stopRecording();
    }, 16000);
  } catch (err) {
    console.log("err--",err);
  }
  }

  // toggle webcam on/off
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    // width: {ideal: 1024},
    // height: {ideal: 576}
  };
  public errors: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage?: WebcamImage;

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<
    boolean | string
  >();

  public triggerSnapshot(): void {
    this.trigger.next();
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  // UPLOAD LIVENESS

  uploadImage() {
    const blob = this.recordedBlobs;
    const imageName = 'livenes' + Date.now() + '.webm';
    const imageFile = new File(blob, imageName, { type: 'video/webm' });
    this.fullPageLoader = true;
    const formData = new FormData();
    formData.append('image', imageFile, imageFile['name']);
    this.apihttp.post(this.api.UPLOADFILE, formData).subscribe(
      (res) => {
        console.log(res);
        let imagedate: any = this.datepipe.transform(
          new Date(),
          'yyyy-MM-dd hh:mm:ss'
        );
        const formLData = new FormData();
        formLData.append('LivenessVerifiedAt', imagedate);
        formLData.append('LivenessImage', res['data']['file_name']);
        this.apihttp.post(this.api.LIVENESSUPDATE, formLData).subscribe(
          (res1) => {
            console.log(res1);

            const formData = new FormData();
          
            formData.append('loanjson', '{}');
            formData.append('loan_type_id', this.selectedId);
            formData.append('section_id', '50');

            if (localStorage.getItem('draft_id')) {
              formData.append(
                'DraftId',
                atob(localStorage.getItem('draft_id') + '')
              );
              this.apihttp.post(this.api.LOANDRAFTUPDATE, formData).subscribe(
                (data) => {
                  this.livenesStatus = res1['data']['LivenessVerified'];
                  this.fullPageLoader = false;
                  this.toastr.success(res1['data']['message'], '', {
                    positionClass: 'toast-top-center',
                    closeButton: true,
                    enableHtml: true,
                  });
                },
                (err) => {
                  this.fullPageLoader = false;
                }
              );
            } else {
              this.apihttp.post(this.api.LOANDRAFTINSERT, formData).subscribe(
                (res2) => {
                  localStorage.setItem(
                    'draft_id',
                    btoa(res2['data']['draftId'])
                  );
                  this.livenesStatus = res1['data']['LivenessVerified'];
                  this.fullPageLoader = false;
                  this.toastr.success(res1['data']['message'], '', {
                    positionClass: 'toast-top-center',
                    closeButton: true,
                    enableHtml: true,
                  });
                },
                (err) => {
                  this.fullPageLoader = false;
                }
              );
            }
          },
          (err) => {
            console.log(err);
            this.fullPageLoader = false;
          }
        );
      },
      (err) => {
        console.log(err);
        this.fullPageLoader = false;
      }
    );
  }

  // RECORD VIDEO
  async startRecording() {
    this.takeCamera = true;
    this.startpickLivenessTest();
  }
  stopRecording() {
    this.stream.getTracks().forEach((track) => {
      track.stop();
      this.takeCamera = false;
      this.mediaRecorder.stop();
      this.isRecording = !this.isRecording;
    });
    if (this.timerId) clearTimeout(this.timerId);
  }

  playRecording() {
    if (!this.recordedBlobs || !this.recordedBlobs.length) {
      console.log('cannot play.');
      return;
    }
    this.recordVideoElement.play();
  }

  onDataAvailableEvent() {
    try {
      this.mediaRecorder.ondataavailable = (event: any) => {
        if (event.data && event.data.size > 0) {
          this.recordedBlobs.push(event.data);
        }
      };
    } catch (error) {
      console.log(error);
    }
  }

  onStopRecordingEvent() {
    try {
      this.mediaRecorder.onstop = (event: Event) => {
        const videoBuffer = new Blob(this.recordedBlobs, {
          type: 'video/webm',
        });
        this.downloadUrl = window.URL.createObjectURL(videoBuffer); // you can download with <a> tag
        this.recordVideoElement.src = this.downloadUrl;
      };
    } catch (error) {
      console.log(error);
    }
  }
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  // KEYPRESS EVENTS

  keyPressNumbersWithDecimal(event) {
    var charCode = event.which ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }
  keyPressNumbers(event) {
    var charCode = event.which ? event.which : event.keyCode;
    // Only Numbers 0-9
    if (charCode < 48 || charCode > 57) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  forTrn(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  onKeydownEvent(event, formgroup, fieldname) {
    var charCode = event.which ? event.which : event.keyCode;
    if (charCode < 48 || charCode > 57) {
      event.preventDefault();
      return false;
    } else {
      if (
        this.filterForm.controls[formgroup].controls[fieldname].value.length < 2
      ) {
        let monthkeys = ['2', '3', '4', '5', '6', '7', '8', '9'];
        if (
          monthkeys.includes(
            this.filterForm.controls[formgroup].controls[fieldname].value
          )
        )
          this.filterForm.controls[formgroup].controls[fieldname].setValue(
            '0' +
              (this.filterForm.controls[formgroup].controls[fieldname].value !=
              '0'
                ? this.filterForm.controls[formgroup].controls[fieldname].value
                : '')
          );
      }
      if (
        this.filterForm.controls[formgroup].controls[fieldname].value.length ==
        2
      ) {
        this.filterForm.controls[formgroup].controls[fieldname].setValue(
          this.filterForm.controls[formgroup].controls[fieldname].value + '/'
        );
      }
      return true;
    }
  }
  keyPressEmail(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z0-9@._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  keyPressAlphanumeric(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z0-9'\s-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaEmployer(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9\s'&-./()]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressIdAlphanumeric(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z0-9-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  keyPressAmount(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9.]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  onPaste(e) {
    e.preventDefault();
    return false;
  }

  keyPressAlpha(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z\s-']/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  customFilter = function (allEmployers: any[], query: string): any[] {
    return allEmployers.filter((x) =>
      x.toLowerCase().startsWith(query.toLowerCase())
    );
  };
  customFilter1 = function (allEmployers: any[], query: string): any[] {
    return allEmployers.filter((x) =>
      x.description.toLowerCase().startsWith(query.toLowerCase())
    );
  };
}

export const isValidDate = (c: FormControl) => {
  if (c.value != '') {
    let timeDiff = Math.abs(Date.now() - new Date(c.value).getTime());
    let age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    return age >= 18
      ? null
      : {
          validateDate: {
            valid: false,
          },
        };
  } else {
    return null;
  }
};
