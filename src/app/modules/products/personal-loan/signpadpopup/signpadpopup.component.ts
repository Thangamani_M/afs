import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SignaturePad } from 'angular2-signaturepad';

@Component({
  selector: 'app-signpadpopup',
  templateUrl: './signpadpopup.component.html',
  styleUrls: ['./signpadpopup.component.css']
})
export class SignpadpopupComponent implements AfterViewInit {

  public imageDataBase64: string;
  disablebtn=true
  public signaturePadOptions: object = {
    // passed through to szimek/signature_pad constructor
    minWidth: 1,
     canvasWidth: 400,
    // canvasHeight: 400,
    backgroundColor: 'rgb(255, 255, 255)',
    penColor: 'rgb(0, 0, 0)',
  };
  @ViewChild('signatureCanvas', { static: true }) signaturePad: SignaturePad;

  constructor(private dialogRef: NgbModal, public activeModal: NgbActiveModal) {}
 
  // 以下全都是針對手寫板寫的
  ngAfterViewInit() {
    // this.signaturePad is now available
    // this.signaturePad.set('minWidth', 1); // set szimek/signature_pad options at runtime
    // if (window.innerWidth <= 768) {
    //   this.signaturePad.set('canvasWidth', window.innerWidth - 40);
    //   this.signaturePad.set('canvasHeight', window.innerHeight - 250);
    // } else {
    //   this.signaturePad.set('canvasWidth', 800);
    //   this.signaturePad.set('canvasHeight', 400);
    // }
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }

  drawComplete() {
    this.imageDataBase64 = this.signaturePad.toDataURL('image/jpeg');
if(!this.signaturePad.isEmpty()){
   this.disablebtn=false
}
    // will be notified of szimek/signature_pad's onEnd event
  }

  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }

  clear() {
    this.signaturePad.clear();
    this.imageDataBase64 = '';
    this.disablebtn=true
  }

  emitToFather() {
    console.log(this.signaturePad.isEmpty());
    // this.dialogRef.close(this.imageDataBase64);
    // this.dialogRef.dismissAll()
    if(!this.signaturePad.isEmpty())
        this.activeModal.close(this.imageDataBase64);

  }

  closeDialog() {
    this.dialogRef.dismissAll();
  }

}
