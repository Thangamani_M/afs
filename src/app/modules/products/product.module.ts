import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PersonalLoanComponent } from './personal-loan/personal-loan.component';

import { LoanFormComponent } from './personal-loan/loan-form/loan-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';
import { SignaturePadModule } from 'angular2-signaturepad';
import { SignpadpopupComponent } from './personal-loan/signpadpopup/signpadpopup.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { WebcamModule } from 'ngx-webcam';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { SuccessPopupComponent } from './personal-loan/loan-form/success-popup/success-popup.component';
import { CurrencyMaskModule } from "ng2-currency-mask";

const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: PersonalLoanComponent },
      //{ path: 'lo', component: LoanFormComponent },

      {
        path: 'loan-form/:id', component: LoanFormComponent, data: {
          breadcrumb: [
            { label: 'Products/Loan form', url: 'products/loan-form' }]
        }
      }
    ]
  }
]

const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};

@NgModule({
  declarations: [PersonalLoanComponent,
    SuccessPopupComponent,
    SignpadpopupComponent,
    LoanFormComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(maskConfigFunction),
    MatCheckboxModule,
    MatDialogModule,
    SignaturePadModule,
    NgDynamicBreadcrumbModule,
    NgbModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    GooglePlaceModule,
    WebcamModule,
    AutocompleteLibModule,
    MatAutocompleteModule,
    CurrencyMaskModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [FileTypeValidator],
})
export class ProductModule { }
