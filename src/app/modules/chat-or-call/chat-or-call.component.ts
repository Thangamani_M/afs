import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat-or-call',
  templateUrl: './chat-or-call.component.html',
  styleUrls: ['./chat-or-call.component.css']
})
export class ChatOrCallComponent implements OnInit {
  title = 'google-places-autocomplete';
  userAddress: string = ''
  userLatitude: string = ''
  userLongitude: string = ''


  constructor() { }

  ngOnInit(): void {

  }

  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
  }
  openMessenger() {

    // let url = "fb-messenger://user-thread/\(id)"
    // if(url){

    // }

    // let id = "mark.moeykens"
    // if let url = URL(string: "fb-messenger://user-thread/\(id)") {

    //   // Attempt to open in Messenger App first
    //   UIApplication.shared.open(url, options: [: ], completionHandler: {
    //         (success) in

    //         if success == false {
    //     // Messenger is not installed. Open in browser instead.
    //     let url = URL(string: "https://m.me/\(id)")
    //     if UIApplication.shared.canOpenURL(url!) {
    //       UIApplication.shared.open(url!)
    //     }
    //   }
    // })
    // }
  }
  liveChat(){
    window["$zoho"].salesiq.floatwindow.visible('show');
  }


}
