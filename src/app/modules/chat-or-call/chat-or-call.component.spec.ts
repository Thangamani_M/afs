import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatOrCallComponent } from './chat-or-call.component';

describe('ChatOrCallComponent', () => {
  let component: ChatOrCallComponent;
  let fixture: ComponentFixture<ChatOrCallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatOrCallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatOrCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
