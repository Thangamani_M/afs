import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ServicesComponent } from './services.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';

import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AppoinmentsComponent } from './appoinments/appoinments.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';
// import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';


import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: ServicesComponent },
      { path: 'services/:id', component: ServicesComponent }
    ]
     
  }
]

const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};
const lang = "en-US";
@NgModule({
  declarations: [ServicesComponent,AppoinmentsComponent],
  imports: [
    CommonModule,
    NgbModule,
    NgxPaginationModule,
    MatPaginatorModule,
    FormsModule, ReactiveFormsModule,
    // NgxMatTimepickerModule.setLocale(lang),
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(maskConfigFunction),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    MatIconModule,
    SharedModule
  ],
  providers:[FileTypeValidator,DatePipe]
})
export class ServicesModule { }
