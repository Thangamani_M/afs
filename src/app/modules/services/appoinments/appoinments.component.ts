import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';
import * as moment  from 'moment'
@Component({
  selector: 'app-appoinments',
  templateUrl: './appoinments.component.html',
  styleUrls: ['./appoinments.component.css']
})
export class AppoinmentsComponent implements OnInit {
  loanId: any;
  appointmentData: any;
  loading: boolean = false;
  floading: boolean = false;
  empty: boolean;
  appointmentId;
  startTime;
  toTime;

  today = new Date();
  submitted = false
  rescheduleForm!:FormGroup
  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  pageInfo = {
    page: 1,
    offset: 5
  }
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  rloading: boolean = false;
  currentdate: string;
  currenttime: Date;
  @Output() phoneNumEvent = new EventEmitter<any>();
  @Output() isLoadEvent = new EventEmitter<any>();
  defaultstartTime: any;
  defaulttoTime: any;
  branchid='';
  mindate =new Date();
  constructor(private apihttp: ApiHttpService,public datepipe: DatePipe, private modalService: NgbModal, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) {
    this.paginationSettings["enablePagination"] = false;
  }

  ngOnInit(): void {

    this.loading = true;

    let type = "uid-10084".split('-');

    this.loanId = type[1];

    this.rescheduleForm = new FormGroup({
      appointmentDate: new FormControl('', Validators.required),
      timeslot: new FormControl('', Validators.required),
    })

    this.getAppointment()

    this.getToday()
    this.rescheduleForm.controls.timeslot?.valueChanges?.subscribe(data=>{
      this.gethourminutesMinMax(data)
    })
  }
  transform(time: any): any {
    let hour = (time.split(':'))[0]
    let min = (time.split(':'))[1]
    let part = hour > 12 ? 'PM' : 'AM';
    min = (min + '').length == 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = (hour + '').length == 1 ? `${hour}` : hour;
    // return `${hour}:${min} ${part}`
    return `${hour}:${min} ${part}`
  }
  // getToday() {
  //   let dtToday = new Date()
  //   // dtToday.setDate(dtToday.getDate() + 1);
  //   let month = dtToday.getMonth() + 1 + '';
  //   let day = dtToday.getDate() + '';
  //   let year = dtToday.getFullYear();
  //   if (Number(month) < 10)
  //     month = '0' + month.toString();
  //   if (Number(day) < 10)
  //     day = '0' + day.toString();

  //   this.currentdate = year + '-' + month + '-' + day;
  //   this.currenttime = dtToday
  // }
  getToday() {
    let dtToday = new Date()
    dtToday.setDate(dtToday.getDate()+1);
    let month = dtToday.getMonth() + 1 + '';
    let day = dtToday.getDate() + '';
    let year = dtToday.getFullYear();
    if (Number(month) < 10)
      month = '0' + month.toString();
    if (Number(day) < 10)
      day = '0' + day.toString();

    this.currentdate = year + '-' + month + '-' + day;
    this.mindate =new Date(year + '-' + month + '-' + day) ;
    this.currenttime = dtToday
  }

  getAppointment() {
    // GETALLTRANSTACTIONS
    this.floading = true;
    this.paginationSettings["offset"] = 5;
    let formData = new FormData()
    formData.append('page', this.pageInfo.page + '')
    formData.append('offset', this.pageInfo.offset + '')
    this.http.post(this.api.GETAPPOINTMENT, formData).subscribe(
      (res: any) => {
        this.appointmentData = res.data.data;
        this.phoneNumEvent.emit(res.data)
        this.isLoadEvent.emit()
        console.log("res",res)
        this.startTime=res.data.StartTime
        this.toTime=res.data.ToTime
  
        this.defaultstartTime=res.data.BranchStartTime
        this.defaulttoTime=res.data.BranchToTime
          


        this.paginationSettings["length"] = res.data.totalCount;
        this.paginationSettings["enablePagination"] = true;
        this.paginationSettings["totalPages"] = res.data?.totalPages;
        this.loading = false;
        this.floading = false;
        // this.goTo(1)
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.loading = false;
        this.floading = false;
        this.isLoadEvent.emit()
      }
    );
  }

  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.getAppointment()
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
  updateschedule(type, apid) {
    this.floading = true;
    let formData = new FormData()
    formData.append('AppointmentBookingId', apid)
    formData.append('AppointmentStatus', type)
    this.http.post(this.api.UPDATEAPPOINTMENT, formData).subscribe(
      (res: any) => {

        this.getAppointment()
      },
      (error: any) => {
        console.log("Error", error);
        this.floading = false;
      });

  }
  reschedule(id, date, time, content,branchid) {
    console.log(date)
    this.appointmentId = id
    this.branchid=branchid
    this.rescheduleForm.patchValue({
      appointmentDate: new Date(date),
      timeslot: time.substring(0, 5)
    })
    console.log(this.rescheduleForm.value)
    this.modalService.open(content).result.then((res) => {
    }, (res) => {
      console.log(res)

    })

  }
  resuduleSubmit() {

    this.submitted = true
    if (this.rescheduleForm.invalid) return;
    this.rloading = true;
    let formData = new FormData()
    let sortDate = this.datepipe.transform( this.rescheduleForm.value.appointmentDate, 'yyyy-MM-dd') + '';
    formData.append('AppointmentBookingId', this.appointmentId)
    formData.append('AppointmentStatus', '7')
    formData.append('AppointmentDate', sortDate)
    formData.append('AppointmentStartTime', this.convertTime12to24(this.rescheduleForm.value.timeslot))
    this.http.post(this.api.UPDATEAPPOINTMENT, formData).subscribe(
      (res: any) => {
        this.rloading = false;
        this.modalService.dismissAll()
        this.getAppointment()
      },
      (error: any) => {
        this.rloading = false;
        console.log("Error", error);
      });
  }




  gethourminutesMinMax(e) {
    let startTime =(this.branchid=='12')?this.defaultstartTime:this.startTime;
    let endTime = (this.branchid=='12')?this.defaulttoTime:this.toTime;
    let returnTime = this.convertTime12to24(e);
    console.log("validTime--", returnTime)
    let currentDate = new Date()
    let startDate = new Date(currentDate.getTime());
    startDate.setHours(Number(startTime.split(":")[0]));
    startDate.setMinutes(Number(startTime.split(":")[1]));

    let endDate = new Date(currentDate.getTime());
    endDate.setHours(Number(endTime.split(":")[0]));
    endDate.setMinutes(Number(endTime.split(":")[1]));

    let returnDate = new Date(currentDate.getTime());
    returnDate.setHours(Number(returnTime.split(":")[0]));
    returnDate.setMinutes(Number(returnTime.split(":")[1]));
    let valid = startDate <= returnDate && endDate >= returnDate
    if (valid) {
      //this.rescheduleForm.controls.timeslot.setErrors(null)
      this.removeError(this.rescheduleForm.controls.timeslot, 'minMaxError');
    } else {
      this.rescheduleForm.controls.timeslot.setErrors({ "minMaxError": true })
    }
    let sortDate = this.datepipe.transform( this.rescheduleForm.value.appointmentDate, 'yyyy-MM-dd') + '';
    let todayDate = this.datepipe.transform(currentDate, 'yyyy-MM-dd') + '';
    if(sortDate==todayDate){
      let currentHour = currentDate.getHours()
      if (currentHour<Number(returnTime.split(":")[0])) {
        // this.rescheduleForm.controls.timeslot.setErrors(null)
        this.removeError(this.rescheduleForm.controls.timeslot, 'minTodayError');
      } else {
        this.rescheduleForm.controls.timeslot.setErrors({ "minTodayError": true })
      }
      }
    console.log("validTime", valid)
  }


  convertTime12to24 = time12h => {
    const [time, modifier] = time12h.split(" ");
   
    let [hours, minutes] = time.split(":");
   
    if (hours === "12") {
      hours = "00";
    }
   
    if (modifier === "PM") {
      hours = parseInt(hours, 10) + 12;
    }
    if(hours.length==1)
    {
      hours = "0"+hours;
    }
    return `${hours}:${minutes}`;
  };
   get12HourFormat(time){
    if(time){
    const number = moment(time, ["HH.mm"]).format("hh:mm a");
    return number
    } 
    return ''
   }

  removeError(control: AbstractControl, error: string) {
    const err = control.errors; // get control errors
    if (err) {
      delete err[error]; // delete your own error
      if (!Object.keys(err).length) { // if no errors left
        control.setErrors(null); // set control errors to null making it VALID
      } else {
        control.setErrors(err); // controls got other errors so set them back
      }
    }
  }
}
