import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeletePopupComponent } from 'src/app/shared/components/delete-popup/delete-popup.component';
import * as moment  from 'moment'


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  sectionId = '1';
  selectedId = 2
  addbooking = false
  appointmentData: any = [];
  cardInfo: boolean = false;
  openButton: boolean = true;
  closeButton: boolean = false;
  newCard: boolean = false;
  cardData: any;
  showCards: boolean = true;
  popupLoading: boolean = false;
  earlyAmount: any;
  selectedCardToken: any;
  tempcondition=false
  loading: boolean = false;
  submitted = false
  paysubmitted=false
  dropdowndatas: any = [];
  getLoanId: any;
  currentValue: any;
  userInfo: any;
  loanidload = false
  fileValidation: boolean = false;
  currentdate;
  today = new Date();
  startTime;
  toTime ;
  defaultStartTime
  defaultToTime
  branchStartTime
  branchToTime
  fieldname: any = [];
  imagePath: any;
  imageJson: any = [{ id: '', url: '' }];
  currenttime: any;
  bookingForm!:FormGroup

  billingForm = new FormGroup({
    billingLoanId: new FormControl(null, Validators.required),
    // customerName: new FormControl(''),
    // billingValue: new FormControl(''),
    billPaymentType: new FormControl(null, Validators.required)
  })

  raiseEnquiryForm = new FormGroup({
    loanId: new FormControl(null, Validators.required),
    enquiryType: new FormControl(null, Validators.required),
    // attachments: new FormControl('', [Validators.required, this.imagvalidator.validate]),
    // attachments: new FormControl(''),
    attachments: new FormControl('', [this.imagvalidator.validate]),
    comments: new FormControl('', Validators.required)
  })

  rePaymentForm = new FormGroup({
    repayLoanId: new FormControl(null, Validators.required),
    // repayAmount: new FormControl(''),
    // currentRepayMode: new FormControl(''),
    desiredRepayMode: new FormControl(null, Validators.required),
    comments:new FormControl('')
    // repayEmployer: new FormControl(null, Validators.required)
  })


  loanPreclosureForm = new FormGroup({
    preClosureloanId: new FormControl(null, Validators.required),
    // preClosureAmount: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(7)]),
  })

  public newCardForm = new FormGroup({
    holderName: new FormControl('', [Validators.required]),
    cardNumber: new FormControl('', [Validators.required]),
    expiryDate: new FormControl('', [Validators.required,isValidDate]),
    // expiryYear: new FormControl('', [Validators.required]),
    cardCvv: new FormControl('', [Validators.required])
  })
  transactionAmount: any;
  allPayData: string;
  inputData: any = [];
  cardCheckout: boolean = false;
  tokenCheckout: boolean = false;
  proceedToPay: boolean = true;
  @ViewChild('myFormPost') myFormPost: ElementRef;
  precloseAmount: any;
  customerName: any;
  billingValue: any;
  repayAmount: any;
  currentRepayMode: any;
  preClosureAmount: any;
  repayDetails: any;
  preClosureDetails: any;
  phoneNumber = '';
  makeDefault=false
  mindate =new Date();
  cardid="0";
 popupDeleteLoading = false;
  timeLoader=true;
  constructor(private EncrDecr: EncrDecrService, private modalService: NgbModal,public datepipe: DatePipe, private fb: FormBuilder, private imagvalidator: FileTypeValidator, private http: HttpClient, private api: ApiEndpointsService, private toastr: ToastrService, private route: ActivatedRoute) {
  }

  tabsData = [{
    id: '1',
    name: 'View & Manage Billing Cycle'
  }, {
    id: '2',
    name: 'Desired Repayment Method'
  }, {
    id: '3',
    name: 'Raise an Enquiry'
  }
    , {
    id: '4',
    name: 'Book an Appointment'
  }, {
    id: '5',
    name: 'Loan Pre-closure'
  }
  ]

  ngOnInit(): void {
   this.bookingForm = new FormGroup({
      appointmenttype: new FormControl(null, Validators.required),
      BranchId: new FormControl(null, Validators.required),
      appointmentDate: new FormControl('', Validators.required),
      timeslot: new FormControl('', Validators.required),
      reason: new FormControl('', Validators.required)
    })

    this.getToday()

    let getUser = localStorage.getItem('user');
    if (getUser) {
      this.userInfo = JSON.parse(getUser)
    }
    console.log(this.userInfo);

    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      console.log('Master Data:', this.dropdowndatas)
    }

  this.bookingForm.controls.timeslot?.valueChanges?.subscribe(data=>{
    // if(this.bookingForm.controls.BranchId.valid)
        this.gethourminutesMinMax(data)
    //  else
    //    {
    //     this.bookingForm.controls.timeslot.setValue('')
    //      this.toastr.error('Please select branch', '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
    //     }   
  })
    // Get Preclosure Id
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        let type = paramid["id"].split('~');
        console.log(type)
        // this.loanId = type[1];
        if (type[0] == 'preclosure') {
          console.log(type[1]);
          this.loanPreclosureForm.controls["preClosureloanId"].setValue(type[1]);
          this.sectionId = '5';

          this.loanidload = true
          const formdata = new FormData()
          formdata.append('loan_id', type[1]);
          this.http.post(this.api.PRECLOSUREAMOUNT, formdata).subscribe(
            (res: any) => {
              console.log('Preclosure Amount_:', res)
              this.loanidload = false;
              this.preClosureDetails = res.data;
            }),
            (error: any) => {
              this.loanidload = false
              console.log("Error", error);
            }
        }
        else if (type[0] == 'repayment') {
          // debugger;
          console.log(type[1]);
          this.rePaymentForm.controls["repayLoanId"].setValue(type[1]);
          console.log('Rv:', this.rePaymentForm.value.repayLoanId)
          this.sectionId = '2';

          this.loanidload = true
          const formdata = new FormData()
          this.repayAmount = ''
          this.currentRepayMode = '';
          this.billingValue = '';
          formdata.append('LoanId', type[1]);
          this.http.post(this.api.REPAYMENTCURRENT, formdata).subscribe(
            (res: any) => {
              console.log('loan data_:', res)
              this.repayDetails = res.data;
              this.loanidload = false;
            },
            (error: any) => {
              this.loanidload = false
              console.log("Error", error);
            }
          );

        } else {
          this.sectionId = '';
        }
      }
    });



    // Get Repayment Loan ID's
    this.http.get(this.api.REPAYMENTMASTERDATA).subscribe(
      (res: any) => {
        this.getLoanId = res.data.Loans;
        // console.log("All Loan Id's", this.getLoanId)
        if(this.getLoanId && this.getLoanId.length>0){
           this.getCurrentValue(this.getLoanId[0].LoanId)
           this.billingForm.controls.billingLoanId.setValue(this.getLoanId[0].LoanId)
        }
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );

    // SET Form Values
    this.customerName = this.userInfo.userName;
    if (this.preClosureAmount) {
      this.precloseAmount = this.preClosureAmount;
      console.log("PR-AM", this.precloseAmount)
    }
  }

  changePhone(phone) {

    this.phoneNumber = phone.PhoneNumber
    this.startTime = phone.StartTime
    this.toTime = phone.ToTime
    this.defaultStartTime = phone.StartTime
    this.defaultToTime = phone.ToTime
    this.branchStartTime = phone.BranchStartTime
    this.branchToTime = phone.BranchToTime
  
  }
  isLoaderCange()
  {
    this.timeLoader=false
  }
  get billingControls() {
    return this.billingForm.controls
  }
  get repayModeControls() {
    return this.rePaymentForm.controls
  }

  
  getToday() {
    let dtToday = new Date()
    dtToday.setDate(dtToday.getDate()+1);
    let month = dtToday.getMonth() + 1 + '';
    let day = dtToday.getDate() + '';
    let year = dtToday.getFullYear();
    if (Number(month) < 10)
      month = '0' + month.toString();
    if (Number(day) < 10)
      day = '0' + day.toString();

    this.currentdate = year + '-' + month + '-' + day;
    this.mindate =new Date(year + '-' + month + '-' + day) ;
    this.currenttime = dtToday
  }

  // getToday() {
  //   let dtToday = new Date()
  //   dtToday.setDate(dtToday.getDate() + 1);
  //   let month = dtToday.getMonth() + 1 + '';
  //   let day = dtToday.getDate() + '';
  //   let year = dtToday.getFullYear();
  //   if (Number(month) < 10)
  //     month = '0' + month.toString();
  //   if (Number(day) < 10)
  //     day = '0' + day.toString();

  //   this.currentdate = year + '-' + month + '-' + day;
  //   this.currenttime = dtToday
  // }

  goTo(id) {
    // debugger;
    this.sectionId = id
    this.billingForm.reset()
    this.rePaymentForm.reset()

    this.repayDetails = '';

    if(this.sectionId=='1') {
      if(this.getLoanId && this.getLoanId.length>0){
      this.getCurrentValue(this.getLoanId[0].LoanId)
      this.billingForm.controls.billingLoanId.setValue(this.getLoanId[0].LoanId)
      }
    } else if(this.sectionId=='2') {
      if(this.getLoanId && this.getLoanId.length>0){
      this.getCurrentValue(this.getLoanId[0].LoanId)
      this.rePaymentForm.controls.repayLoanId.setValue(this.getLoanId[0].LoanId)
      }
    } else if(this.sectionId=='3') {
      if(this.getLoanId && this.getLoanId.length>0){
      this.getCurrentValue(this.getLoanId[0].LoanId)
      this.raiseEnquiryForm.controls.loanId.setValue(this.getLoanId[0].LoanId)
      }
    } else if(this.sectionId=='4'){
      this.bookingForm.controls.appointmenttype.setValue(null)
      this.bookingForm.controls.BranchId.setValue(null)
      this.bookingForm.controls.appointmentDate.setValue('')
      this.bookingForm.controls.timeslot.setValue('')
      this.bookingForm.controls.reason.setValue('')
       this.bookingForm.controls.appointmenttype.markAsPristine()
      this.bookingForm.controls.BranchId.markAsPristine()
      this.bookingForm.controls.appointmentDate.markAsPristine()
      this.bookingForm.controls.timeslot.markAsPristine()
      this.bookingForm.controls.reason.markAsPristine()
    }
     else if(this.sectionId=='5') {
      if(this.getLoanId && this.getLoanId.length>0){
      this.getPreCloseValue(this.getLoanId[0].LoanId)
      this.loanPreclosureForm.controls.preClosureloanId.setValue(this.getLoanId[0].LoanId)
      }
    } 

    this.submitted = false
  }
  geturl(id) {
    let url = this.imageJson.filter(item => item.id == id)
    if (url.length > 0)
      return url[0].url
    return false
  }
  booking() {

    this.addbooking = true
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }
  changeBranch(){
    if(this.bookingForm.value.BranchId=='12'){
      this.startTime =this.branchStartTime
      this.toTime =this.branchToTime
    } else{
      this.startTime =this.defaultStartTime
      this.toTime =this.defaultToTime
    }
  }
  submit() {
    this.submitted = true
    console.log(this.bookingForm.value)
    if (this.bookingForm.invalid) {
      return
    }
    this.loading = true
    const formdata = new FormData()
    let sortDate = this.datepipe.transform( this.bookingForm.value.appointmentDate, 'yyyy-MM-dd') + '';
    formdata.append('BranchId', this.bookingForm.value.BranchId);
    formdata.append('AppointmentBookingTypeId', this.bookingForm.value.appointmenttype);
    formdata.append('AppointmentDate', sortDate);
    formdata.append('AppointmentStartTime', this.convertTime12to24(this.bookingForm.value.timeslot));
    formdata.append('Comments', this.bookingForm.value.reason);
    this.http.post(this.api.INSERTAPPOINTMENT, formdata).subscribe(
      (res: any) => {
        //this.bookingForm.reset()
       
        this.bookingForm.controls.appointmenttype.setValue(null)
        this.bookingForm.controls.BranchId.setValue(null)
        this.bookingForm.controls.appointmentDate.setValue('')
        this.bookingForm.controls.timeslot.setValue('')
        this.bookingForm.controls.reason.setValue('')
         this.bookingForm.controls.appointmenttype.markAsPristine()
        this.bookingForm.controls.BranchId.markAsPristine()
        this.bookingForm.controls.appointmentDate.markAsPristine()
        this.bookingForm.controls.timeslot.markAsPristine()
        this.bookingForm.controls.reason.markAsPristine()
        this.submitted = false
        this.appointmentData = res.data.data;
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
        this.loading = false;
        this.addbooking = false
        // this.goTo(1)
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.loading = false;
      }
    );
  }
  cancelbooking() {
    this.addbooking = false
  }
  getCurrentValue(id: any) {
    // alert(id)

    this.loanidload = true
    const formdata = new FormData()
    this.repayAmount = ''
    this.currentRepayMode = '';
    this.billingValue = '';
    formdata.append('LoanId', id);
    this.http.post(this.api.REPAYMENTCURRENT, formdata).subscribe(
      (res: any) => {
        console.log('loan data_:', res)
        this.repayDetails = res.data;
        this.loanidload = false
           if(this.sectionId =='2')
           this.rePaymentForm.controls.desiredRepayMode.setValue(this.repayDetails.LoanTypeId=="1"?4:5)
        if (res.data.length > 0) {
          res.data.forEach(element => {
            // this.loanidload = false 
            if (element.ReferenceType == 'DesiredRepayment') {
              this.repayAmount = element.LoanAmount ? Number(element.LoanAmount.split('.')[0]) : '';
              this.currentRepayMode = element.ReferenceValue ? element.ReferenceValue : '';
              this.preClosureAmount = element.LoanAmount ? Number(element.LoanAmount.split('.')[0]) : '';
              this.precloseAmount = element.LoanAmount ? Number(element.LoanAmount.split('.')[0]) : '0';
              console.log("PR-AM", this.precloseAmount,element.LoanTypeId)
              this.rePaymentForm.controls.desiredRepayMode.setValue(element.LoanTypeId=="1"?4:5)
            } if (element.ReferenceType == 'BillingCycle') {
              this.customerName = element.FirstName ? element.FirstName : '';
              this.billingValue = element.ReferenceValue ? element.ReferenceValue : '';
            }
          });
        } else {
          // debugger;
          this.repayAmount = '';
          this.currentRepayMode = '';
          this.preClosureAmount = '';
          this.billingValue = '';

        }


        // console.log(this.currentValue)
      },
      (error: any) => {
        this.loanidload = false
        console.log("Error", error);
      }
    );
  }
  getPreCloseValue(id: any) {
    this.loanidload = true
    const formdata = new FormData()
    formdata.append('loan_id', id);

    this.http.post(this.api.PRECLOSUREAMOUNT, formdata).subscribe(
      (res: any) => {
        console.log('Preclosure Amount_:', res)
        this.loanidload = false;
        this.preClosureDetails = res.data;
      }),
      (error: any) => {
        this.loanidload = false
        console.log("Error", error);
      }

  }
  numberWithCommasdecimal(x) {
    let val = x.toString().split(".");
     if (val.length > 1){
      val = x.toFixed(2)
     } else {
      val = val+".00"
     }
        
    let parts = val.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }
  reqPreClose() {
    this.submitted = true
    if (this.loanPreclosureForm.invalid)
      return
    if (this.loanPreclosureForm.value.preClosureloanId && this.preClosureDetails?.amount) {
      this.loanidload = true
      const formdata = new FormData()
      formdata.append('LoanId', this.loanPreclosureForm.value.preClosureloanId);
      formdata.append('Amount', this.preClosureDetails?.amount);

      this.http.post(this.api.PRECLOSUREINSERT, formdata).subscribe(
        (res: any) => {
          console.log('Preclosure Amount_:', res)
          this.loanidload = false;
          this.loanPreclosureForm.reset()
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
          this.preClosureDetails = ''
          this.submitted = false
        }, (error: any) => {
          this.loanidload = false
          console.log("Error", error);
        })

    }
    // else {
    //    this.toastr.error('Please select loan id/loan amount', '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })  
    // }
  }
  fileChange(event: any, FormControl) {
    this.fileValidation = false;

    console.log("FormControl", FormControl)
    // debugger;

    if (event.target.files.length > 0) {
      const formData = new FormData();

      const file = event.target.files[0];
      console.log("Value", this.raiseEnquiryForm.controls)
      this.raiseEnquiryForm.controls[FormControl].setValue(file['name'])
      this.raiseEnquiryForm.controls[FormControl].markAsDirty()
      if (!this.imagvalidator.validate(this.raiseEnquiryForm.controls[FormControl] as FormControl)) {
        const max_size = 25097152;
        console.log(event.target.files[0].size, (event.target.files[0].size / (1024 * 1024)).toFixed(2))
        if (event.target.files[0].size / (1024 * 1024) > max_size / (1024 * 1024)) {
          console.log(max_size / (1024 * 1024) + 'Mb')
          this.raiseEnquiryForm.controls[FormControl].setErrors({ 'filleSizeincorrect': true });
          return
        } else {
          this.raiseEnquiryForm.controls[FormControl].setErrors(null);
        }

        formData.append("image", file, file['name']);

        this.fieldname.push(FormControl)

        this.http.post(this.api.UPLOADFILE, formData).subscribe(
          (res) => {
            this.fileValidation = false;
            this.raiseEnquiryForm.controls[FormControl].setValue(res['data']['file_name']);
            this.imagePath = res['data']['path'];
            this.imageJson.push({ id: res['data']['file_name'], url: res['data']['path'] })
            const index = this.fieldname.indexOf(FormControl, 0);

            if (index > -1) {
              this.fieldname.splice(index, 1);
            }

          },
          (err) => {
            console.log(err);
            this.fileValidation = true;
            const index = this.fieldname.indexOf(FormControl, 0);
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }
            this.raiseEnquiryForm.controls[FormControl].setValue('');
            this.imagePath = '';
          }
        );
      }
    }
  }

  submittab(type: any) {
    // debugger;
    if (type == '1') {
      this.billingFormSubmit()
    } else if (type == '2') {
      this.repayFormSubmit();
    } else if (type == '3') {
      this.raiseanEnquirySubmit()
    }
  }

  cancelForm(type: any) {
    // debugger;
    this.submitted = false
    this.repayDetails = '';
    if (type == '1') {
      this.billingForm.reset();
      this.customerName = this.userInfo.userName;
    } else if (type == '2') {
      this.rePaymentForm.reset();

    } else if (type == '3') {
      this.raiseEnquiryForm.reset()
    } else if (type == '5') {
      this.loanPreclosureForm.reset()
    }
  }


  billingFormSubmit() {

    this.submitted = true

    if (this.billingForm.invalid) {
      return
    }
    this.loading = true
    const formdata = new FormData()
    formdata.append('Type', this.billingForm.value.billPaymentType);
    formdata.append('LoanId', this.billingForm.value.billingLoanId);
    this.http.post(this.api.BILLINGINSERT, formdata).subscribe(
      (res: any) => {
        console.log(res)
        this.billingForm.reset();
        this.repayDetails = '';
        this.submitted = false
        this.loading = false
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
      },
      (error: any) => {
        this.loading = false
        console.log("Error", error);
      }
    );
  }


  repayFormSubmit() {
    // debugger;
    this.submitted = true;
    if (this.rePaymentForm.invalid) {
      return
    }
    this.loading = true
    const formdata = new FormData()
    formdata.append('Type', this.rePaymentForm.value.desiredRepayMode);
    formdata.append('LoanId', this.rePaymentForm.value.repayLoanId);
     formdata.append('comments', this.rePaymentForm.value.comments);
    this.http.post(this.api.REPAYMENTINSERT, formdata).subscribe(
      (res: any) => {
        console.log(res)
        this.rePaymentForm.reset();
        this.repayDetails = '';
        this.submitted = false
        this.loading = false
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
      },
      (error: any) => {
        this.loading = false
        console.log("Error", error);
      }
    );
  }


  raiseanEnquirySubmit() {
    // this.fileValidation = false;
    this.submitted = true
    console.log(this.fieldname)
    if (this.raiseEnquiryForm.invalid || this.fieldname.length > 0) {
      return
    }
    console.log(this.raiseEnquiryForm.value)
    this.loading = true

    const formdata = new FormData()
    formdata.append('EnquiryTypeId', this.raiseEnquiryForm.value.enquiryType);
    formdata.append('LoanId', this.raiseEnquiryForm.value.loanId);
    formdata.append('Attachment', this.raiseEnquiryForm.value.attachments);
    formdata.append('Comments', this.raiseEnquiryForm.value.comments);
    this.http.post(this.api.ENQUIRYINSERT, formdata).subscribe(
      (res: any) => {
        console.log(res)
        this.loading = false
        this.raiseEnquiryForm.reset();
        this.submitted = false
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
      },
      (error: any) => {
        this.loading = false
        console.log("Error", error);
      }
    );

  }
  keyPressAlpha(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z\s]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }

  gotoPayment(id) {
    localStorage.setItem('loan-precloser',id+'')
    this.proceedToPay = false;
    this.cardInfo = true;
    this.getCardDetails();
  }

  getCardDetails() {
    const loanData = new FormData();
    loanData.append("loan_id", this.loanPreclosureForm.value.preClosureloanId);
    // debugger;    
    this.http.post(this.api.GETCARDDETAILSBYUSERID, loanData).subscribe(
      (res: any) => {
        this.earlyAmount = res.data;
        this.popupLoading = false;
        this.popupDeleteLoading = false;
        this.cardData = this.earlyAmount.data;
        if(this.cardData && this.cardData.length>0)
        {
          this.cardData.forEach(item=>{
            if(item.IsDefault==1){
             this.selectedCardToken = item.card_token;
             this.cardid =item.cardid
            }
          })
        }
       console.log('Card Details:', res,this.selectedCardToken )
      },
      (error: any) => {
        this.popupLoading = false;
        this.popupDeleteLoading = false;
        console.log("Error", error);
        // this.toastr.error(error)

      }
    );
  }

  onSelectedCard(token: any,cardid) {
    this.selectedCardToken = token;
    this.cardid =cardid
  }
  removeCard(id){
    


    this.modalService.open(DeletePopupComponent, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      console.log(res)
      if (res && res == 1) {
        this.popupDeleteLoading = true;
        this.http.post(this.api.DELETECARD, {id:id}).subscribe(
          (res: any) => {
            this.getCardDetails();
          },error=>{
           this.popupDeleteLoading = false;
          })
      }
    }, (res) => {
      console.log(res)
    });
    
  }
  makePayment() {

    // console.log(this.newCardForm.controls.value)
    // debugger;
    this.paysubmitted=true
    if(this.newCard && this.newCardForm.invalid) return
    if (this.newCardForm.valid || this.selectedCardToken || this.selectedCardToken !== undefined) {
      if(!this.tempcondition){
        this.popupDeleteLoading = false;
        this.toastr.error("Please check Terms of Use",'',{ positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
      return
      }
      if (typeof (this.preClosureDetails?.amount) === 'string') {
        this.transactionAmount = this.preClosureDetails?.amount.replace(',', '')
      } else {
        this.transactionAmount = this.preClosureDetails?.amount;
      }
      let randomVal = Math.floor(10000000 + Math.random() * 90000000);
      
      let deviceToken = localStorage.getItem('accessTOKEN');
      // console.log(deviceToken);
        let userInfo;
      let getUser = localStorage.getItem('userDetails');
      if (getUser) {
        userInfo = JSON.parse(getUser)
      }
      this.popupDeleteLoading = true;
      let payData = {
        "reference_number": randomVal,
        "device_type": "3",
        "device_token": deviceToken,
        "payment_method": '2',
        "currency_code": "JMD",
        "loan_id": this.loanPreclosureForm.value.preClosureloanId,
        "amount": this.transactionAmount
      }
      if(this.selectedCardToken && !this.newCard)
         payData =Object.assign(payData,{"CardToken":this.selectedCardToken})
      console.log('PAYMENT DATA_:', payData);
      this.allPayData = this.EncrDecr.set(JSON.stringify(payData));
      let encryptData = this.EncrDecr.getEncoding(this.allPayData);
      let token;
      token = localStorage.getItem("accessTOKEN");
      console.log('Token', token)
      let httpHeaders = new HttpHeaders({
        "Content-Type": "text/plain",
        "Authorization": token,
        "Accept": "*/*"
      });

      // console.log('Encrypted:', encryptData);
      this.http.post(this.api.MAKEPAYMENT, encryptData, { headers: httpHeaders }).subscribe(
        (res: any) => {
          this.popupDeleteLoading = false;
          this.inputData.push({ name: "ClientId", value: res.data.ClientId })
          this.inputData.push({ name: "TransactionId", value: res.data.TransactionId })
          this.inputData.push({ name: "CurrencyCode", value: "JMD" })
          this.inputData.push({ name: "Amount", value: this.transactionAmount })
          this.inputData.push({ name: "Signature", value: res.data.Signature })
          this.inputData.push({ name: "ReturnToMerchant", value: "Y" })
          this.inputData.push({ name: "AutoRedirect", value: "Y" })
          this.inputData.push({ name: "CustomerReference", value: userInfo?.FirstName })
          this.inputData.push({ name: "BillToEmail", value: userInfo?.Email })
          this.inputData.push({ name: "BillToTelephone", value: "" })
          this.inputData.push({ name: "CustomerInvoice", value: "" })
          this.inputData.push({ name: "BillToAddress", value: "" })
          this.inputData.push({ name: "BillToCountry", value: "" })
          this.inputData.push({ name: "BillToState", value: "" })
          this.inputData.push({ name: "BillToFirstName", value: "" })
          this.inputData.push({ name: "BillToCity", value: "" })
          this.inputData.push({ name: "BillToZipPostCode", value: "" })
          this.inputData.push({ name: "CustomMessage", value: (this.makeDefault?1:0)+"~~0"+"~~"+this.cardid })
          // // USE CARD DETAILS:
          if (this.newCard) {
            //   window.location.href = "https://paymentdemo.myamberpay.com/gateway/v1/quick-checkout";
            this.inputData.push({ name: "CustomerReference", value: this.newCardForm.value.holderName })
            this.inputData.push({ name: "CardHolderName", value: this.newCardForm.value.holderName })
            this.inputData.push({ name: "CardNumber", value: this.newCardForm.value.cardNumber })
            this.inputData.push({ name: "CVV", value: this.newCardForm.value.cardCvv })
            this.inputData.push({ name: "Month", value: this.newCardForm.value.expiryDate.substring(0,2) })
            this.inputData.push({ name: "Year", value: this.newCardForm.value.expiryDate.substring(2,6) })
            this.inputData.push({ name: "CardTokenize", value: "Y" })
            console.log("Card_data:", this.inputData);
            this.cardCheckout = true;
          }

          // //SELECT CARD
          if ((this.showCards || this.selectedCardToken) && (!this.newCard)) {
            //   window.location.href = "https://paymentdemo.myamberpay.com/gateway/v1/tokenize-checkout";
            this.inputData.push({ name: "3DSFlag", value: "Y" })
            this.inputData.push({ name: "PaymentMethod", value: "" })
            this.inputData.push({ name: "CardToken", value: this.selectedCardToken })
            this.tokenCheckout = true;
            console.log("Token_data:", this.inputData);
          }
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          this.popupDeleteLoading = true;
        }
      )
    }
    else {
      this.popupDeleteLoading = false;
         this.toastr.error("Please select/add card",'',{ positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
    }

  }
  getAmountMask(len, strlen) {
    let count = "0";
    for (let i = 0; i < len - 1; i++) {
      count += "0";
    }
    if (strlen <= 3) {
      return count
    }
    return this.numberWithCommas(count);
  }
  numberWithCommas(x) {
    x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return x;
  }
  addNewCard() {
    this.showCards = false;
    this.newCard = true;
    this.openButton = false;
    this.closeButton = true;
  }
  closeCardForm() {
    this.newCard = false;
    this.showCards = true;
    this.openButton = true;
    this.closeButton = false;
  }

  ngAfterViewChecked() {
    if (this.cardCheckout && this.myFormPost) {
      this.myFormPost.nativeElement.submit();
      this.cardCheckout = false;


    } else if (this.tokenCheckout && this.myFormPost) {
      this.myFormPost.nativeElement.submit();
      this.tokenCheckout = false;
    }
  }

  gethourminutesMinMax(e) {
    if(!e) return
    let startTime =  this.startTime;
    let endTime = this.toTime;
   // let returnTime = e.target.value;
    let returnTime = this.convertTime12to24(e);
    
    console.log("validTime--", returnTime)
    let currentDate = new Date()
    let startDate = new Date(currentDate.getTime());
    startDate.setHours(Number(startTime.split(":")[0]));
    startDate.setMinutes(Number(startTime.split(":")[1]));

    let endDate = new Date(currentDate.getTime());
    endDate.setHours(Number(endTime.split(":")[0]));
    endDate.setMinutes(Number(endTime.split(":")[1]));

    let returnDate = new Date(currentDate.getTime());
    returnDate.setHours(Number(returnTime.split(":")[0]));
    returnDate.setMinutes(Number(returnTime.split(":")[1]));
    let valid = startDate <= returnDate && endDate >= returnDate
    if (valid) {
      //this.bookingForm.controls.timeslot.setErrors(null)
      this.removeError(this.bookingForm.controls.timeslot, 'minMaxError');
    } else {
      this.bookingForm.controls.timeslot.setErrors({ "minMaxError": true })
    }

    let sortDate = this.datepipe.transform( this.bookingForm.value.appointmentDate, 'yyyy-MM-dd') + '';
    let todayDate = this.datepipe.transform(currentDate, 'yyyy-MM-dd') + '';
    
    if(sortDate==todayDate){
    let currentHour = currentDate.getHours()
    if (currentHour<Number(returnTime.split(":")[0])) {
      this.removeError(this.bookingForm.controls.timeslot, 'minTodayError');
    } else {
      this.bookingForm.controls.timeslot.setErrors({ "minTodayError": true })
    }
    }


    console.log("validTime", valid)
  }

 convertTime12to24 = time12h => {
    const [time, modifier] = time12h.split(" ");
   
    let [hours, minutes] = time.split(":");
   
    if (hours === "12") {
      hours = "00";
    }
   
    if (modifier === "PM") {
      hours = parseInt(hours, 10) + 12;
    }
     if(hours.length==1)
      {
        hours = "0"+hours;
      }
    return `${hours}:${minutes}`;
  };
   get12HourFormat(time){
    if(time){
    const number = moment(time, ["HH.mm"]).format("hh:mm a");
    return number
    } 
    return ''
   }


  removeError(control: AbstractControl, error: string) {
    const err = control.errors; // get control errors
    if (err) {
      delete err[error]; // delete your own error
      if (!Object.keys(err).length) { // if no errors left
        control.setErrors(null); // set control errors to null making it VALID
      } else {
        control.setErrors(err); // controls got other errors so set them back
      }
    }
  }
}


export const isValidDate = (c: FormControl) => {
  if(!c.value) return null
  if ((c.value).match(/^(0[1-9]|1[0-2])\/?([0-9]{4})$/)) {
      // get midnight of first day of the next month
      const expiry = new Date(c.value.substring(2,6),c.value.substring(0,2));
      const current = new Date();

    return expiry.getTime() > current.getTime() ? null: {
      validateDate:true
    } ;
  } else {
    return {
      validateDate:true
    }
  }
}