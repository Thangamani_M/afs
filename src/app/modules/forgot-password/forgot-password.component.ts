import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { NgxOtpInputComponent, NgxOtpInputConfig } from "ngx-otp-input";
import { VerifForgotComponent } from './verif-forgot/verif-forgot.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})

export class ForgotPasswordComponent implements OnInit {
  @ViewChild('ngxotp') ngxotp: NgxOtpInputComponent;

  forgotPassword: boolean = true;
  setPassword: boolean = false;
  showPword: boolean = false;
  showCPword: boolean = false;
  otpVerify: boolean = true;
  fpOtpSuccess: boolean = false;
  closeModal: string;
  encPassword: any;
  popupLoading: boolean = false;

  sitekey=environment.recaptcha.siteKey

  loading: boolean = false;
  isSumitted = false
  // isClickedOnce = 0;
  isClickedOnce = true;

  scrollBlocks = [
    { title: 'A Faster Way', short: 'A faster way to apply for your personal and business loans.', src: "../../../../assets/img/scroll-1.png" },
    { title: 'Personal Loans', short: 'Personal loans are available for both private and public sector employees.', src: "../../../../assets/img/scroll-2.png" },
    // { title: 'Third Slide', short: 'Third Slide Short', src: "../../../../assets/img/Login-bg.png" }
  ];


  public fPwordForm = new FormGroup({
    userEmail: new FormControl('', [Validators.required, Validators.pattern('^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{7})+$')]),//, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
    token:new FormControl('',[Validators.required])
  })

  public resetPForm: FormGroup
  otpValue: any;


  // constructor(private modalService: BsModalService, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  //   ) { }
  constructor(private EncrDecr: EncrDecrService, private modalService: NgbModal, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.resetPForm = this.fb.group({
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{5,}'), Validators.minLength(6), Validators.maxLength(12)]],
      confirmPassword: ['', [Validators.required]],
    }, { validators: this.checkPasswords })
  }


  private passwordMatcher(control: FormControl) {
    if (
      this.resetPForm &&
      (control.value !== this.resetPForm.controls.password.value)
    ) {
      return { passwordNotMatch: true };
    }
    return null;
  }

  showPassword() {
    this.showPword = true;
  }

  hidePassword() {
    this.showPword = false;
  }
  showCPassword() {
    this.showCPword = true;
  }

  hideCPassword() {
    this.showCPword = false;
  }


  gotoLogin() {
    this.router.navigateByUrl('/login');
  }

  get fpcontrols() {
    return this.fPwordForm.controls
  }

  get resetPcontrols() {
    return this.resetPForm.controls
  }

  submitFP(captcha) {
    this.isSumitted = true
    if (this.fPwordForm.invalid) {
      //captcha?.reset()
      //this.fPwordForm.controls.token.setValue('')
      return
    }
    this.isClickedOnce = false;
    this.fpOtpSuccess = false;
    this.otpVerify = false;
    this.loading = true;
    // debugger;
    const formData = new FormData();


    let username=this.fPwordForm.value.userEmail
    let logintype = 'email'
    if(/([0-9]{7})+$/.test(username)){
        username ='+1'+this.fPwordForm.value.userEmail
        logintype = 'phone'
    }
    formData.append("email", username);
    formData.append("grecaptcharesponse", this.fPwordForm.value.token);
    this.http.post(this.api.FORGOTPASSWORD, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.isClickedOnce = true;
      
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        this.loading = false;
        this.otpVerify = true;
        // this.intervalId = 0;
        // this.seconds = 60;
        // this.modalService.open(VerifForgotComponent, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
        //   if(res='1')
        //     this.forgotPassword = false;
        // }, (res) => {
        //   this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        // });
        captcha?.reset()
        const modalRef = this.modalService.open(VerifForgotComponent);
        modalRef.componentInstance.otpemail = username;
        modalRef.componentInstance.logintype = logintype;
        modalRef.result.then((res) => {
          this.isSumitted = false;
          this.fPwordForm.controls.token.setValue('')
    
          if (res.value = '1') {
            this.otpValue = res.otpvalue
            this.forgotPassword = false;
            this.setPassword = true;
          }
        }, (res) => {
          this.isSumitted = false;
          this.fPwordForm.controls.token.setValue('')
          captcha?.reset()
          this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        });

        // this.modalService.dismissAll()
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.fPwordForm.controls.token.setValue('')
        captcha?.reset()
        this.loading = false;
        this.isClickedOnce = true;
        this.isSumitted = false;
      }
    );

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // handeOtpChange(value: any): void {
  //   console.log(value);
  //   this.otpValue = value;
  // }

  showResponse(e){
    this.fPwordForm.controls.token.setValue(e?.response)
   console.log(e?.response)
  }
  resetPwd(modalResponse) {
    this.isSumitted = true
    if (this.resetPForm.invalid) {
      return
    }
    this.isClickedOnce = false;
    this.loading = true;
    this.encPassword = this.EncrDecr.set(this.resetPForm.value.password);
    let password = this.EncrDecr.getEncoding(this.encPassword)

    console.log("encPwd_:", this.encPassword);
    let username=this.fPwordForm.value.userEmail
    let logintype = 'email'
    if(/([0-9]{7})+$/.test(username)){
        username ='+1'+this.fPwordForm.value.userEmail
        logintype = 'phone'
    }
    let encEmail = this.EncrDecr.set(username);
    let email = this.EncrDecr.getEncoding(encEmail)
    const formData = new FormData();
    formData.append("email", email);
    // formData.append("email", "ravin@myambergroup.com");
    formData.append("password", password);
    if(this.otpValue)
     formData.append("otp", this.otpValue);
    this.http.post(this.api.CHANGEPASSWORD, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.loading = false;
        this.isSumitted = false;
        this.isClickedOnce = true;

        // this.toastr.success(res.message);

        // this.router.navigateByUrl('/login');
        this.modalService.open(modalResponse, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
          this.closeModal = `Closed with: ${res}`;
        }, (res) => {
          this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
          this.isClickedOnce = true;

        });


      },
      (error: any) => {
        this.isClickedOnce = true;
        console.log("Error", error);
        this.isSumitted = false;
        this.loading = false;
        this.fpOtpSuccess = false;
        this.resetPForm.reset();
      }
    );
  }

  login() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/login');
  }

  onPaste(e) {
    e.preventDefault();
    return false;
  }

  keyPressEmail(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9@._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  checkPasswords: ValidatorFn = (group): ValidationErrors | null => {
    let pass = '', confirmPass = '';
    if (this.resetPForm) {
      pass = this.resetPForm.controls['password'].value;
      confirmPass = this.resetPForm.controls['confirmPassword'].value
    }
    if (confirmPass.length > 0)
      return pass === confirmPass ? null : { notSame: true }
    else return null
  }
}