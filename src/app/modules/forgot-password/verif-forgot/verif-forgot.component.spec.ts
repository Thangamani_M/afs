import { ComponentFixture, TestBed } from '@angular/core/testing';


import { VerifForgotComponent } from './verif-forgot.component';

describe('VerifForgotComponent', () => {
  let component: VerifForgotComponent;
  let fixture: ComponentFixture<VerifForgotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerifForgotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifForgotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
