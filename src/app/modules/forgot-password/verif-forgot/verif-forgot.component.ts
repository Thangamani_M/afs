import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './../../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { NgxOtpInputComponent, NgxOtpInputConfig } from "ngx-otp-input";


@Component({
  selector: 'app-verif-forgot',
  templateUrl: './verif-forgot.component.html',
  styleUrls: ['./verif-forgot.component.css']
})

export class VerifForgotComponent implements OnInit {
  @ViewChild('ngxotp') ngxotp: NgxOtpInputComponent;

  forgotPassword: boolean = true;
  setPassword: boolean = false;
  otpVerify: boolean = true;
  fpOtpSuccess: boolean = false;
  closeModal: string;
  encPassword: any;
  popupLoading: boolean = false;
  resend: boolean = false;
  intervalId = 0;
  message = '';
  seconds = 90;
  loading: boolean = false;
  isSumitted = false
  // isClickedOnce = 0;
  isClickedOnce = true;
  otpNotValied = false;
  enterOTP: boolean = false;

  otpInputConfig: NgxOtpInputConfig = {
    otpLength: 6,
    autofocus: true,
    classList: {
      inputBox: "my-super-box-class",
      input: "my-super-class",
      inputFilled: "my-super-filled-class",
      inputDisabled: "my-super-disable-class",
      inputSuccess: "my-super-success-class",
      inputError: "my-super-error-class"
    }
  };

  otpValue: any;
  @Input() otpemail="";
  @Input() logintype=""
  // constructor(private modalService: BsModalService, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  //   ) { }
  constructor(private EncrDecr: EncrDecrService, private modalService: NgbModal,public activeModal: NgbActiveModal, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.otpNotValied = false;
    // this.ngxotp.clear();
    this.message = ''
    this.startTimer();
  }


  gotoLogin() {
    this.router.navigateByUrl('/login');
  }


  handleFillEvent(value: any): void {
    console.log(value);
    this.otpValue = value;
    this.otpNotValied = false;
  }
  submitFPOtp() {
 
    this.otpNotValied = false;
    this.fpOtpSuccess = false;
    this.popupLoading =true
    if (this.otpValue == null || this.otpValue == '') {
      this.otpNotValied = false;
      this.enterOTP = true;
      this.popupLoading =false
    }
    else if (this.otpValue && this.otpValue.length == 6) {
      this.otpNotValied = false;
      const formData = new FormData();
      formData.append("email", this.otpemail);
      formData.append("otp", this.otpValue);

      this.http.post(this.api.FORGOTVERIFYOTP, formData).subscribe(
        (res: any) => {
          console.log(res)
          // this.fPOtpForm.reset();
          this.popupLoading =false
          this.isSumitted = false;
          if (this.EncrDecr.getCryptLibDecoding(res.data.verified_enc)=="allow") {
            this.enterOTP = false;
            this.otpVerify = false;
            this.fpOtpSuccess = true;
            this.forgotPassword = false;
         
            setTimeout(() => {
              this.activeModal.close({value:'1',otpvalue:this.otpValue});
              // this.fPOtpForm.reset();
            }, 2000);
            this.setPassword = true;
          } else {
            this.ngxotp.clear()
            this.fpOtpSuccess = false;
            // this.fPOtpForm.reset();
            this.enterOTP = false;
            this.otpNotValied = true;
            setTimeout(() => {
              this.otpVerify = true;
              // this.fPOtpForm.reset();
            }, 2000);
            // this.fpOtpFailure = false;
          }

        },
        (error: any) => {
          this.ngxotp.clear()
          console.log("Error", error);
          this.isSumitted = false;
          // this.fPOtpForm.reset();
          this.popupLoading =false
          // this.toastr.error(error)
        }
      );
    }
  }
  
  login() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/login');
  }

  startTimer() {
    // debugger;

    this.clearTimer();
    this.seconds = 90
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.stop();
        this.resend = true;
      } else {
        if (this.seconds < 0) {
          this.resend = false;
          this.seconds = 90;
        } // reset
        this.message = `${this.seconds}`;
      }
    }, 1000);
  }


  clearTimer() {

    clearInterval(this.intervalId);
  }

  stop() {
    this.clearTimer();
    this.message = `${this.seconds}`;
  }
  resendOTP() {
    // this.ngxotp.clear();
    this.isClickedOnce = false;
    this.popupLoading = true
    const formData = new FormData();
    formData.append("email", this.otpemail);

    this.http.post(this.api.FORGOTPASSWORD, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.isClickedOnce = true;
        this.popupLoading = false
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        this.resend = false;
        this.message = ''
        this.startTimer();
        // this.modalService.dismissAll()
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        // this.loading = false;
        this.popupLoading = false
        this.isClickedOnce = true;
      }
    );
  }
  closepopup(){
    this.modalService.dismissAll()
  }
}