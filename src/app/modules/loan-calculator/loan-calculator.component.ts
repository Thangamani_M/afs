import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import * as Highcharts from 'highcharts';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

import Histogram from 'highcharts/modules/histogram-bellcurve';
Histogram(Highcharts);

import highcharts3D from 'highcharts/highcharts-3d';
highcharts3D(Highcharts);

// const Exporting = require('highcharts/modules/exporting');
// Exporting(Highcharts);

// const ExportData = require('highcharts/modules/export-data');
// ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);
Highcharts.setOptions({
  lang: {
      thousandsSep: ','
  }
});
@Component({
  selector: 'app-loan-calculator',
  templateUrl: './loan-calculator.component.html',
  styleUrls: ['./loan-calculator.component.css']
})
export class LoanCalculatorComponent implements OnInit {
  loanmonth = 1
  loanamount = 10000
  inputamount = 10000
  inputNetamount=0;
  netsales = 0.0;
  inputmonth = 1
  maxmummonth = 84
  minimummonth = 1
  numyrs
  public activity;
  public xData;
  public label;
  options: any;
  products: any = [];
  loantype = '1'
  interestrate = 0
  principalamount = 0.00
  interestamount = 0
  monthlyinstalment = 0
  totalamount = 0
  isloading = false;
  // chartPreview = true;
  tenurYears: any;
  tenurMonths: any;
  productValues: any;
  personalLoanValues: any;
  businessLoanValues: any;
  defaultValues: any;
  invalidmaxamount = false
  invalidminamount = false
  invalidmaxmonth = false;
  invalidminmonth = false;
  invalidminnetamount = false
  invalidmaxnetamount = false
  lnType: any = 1;
  minInterest: any = 0;
  maxInterest: any = 0;
  minTotAmnt: any = 0;
  maxTotAmnt: any = 0;
  minMonthInstal: any = 0;
  maxMonthInstal: any = 0;
  amountlength=5;

  constructor(private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) {
    this.getOption()
  }
  changeAmount(e) {
    if (!this.getAmountValidate(e.target.value))
      this.loanamount = e.target.value
       
  }
  changeNetAmount(e) {
    if (!this.getNetAmountValidate(e.target.value))
      this.netsales = e.target.value
       
  }
  changeMonth(e) {
    if (!this.getMonthValidate(e.target.value))
      this.loanmonth = e.target.value
  }

  changeAmountLength(e){
 console.log(this.inputamount)
 if (!this.getAmountValidate(this.inputamount))
    this.loanamount=this.inputamount
    this.amountlength= e.target.value.length
  }

  changeNetAmountLength(e){
    console.log(this.inputNetamount)
    if (!this.getNetAmountValidate(this.inputNetamount))
       this.netsales=this.inputNetamount
      //  this.amountlength= e.target.value.length
     }
  getAmountValidate(e) {
    this.invalidminamount = false
    this.invalidmaxamount = false
    if (e < this.defaultValues.MinAmount) {
      this.invalidminamount = true
      return true
    }
    if (e > this.defaultValues.MaxAmount) {
      this.invalidmaxamount = true
      return true
    }
    return false
  }
  getNetAmountValidate(e) {
    this.invalidminnetamount = false
    this.invalidmaxnetamount = false
    if (e < this.defaultValues.MinNetSalaes) {
      this.invalidminnetamount = true
      return true
    }
    if (e > this.defaultValues.MaxNetSales) {
      this.invalidmaxnetamount = true
      return true
    }
    return false
  }
  getMonthValidate(e) {
    this.invalidminmonth = false
    this.invalidmaxmonth = false
    if (e < this.minimummonth) {
      this.invalidminmonth = true
      return true
    }
    if (e > this.maxmummonth) {
      this.invalidmaxmonth = true
      return true
    }
    return false
  }

  sliderLoanamount(e) {
    this.inputamount = e
    this.getAmountValidate(e)
  }

  sliderNetIncome(e) {
     this.inputNetamount = e
     this.getNetAmountValidate(e)
  }

  sliderLoanmonth(e) {
    console.log(e)
   
    this.inputmonth = e
  
    this.getMonthValidate(e)

    if (this.lnType == 0 || this.lnType == 1) {
      this.tenurYears = Math.floor(this.loanmonth / 12);
      this.tenurMonths = Math.floor(this.loanmonth % 12);
    } else if (this.lnType == 2) {
      this.tenurYears = Math.floor(this.loanmonth / 12);
      this.tenurMonths = Math.floor(this.loanmonth % 12);
    }
  }
  getOption() {
    this.options = {
      chart: {
        type: 'pie',
        height: 222,
        options3d: {
          enabled: true,
          alpha: 55,
          beta: 0
          //  viewDistance: 200,       
        }
      },
     
      title: {
        text: ''
      },
      
      subtitle: {
        text: ''
      },
      credits: {
        enabled: false
      },
      
      plotOptions: {
        pie: {
          innerSize: 120,
          depth: 55,
          allowPointSelect: true,
          cursor: 'pointer',

          dataLabels: {
            enabled: false
          },

          showInLegend: false
        },
        //   column: {

        //     depth: 100,
        //     groupZPadding: 200,

        // }
      },
    
      series: [
        {
          name: 'Delivered amount ($)',
          data: [{
            name: 'Principal Amount ',
            y:this.principalamount
            // y: this.principalamount == 0 ? 2 : this.principalamount
            // sliced: true,
            // selected: true
          }, {
            name: 'Interest Amount ',
            y: this.interestamount,
            // y: this.interestamount == 0 ? 1 : this.interestamount,
            // /sliced: true,
            // selected: true
          },]
        }
      ],
      colors: ['#4572A7', '#ffcf01']
    };
  }
  ngOnInit(): void {
    // CALCULATORVALUE
    this.apihttp.get(this.api.CALCULATORVALUE).subscribe(
      (res: any) => {
        console.log("All Values:", res.data)
        // this.productValues = res.data;
        this.personalLoanValues = res.data[0];
        this.businessLoanValues = res.data[1];
        this.maxmummonth = this.personalLoanValues.MaxTenure
        this.minimummonth = this.personalLoanValues.MinTenure
        this.inputamount = this.personalLoanValues.DefaultAmount
        this.inputmonth = this.personalLoanValues.MinTenure
        this.loanamount = this.personalLoanValues.DefaultAmount
        this.defaultValues = this.personalLoanValues;
        console.log(this.defaultValues);
      },
      (error: any) => {
        console.log("Error", error);
      }
    );


    this.apihttp.get(this.api.TYPESOFLOANS).subscribe(
      (res: any) => {
        this.products = res.data;
        console.log("All Loans:", this.products)
        // if (!this.chartPreview)
        setTimeout(() => {
          Highcharts.chart('container', this.options);
        }, 100)
        // Highcharts.chart('container', this.options);

      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error.message)
        // Highcharts.chart('container', this.options);
      }
    );
    // this.reset();
  }

  onChange(prod) {
    this.inputmonth = 1;
    this.lnType = prod;
    this.loantype = prod
    this.invalidminmonth=false
    this.invalidmaxamount=false
    this.invalidmaxmonth=false
    this.invalidminamount=false
    this.invalidmaxnetamount=false
    this.invalidminnetamount=false
    // console.log(prod);
    if (prod == 1) {
      this.defaultValues = this.personalLoanValues;
      console.log(this.defaultValues);
      this.maxmummonth = this.personalLoanValues.MaxTenure
      this.minimummonth = this.personalLoanValues.MinTenure
      this.inputamount = this.personalLoanValues.DefaultAmount
      this.loanamount = this.personalLoanValues.DefaultAmount
      this.inputmonth = this.personalLoanValues.MinTenure
    } else if (prod == 2) {
      this.defaultValues = this.businessLoanValues;
      console.log(this.defaultValues);
      this.maxmummonth = this.businessLoanValues.MaxTenure
      this.minimummonth = this.businessLoanValues.MinTenure
      this.inputamount = this.businessLoanValues.DefaultAmount
      this.inputmonth = this.businessLoanValues.MinTenure
      this.netsales = this.businessLoanValues.MinNetSalaes
      this.loanamount = this.businessLoanValues.DefaultAmount
      this.inputNetamount =this.businessLoanValues.MinNetSalaes
    }
    this.reset();
  }



  formatLabel = 100


  calculateLoan() {

    this.isloading = true;
    if (this.lnType == 0 || this.lnType == 1) {
      this.tenurYears = Math.floor(this.loanmonth / 12);
      this.tenurMonths = Math.floor(this.loanmonth % 12);
    } else if (this.lnType == 2) {
      this.tenurYears = Math.floor(this.loanmonth / 12);
      this.tenurMonths = Math.floor(this.loanmonth % 12);
    }
    console.log(this.tenurMonths);
    const formData = new FormData()
    formData.append('loan_amount', this.loanamount + '')
    formData.append('loan_tenure', this.loanmonth + '')
    formData.append('loan_type_id', this.lnType + '')
    if (this.numyrs) {
      formData.append('turn_over', this.numyrs + '')
    }

    this.apihttp.post(this.api.LOANCALCULATOR, formData).subscribe(
      (res: any) => {
        console.log(res);
        // chartData = res;
        this.isloading = false
        if (res) {
          console.log('Loan_Data:', res)
          this.interestrate = res.data.min.intereset_rate
          this.principalamount = Number(res.data.min.principal_amount)
        
          this.interestamount = res.data.min.interest_amount
          //this.monthlyinstalment = res.data.avg.total_amount_payable / (this.loanmonth * 12)
          this.monthlyinstalment = res.data.min.monthly_repayment
          this.totalamount = res.data.min.total_amount_payable

          // this.minInterest = res.data.min.interest_amount;
          // this.maxInterest = res.data.max.interest_amount;
          // this.minTotAmnt = res.data.min.total_amount_payable
          // this.maxTotAmnt = res.data.max.total_amount_payable
          // this.minMonthInstal = res.data.min.monthly_repayment
          // this.maxMonthInstal = res.data.max.monthly_repayment

          this.getOption()
          setTimeout(() => {
            Highcharts.chart('container', this.options);
          }, 100)
          // this.reset();
        }
      },
      (error: any) => {
        this.isloading = false
        // this.chartPreview = true;
        console.log("Error", error);
        //   this.toastr.error(error.message)
        // this.reset();
      }
    );
  }

  getAmountMask(len, strlen) {
   
    let count = "1";
    for (let i = 0; i < len - 1; i++) {
      count += "1";
    }
    return this.numWithCommas(count, strlen);
  }
  numWithCommas(x, sl) {
    let e = x.substring(0, sl).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let t = e + x.substring(sl, x.length)
    return t.replaceAll("1", "0");
  }

  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
  numberWithCommas(x) {
    let val = x.toString().split(".");
     if (val.length > 1){
      val = x.toFixed(2)
     } else {
      val = val+".00"
     }
        
    let parts = val.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

  reset() {
    // this.chartPreview = true;

    // this.loantype = '1'
    // this.lnType == 1
    this.interestrate = 0
    this.principalamount = 0
    this.interestamount = 0
    this.monthlyinstalment = 0
    this.totalamount = 0
    this.tenurYears = 0;
    this.tenurMonths = 0;
    this.minInterest = 0;
    this.maxInterest = 0
    this.minTotAmnt = 0
    this.maxTotAmnt = 0
    this.minMonthInstal = 0
    this.maxMonthInstal = 0
    this.invalidminmonth=false
    this.invalidmaxamount=false
    this.invalidmaxmonth=false
    this.invalidminamount=false
    this.invalidmaxnetamount=false
    this.invalidminnetamount=false
    if (this.lnType == 1) {
      this.defaultValues = this.personalLoanValues;
      console.log(this.defaultValues);
      this.maxmummonth = this.personalLoanValues.MaxTenure
      this.minimummonth = this.personalLoanValues.MinTenure
      this.inputamount = this.personalLoanValues.DefaultAmount
      this.inputmonth = this.personalLoanValues.MinTenure
      this.loanmonth = this.personalLoanValues.MinTenure
      this.loanamount = this.personalLoanValues.DefaultAmount
       this.amountlength  = (this.loanamount+'').length
    } else if (this.lnType == 2) {
      this.defaultValues = this.businessLoanValues;
      console.log(this.defaultValues);
      this.maxmummonth = this.businessLoanValues.MaxTenure
      this.minimummonth = this.businessLoanValues.MinTenure
      this.inputamount = this.businessLoanValues.DefaultAmount
      this.inputmonth = this.businessLoanValues.MinTenure
      this.loanmonth = this.businessLoanValues.MinTenure
      this.loanamount = this.businessLoanValues.DefaultAmount
      this.amountlength  = (this.loanamount+'').length
      this.netsales = this.businessLoanValues.MinNetSalaes
    }
    this.getOption()
    Highcharts.chart('container', this.options);
  }
  updateValue(event) {
    console.log(event)
  }
  // My loans list
  loanApply() {
    this.router.navigate(['products', 'loan-form', this.lnType]);
    // this.router.navigate(['products', 'loan-form']);
  }
}