import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoanCalculatorComponent } from './loan-calculator.component';
// import { SharedModule } from 'src/app/shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';
import {MatSliderModule} from '@angular/material/slider';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { CurrencyMaskModule } from 'ng2-currency-mask';
const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: LoanCalculatorComponent },
      { path: 'loan-calculator', component: LoanCalculatorComponent }
    ]
  }
]
const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};

@NgModule({
  declarations: [LoanCalculatorComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(routes),
    MatSliderModule,
    NgxMaskModule.forRoot(maskConfigFunction),
    HighchartsChartModule,
    CurrencyMaskModule
    // SharedModule
  ]
})
export class LoanCalculatorModule { }
