import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MyLoansComponent } from './my-loans.component';
import { LoanDetailsComponent } from './loan-details/loan-details.component';
import { EmiScheduleComponent } from './emi-schedule/emi-schedule.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { CommentListComponent } from './comment-list/comment-list.component';
import { AddCommentComponent } from './add-comment/add-comment.component';
import { ViewCommentComponent } from './view-comment/view-comment.component';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMaskModule } from 'ng2-currency-mask';



const routes: Routes = [
  {
    path: '', children: [
      { path: '', component: MyLoansComponent },
      // { path: 'callback', component: CallbackComponent },
      { path: 'my-loans', component: MyLoansComponent },
      { path: 'loan-details/:id/:lid', component: LoanDetailsComponent, data: { breadcrumb: [
        { label: 'My Loans / Loan Details', url: 'my-loans/loan-details'}]} },
      { path: 'emi-schedule/:id', component: EmiScheduleComponent, data: { breadcrumb: [
        { label: 'My Loans / EMI Schedule', url: 'my-loans/emi-schedule'}]} },
      { path: 'transactions/:id', component: TransactionsComponent , data: { breadcrumb: [
        { label: 'My Loans / Transactions', url: 'my-loans/transactions'}]} },
      { path: 'comment-list/:id', component: CommentListComponent , data: { breadcrumb: [
        { label: 'My Loans / Comment List', url: 'my-loans/comment-list'}]}},
      { path: 'add-comment/:id', component: AddCommentComponent  , data: { breadcrumb: [
        { label: 'My Loans / Add Comment', url: 'my-loans/add-comment'}]}},
     
      { path: 'view-comment/:id/:lid', component: ViewCommentComponent  , data: { breadcrumb: [
        { label: 'My Loans / View Comment', url: 'my-loans/view-comment'}]}}
    ]
  }
]

const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};

@NgModule({
  declarations: [MyLoansComponent, 
    LoanDetailsComponent,
     EmiScheduleComponent,
      TransactionsComponent, 
      CommentListComponent,
       AddCommentComponent,
        ViewCommentComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    MatPaginatorModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(maskConfigFunction),
    BsDatepickerModule.forRoot(),
    NgxSkeletonLoaderModule.forRoot(),
    NgbModule,
    CurrencyMaskModule
  ],
  providers:[FileTypeValidator]
})
export class MyloanModule { }
