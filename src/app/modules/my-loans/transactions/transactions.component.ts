import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { DatePipe } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  loanId: any;
  transactionData: any;
  loading: boolean = false;
  empty: boolean;
  allTransactionDetail: boolean = false;
  userId: any;
  pipe = new DatePipe('en-US');
  dropdowndatas = []
  today = new Date();


  public transactionsForm = new FormGroup({
    transactionType: new FormControl('', [Validators.required]),
    transactionDuration: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)])
  })

  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  pageInfo = {
    page: 1,
    offset: 5
  }
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  constructor(private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      console.log(this.dropdowndatas)
    }

    
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
    this.getTransactionDetails();
  }

  getTransactionDetails() {
    this.loading = true;
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        let type = paramid["id"].split('~');
        console.log(type)
        this.loanId = type[1];
        if (type[0] == 'uid') {
          console.log(type[1]);
          this.userId = type[1];
          this.allTransactionDetail = true;
          this.loading = true;

          // this.transactionData = '';
          const formData = new FormData();
          formData.append("user_id", this.userId);
          formData.append("page", this.pageInfo.page + '');
          formData.append("offset", this.pageInfo.offset + '');
          this.http.post(this.api.GETALLTRANSTACTIONS, formData).subscribe(
            (res: any) => {
              this.transactionData = res.data.data;
              console.log(this.transactionData)
              this.paginationSettings["length"] = res.data.totalCount;
              this.paginationSettings["enablePagination"] = true;
              this.paginationSettings["totalPages"] = res.data?.totalPages;
              this.loading = false;
            },
            (error: any) => {
              console.log("Error", error);
            }
          );
        }
        else {
          const formData = new FormData();
          formData.append("year", "2021");
          formData.append("loan_id", this.loanId);
          formData.append("page", this.pageInfo.page + '');
          formData.append("offset", this.pageInfo.offset + '');

          // debugger;    
          this.http.post(this.api.TRANSTACTIONS, formData).subscribe(
            (res: any) => {
              this.transactionData = res.data.data;
              console.log(this.transactionData)
              this.paginationSettings["length"] = res.data.totalCount;
              this.paginationSettings["enablePagination"] = true;
              this.paginationSettings["totalPages"] = res.data?.totalPages;
              this.loading = false;
              // this.goTo(1)
            },
            (error: any) => {
              console.log("Error", error);
              // this.toastr.error(error)

            }
          );
        }
      }
    });

  }
  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.getTransactionDetails();
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }
  getStatusValue(status) {
    let arraval = this.getdropdownList('TransactionStatus')
    let filterstatus = arraval.filter((item) => item['id'] == status)
    if (filterstatus.length > 0)
      return filterstatus[0]['name']
    return ''
  }

  getPaymentType(status) {
    let arraval = this.getdropdownList('PaymentType')
    let filterstatus = arraval.filter((item) => item['id'] == status)
    if (filterstatus.length > 0)
      return filterstatus[0]['name']
    return ''
  }
  getAllTransactions() {
    this.loading = true;
    const formData = new FormData();
    if (this.transactionsForm.value.transactionDuration) {
      let fromDate = this.pipe.transform(this.transactionsForm.value.transactionDuration[0], 'yyyy-MM-dd') + '';
      let toDate = this.pipe.transform(this.transactionsForm.value.transactionDuration[1], 'yyyy-MM-dd') + '';
      formData.append("fromdate", fromDate);
      formData.append("todate", toDate);
    }
    if (this.transactionsForm.value.transactionType && this.transactionsForm.value.transactionType != '') {
      formData.append("status", this.transactionsForm.value.transactionType);
    }

    formData.append("user_id", this.userId);
    formData.append("page", this.pageInfo.page + '');
    formData.append("offset", this.pageInfo.offset + '');
    this.http.post(this.api.GETALLTRANSTACTIONS, formData).subscribe(
      (res: any) => {
        this.transactionData = res.data.data;
        console.log(this.transactionData)
        this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
      }
    );
    // else {
    //   this.loading = false;
    //   alert("Please select transaction type and duration")
    // }
  }
  requestDownload() {
    // this.loading = true;
    const formData = new FormData();
    if (this.transactionsForm.value.transactionDuration) {
      let fromDate = this.pipe.transform(this.transactionsForm.value.transactionDuration[0], 'yyyy-MM-dd') + '';
      let toDate = this.pipe.transform(this.transactionsForm.value.transactionDuration[1], 'yyyy-MM-dd') + '';
      formData.append("fromdate", fromDate);
      formData.append("todate", toDate);
    }
    if (this.transactionsForm.value.transactionType && this.transactionsForm.value.transactionType != '') {
      formData.append("status", this.transactionsForm.value.transactionType);
    }
    // formData.append("year", "2021");
    this.http.post(this.api.DOWNLOADRECEIPT, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
        // this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
      }
    );

    // else {
    //   // this.loading = false;
    //   alert("Please select transaction type and duration")
    // }
  }
  back() {
    // this.router.navigateByUrl()
    this.router.navigate(['my-loans', 'my-loans']);
  }
  localTime(dateString){
  
    return new Date(dateString + ' UTC');
 }
}
