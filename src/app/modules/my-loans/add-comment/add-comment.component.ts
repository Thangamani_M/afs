import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { UtilityService } from 'src/app/services/common/utility.service';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
import { Location } from '@angular/common'
@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {
  loading: boolean = false;
  isClickedOnce = true;
  imagePath: any = '';
  loanId: any;
  addField: any = [];
  imageJson = [{
    "id": '',
    "url": ''
  }]
  fieldname: any = []
  public commentForm = new FormGroup({
    userComments: new FormControl('', { validators: Validators.required }),
    loanImage: new FormControl('',[this.imagvalidator.validateImage])
    // [Validators.required, this.imagvalidator.validate]
  })

  constructor(private EncrDecr: EncrDecrService,private location: Location, private imagvalidator: FileTypeValidator, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        this.loanId = paramid["id"]
      }
    });
  }
  goBack() {
    this.location.back()
   // this.router.navigate(['my-loans', 'comment-list', this.loanId]);
  }

  cloneDiv() {
    let pos = this.getArrayMax(this.addField)
    this.addField.push(pos)
    this.commentForm.addControl('customFile' + pos, new FormControl('',[this.imagvalidator.validateImage]));
  }
  getArrayMax(array) {
    return (array.length == 0) ? 1 : ((Math.max.apply(null, array)) + 1);
  }
  removeDiv(key) {

    const index = this.addField.indexOf(key, 0);
    if (index > -1) {
      this.addField.splice(index, 1);
    }

    this.commentForm.removeControl('customFile' + key);
  }
  geturl(id) {
    let url = this.imageJson.filter(item => item.id == id)
    if (url.length > 0)
      return url[0].url
    return false
  }
  // For Image Upload
  fileChange(event: any, FormControl) {
    console.log("FormControl", FormControl)
    // debugger;

    if (event.target.files.length > 0) {
      const formData = new FormData();

      const file = event.target.files[0];
      console.log("Value", this.commentForm.controls)
      this.commentForm.controls[FormControl].setValue(file['name'])
      // formData.append("image", file, file['name']);
      this.commentForm.controls[FormControl].markAsDirty()
      formData.append("image", file, file['name']);
     if (!this.imagvalidator.validateImage(this.commentForm.controls[FormControl])) {
      const max_size = 25097152;
      console.log(event.target.files[0].size, (event.target.files[0].size / (1024 * 1024)).toFixed(2))
      if (event.target.files[0].size / (1024 * 1024) > max_size / (1024 * 1024)) {
        console.log(max_size / (1024 * 1024) + 'Mb')
        this.commentForm.controls[FormControl].setErrors({ 'filleSizeincorrect': true });
        return
      } else {
        this.commentForm.controls[FormControl].setErrors(null);
      }
      // this.fileLoader = true;
      this.fieldname.push(FormControl)

      this.apihttp.post(this.api.UPLOADCOMMENTIMAGE, formData).subscribe(
        (res) => {
          this.commentForm.controls[FormControl].setValue(res['data']['file_name']);
          this.imagePath = res['data']['path'];
          this.imageJson.push({ id: res['data']['file_name'], url: res['data']['path'] })
          const index = this.fieldname.indexOf(FormControl, 0);

          if (index > -1) {
            this.fieldname.splice(index, 1);
          }

        },
        (err) => {
          console.log(err);

          const index = this.fieldname.indexOf(FormControl, 0);
          if (index > -1) {
            this.fieldname.splice(index, 1);
          }
          // this.fileLoader = false;

        }
      );
     }

    }


  }


  submitComment() {
    this.isClickedOnce = false;

    if (this.commentForm.invalid || this.fieldname.length > 0) {
      return;
    }
    this.loading = true;
    let imagearray = [this.commentForm.value.loanImage]
    this.addField.forEach((element, i) => {
      if (element != '')
        imagearray.push(this.commentForm.value['customFile' + element])
    });
    let imagedata = imagearray.join(',')
    const formData = new FormData();
    formData.append("loan_comments", this.commentForm.value.userComments);
    formData.append("loan_id", this.loanId);
    formData.append("loan_image", imagedata);

    this.http.post(this.api.ADDLOANCOMMENTS, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.isClickedOnce = true;
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        this.router.navigate(['my-loans', 'comment-list', this.loanId]);
        this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        // this.loading = false;
        this.isClickedOnce = true;
        this.loading = false;
      }
    );
  }
  cancelform() {

  }
}
