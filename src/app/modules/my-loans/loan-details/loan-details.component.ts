import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-loan-details',
  templateUrl: './loan-details.component.html',
  styleUrls: ['./loan-details.component.css']
})

export class LoanDetailsComponent implements OnInit {
  tabsData: any;
  tabsLoader: any=false;
  sectionId: any = 1;
  filterFields: any;
  draftId: any;
  showFormData: any = false;
  fullPageLoader: boolean = false;
  errorMessage;
  loanTypeId: any;
  imageJson = [{
    "id": '',
    "url": ''
  }]
  commentStatus: any;
  dropdowndatas: any = [];
  constructor(private apihttp: ApiHttpService, private location: Location, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      console.log(this.dropdowndatas)
    }
    this.tabsLoader=true
    this.route.params.subscribe((paramid: Params) => {
      console.log(paramid);
      if (paramid["id"]) {
        this.draftId = paramid["id"];
        this.loanTypeId = paramid["lid"];
        const formData = new FormData();
        formData.append("id", this.loanTypeId);
        formData.append("draftid", this.draftId);
        // formData.append("draftid", this.draftId);

        // debugger;    
        this.http.post(this.api.LOANMASTERTABS, formData).subscribe(
          (res: any) => {
            this.errorMessage=null
            this.tabsData = res.data.data;
            this.tabsLoader=false
            //  console.log(this.tabsData)
            if (this.tabsData.length > 0)
              this.goTo(this.tabsData[0].id, 1)
          },
          (error: any) => {
            this.tabsLoader=false
            console.log("Error", error);
            this.errorMessage=error.message
            // this.toastr.error(error)

          }
        );
      }
    });
  }

  getdropdownList(key, val) {
    let dropdownlist = '-'
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      // dropdownlist = filterdata[0]['value']
      let filternmae = filterdata[0]['value'].filter(item => item['id'] == val)
      if (filternmae.length > 0) {
        dropdownlist = filternmae[0].name
      }
    }

    return dropdownlist
  }
  getAllUrl(ids) {
    console.log("IDS", ids)
    if (ids.length > 0) {
      // this.fileLoader = true;
      const formData = new FormData()
      formData.append("json", JSON.stringify(ids));

      this.apihttp.post(this.api.IMAGEMULTIUPLOAD, formData).subscribe(
        (res) => {
          // this.fileLoader = false;
          console.log(res)
          if (res['data'].length > 0)
            this.imageJson = res['data']
        }, error => {
          // this.fileLoader = false;
        })
    }

  }

  geturl(id) {
    let url = this.imageJson.filter(item => item.id == id)
    if (url.length > 0)
      return url[0].url
    return false
  }

  goTo(id: any, first?) {
    if (!first)
      this.fullPageLoader = true;
    this.showFormData = true;
    const loanData = new FormData();
    loanData.append("id", id)
    loanData.append("draftid", this.draftId)

    this.http.post(this.api.CREATEFORMUI, loanData).subscribe(
      (res: any) => {
        this.filterFields = res.data;
        this.fullPageLoader = false;
        let arraids: any = []

        this.filterFields.forEach(sec => {
          sec.forms.forEach((item) => {
            if (item.fieldType == 'Photo' || item.fieldType == 'Signature' || item.fieldType == 'docUpload') {
              if (item['value']) {
                arraids.push(item['value'])
              }
            }
          });
        });
        this.getAllUrl(arraids)

        this.sectionId = id
        console.log("Get form details", this.filterFields);
        this.fullPageLoader = false;
        this.sectionId = id
      },
      (error: any) => {
        this.fullPageLoader = false;
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );
  }

  getnumber(val) {
    return parseInt(val)
  }
  goBack() {
    this.location.back()
  }
}
