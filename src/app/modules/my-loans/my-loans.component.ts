import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeletePopupComponent } from 'src/app/shared/components/delete-popup/delete-popup.component';


@Component({
  selector: 'app-my-loans',
  templateUrl: './my-loans.component.html',
  styleUrls: ['./my-loans.component.css']
})
export class MyLoansComponent implements OnInit {
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  loansData: any;
  loanDetails: any = {};
  activeLoanPosition: any = [];
  loading: boolean = false;
  popupLoading: boolean = false;
  empty: boolean = false;
  preClosureAmount: any;
  earlyAmount: any;
  cardData: any;
  preClosureData: boolean = false;
  payEarlyData: boolean = false;
  payInfull: boolean = false;
  payEMI: boolean = false;
  otherAmount: boolean = false;
  newCard: boolean = false;
  openButton: boolean = true;
  closeButton: boolean = false;
  showCards: boolean = true;
  cardInfo: boolean = false;
  proceedToPay: boolean = true;
  tabName: any;
  otherAmountData: any = '';
  totalAmountPay: any;
  selectedCardToken: any;
  tempcondition=false
  loanId: any;
  cardCheckout: boolean = false;
  tokenCheckout: boolean = false;
  forLoanDetails: boolean = false;
  waitingLoanDetails: boolean = true;
  // loanSubDetails: any = false;
  // activeLoans: any = false;
  @ViewChild('myFormPost') myFormPost: ElementRef;
  @ViewChild('page') page: MatPaginator;
  @ViewChild('gotoPage') gotoPage: ElementRef;
  inputData: any = [];
  loanStatus: boolean = false;
  loanStatusData: any;
  viewLoanDetails: any;
  statusPosition: any;
  allPayData: string;
  state$: Observable<object>;
  pageInfo = {
    page: 1,
    offset: 5
  }
  transactionAmount: any;
  dropdowndatas: any = [];
  validOtherAmount: boolean = false;
  makeDefault=false
  makeAutoPayment=false
  cardid="0";
  popupDeleteLoading=false;
  submitted=false
  validMinOtherAmount=false;
  constructor(private EncrDecr: EncrDecrService,private modalService: NgbModal, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { this.paginationSettings["enablePagination"] = false; }

  public newCardForm = new FormGroup({
    holderName: new FormControl('', [Validators.required]),
    cardNumber: new FormControl('', [Validators.required]),
    expiryDate: new FormControl('', [Validators.required,isValidDate]),
    // expiryYear: new FormControl('', [Validators.required]),
    cardCvv: new FormControl('', [Validators.required])
  })

  ngOnInit(): void {
    localStorage.removeItem('repeatcustomer');
    this.viewAllloans()


    let masterdata = localStorage.getItem('masterdata')
    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      // console.log(this.dropdowndatas)
    }
    
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }
  // My loans list
  viewAllloans(page?) {
    // debugger;
    this.loanDetails = {};
    this.activeLoanPosition = [];
    this.statusPosition = -1
    this.loading = true;
    let userId;
    // let userId = JSON.parse(localStorage.getItem('user'));
    this.paginationSettings["offset"] = 5;
    let getUser = localStorage.getItem('user');
    if (getUser) {
      userId = JSON.parse(getUser).userId
    }
    const formData = new FormData();
    formData.append("UserId", userId);
    formData.append("page", this.pageInfo.page + '');
    formData.append("offset", this.pageInfo.offset + '');

    console.log(formData);

    this.http.post(this.api.LOANDRAFTLIST, formData).subscribe(
      (res: any) => {
        // debugger;
        this.loansData = res.data;
        console.log("All Loans", this.loansData)
        // localStorage.setItem("notificationCount", JSON.stringify(res.data.notification_count));
        // console.log('Notifi_count', JSON.stringify(res.data.notification_count))
        // this.loansData.data.forEach(element => {
        for (var i = 0; i < this.loansData.data.length; i++) {
          // if (element.LoanSectionType == 'Completed' && element.isEditable == 'N') {  && item.LoanStatus=='Active'
          if (this.loansData.data[i].LoanSectionType == 'COMPLETED' && this.loansData.data[i].isEditable == 'N' && this.loansData.data[i].LoanStatus == 'Active') {
            this.activeLoanDetails(this.loansData.data[i].Id, i)
            // console.log(this.loansData.data[i].Id)
            // break;

          }

          // });
        }
        const param = this.route.snapshot.queryParamMap;
        console.log(" this.activatedRoute", param)
        if (param.get('DraftId')) {
          for (var i = 0; i < this.loansData.data.length; i++) {
            if (this.loansData.data[i].DraftId == param.get('DraftId')) {
              this.viewLoanStatus(this.loansData.data[i].Id, i)
              break;
            }
          }
        }
        window.scroll(0, 0)
        this.paginationSettings["length"] = this.loansData.total_items;
        this.paginationSettings["enablePagination"] = true;
        this.paginationSettings["totalPages"] = this.loansData?.total_pages;
        this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.empty = true;
      }
    );
  }
  getServerData(event: any) {
    if (event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      // this.gotoPage.nativeElement.value = this.page.pageIndex + 1;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo = { page: event.pageIndex + 1, offset: this.paginationSettings['offset'] };
      this.viewAllloans()
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
  activeLoanDetails(id: any, position: any) {
    // debugger;
    this.activeLoanPosition.push(position);
    console.log('AL_Position:', this.activeLoanPosition)
    // if () {
    //   this.activeLoans = true;
    const loanData = new FormData();
    loanData.append("loan_id", id);
    // console.log('Loan_ID:', id);

    // debugger;    
    this.http.post(this.api.LOANDETAILS, loanData).subscribe(
      (res: any) => {
        this.waitingLoanDetails = false;
        this.loanDetails = Object.assign(this.loanDetails, { [position]: res.data });
        console.log('Loan Details:', this.loanDetails)
      },
      (error: any) => {
        this.waitingLoanDetails = false;
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );
  }

  viewLoanStatus(id: any, position: any) {
    // debugger;
    // viewLoanStatus(id: any) {
    this.statusPosition = position;
    this.forLoanDetails = true;
    this.loanStatus = true;
    // debugger;
    const formData = new FormData();
    // formData.append("loan_id", "R6YKBREUMVGJVKV");
    formData.append("loan_id", id);

    this.http.post(this.api.GETLOANSTATUS, formData).subscribe(
      (res: any) => {
        this.forLoanDetails = false;
        this.viewLoanDetails = res.data;
        this.loanStatusData = res.data.data;
        console.log("LoanStatus:", this.loanStatusData)

      },
      (error: any) => {
        this.forLoanDetails = false;
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );
  }




  continueForm(id: any, draftId) {
    // debugger;
    localStorage.setItem('continueLoan', '1');
    localStorage.setItem('draft_id', btoa(draftId))
    this.router.navigate(['products', 'loan-form', id]);
  }

  deleteLoan(id: any, draftId) {
    // debugger;
    // LOANDELETE
    this.loading = true;
    const formData = new FormData();
    // formData.append("loan_id", "R6YKBREUMVGJVKV");
    formData.append("DraftId", draftId);

    this.http.post(this.api.LOANDELETE, formData).subscribe(
      (res: any) => {
        this.loading = false;
        console.log("LoanStatus:", res);

        if (localStorage.getItem('selecttabs')) {
          let tab = JSON.parse(atob(localStorage.getItem('selecttabs')!!))
          if (tab.data) {
            tab.data.forEach((element, i) => {
              if (this.util.getFormArrayStorage(element.id)) {
                this.util.removeFormArrayStorage(element.id)
              }
            });
          }
        }
        this.toastr.success(res['message'], '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        this.viewAllloans();
      },
      (error: any) => {
        this.loading = false;
        this.forLoanDetails = false;
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );


  }

  viewDetail(draftId: any, LoanTypeId: any) {
    this.router.navigate(['my-loans', 'loan-details', draftId, LoanTypeId]);
  }


  emiSchedule(id: any) {
    this.router.navigate(['my-loans', 'emi-schedule', id]);
  }
  transactions(id: any) {
    this.router.navigate(['my-loans', 'transactions', 'lid~' + id]);
  }

  allTransactions() {
    // debugger;
    let userId = localStorage.getItem('user_id');

    this.router.navigate(['my-loans', 'transactions', 'uid~' + userId]);
  }

  loanPreClosure(id: any) {
    this.router.navigate(['services', 'services', 'preclosure~' + id]);

  }
  changeRepayMode(id: any) {
    this.router.navigate(['services', 'services', 'repayment~' + id]);
  }
  // loanPreClosure(id: any) {
  //   this.loanId = id;

  //   this.popupLoading = true;
  //   this.payEarlyData = false;
  //   this.preClosureData = true;
  //   const loanData = new FormData();
  //   loanData.append("loan_id", this.loanId);
  //   // debugger;    
  //   this.http.post(this.api.LOANPRECLOSURE, loanData).subscribe(
  //     (res: any) => {
  //       this.preClosureAmount = res.data;
  //       this.popupLoading = false;
  //       // console.log("Preclosure_data:", this.preClosureAmount);

  //       // console.log('Pre Closure Amount:', this.preClosureAmount)
  //     },
  //     (error: any) => {
  //       console.log("Error", error);
  //       // this.toastr.error(error)

  //     }
  //   );
  // }
  getCardDetails() {
    const loanData = new FormData();
    loanData.append("loan_id", this.loanId);
    // debugger;    
    this.http.post(this.api.GETCARDDETAILSBYUSERID, loanData).subscribe(
      (res: any) => {
        this.earlyAmount = res.data;
        this.popupLoading = false;
        this.popupDeleteLoading = false;
        this.cardData = this.earlyAmount.data;
         if(this.cardData && this.cardData.length>0)
         {
           this.cardData.forEach(item=>{
             if(item.IsDefault==1){
              this.selectedCardToken = item.card_token;
              this.cardid =item.cardid
             }
           })
         }
        console.log('Card Details:', res,this.selectedCardToken )
        this.setTab('Pay EMI');

      },
      (error: any) => {
        this.popupLoading = false;
        this.popupDeleteLoading = false;
        console.log("Error", error);
        // this.toastr.error(error)

      }
    );
  }
  getAmount(val){
    return val
  }
  gotoPayment() {
    this.proceedToPay = false;
    this.cardInfo = true;
    this.getCardDetails();
  }

  payEarly(id: any) {
    // debugger;
    this.popupLoading = true;
    this.cardInfo = true;
    this.preClosureData = false;
    this.payEarlyData = true;
    this.loanId = id;
    this.getCardDetails();
  }
  changeAmount(event: any) {
    // alert(event.target.value)
    // {{earlyAmount.totalamount }}
    console.log(this.otherAmountData,this.earlyAmount.totalamount)
    if ((this.otherAmountData > this.earlyAmount.emiamount)) {
      this.validOtherAmount = true;
    } else {
      this.validOtherAmount = false;
    }

    if (this.otherAmountData <1) {
      this.validMinOtherAmount = true;
    } else {
      this.validMinOtherAmount = false;
    }
   
  }

  setTab(tab: any) {
    this.validOtherAmount = false;
    this.tabName = tab;
    this.payInfull = false;
    this.payEMI = false;
    this.otherAmount = false;
    if (tab == "Pay in full") {
      this.payInfull = true;
      
    } else if (tab == "Pay EMI") {
      this.payEMI = true;
    } else if (tab == "Other Amount") {
      this.otherAmountData = '';
      this.otherAmount = true;
      // console.log('Other Amount', this.otherAmountData);     
    }
  }

  addNewCard() {
    this.showCards = false;
    this.newCard = true;
    this.openButton = false;
    this.closeButton = true;

    // if (this.cardData) {
    //   const formData = new FormData();
    //   formData.append("holderName", this.newCardForm.value.holderName);
    //   formData.append("cardNumber", this.newCardForm.value.cardNumber);
    //   formData.append("expiryDate", this.newCardForm.value.expiryDate);
    //   formData.append("cardCvv", this.newCardForm.value.cardCvv);
    //   console.log(formData);
    // }
  }

  closeCardForm() {
    this.newCard = false;
    this.showCards = true;
    this.openButton = true;
    this.closeButton = false;
  }

  onSelectedCard(token: any,cardid) {
    console.log(token,cardid)
    this.selectedCardToken = token;
    this.cardid =cardid
  }
  removeCard(id){
    
    this.modalService.open(DeletePopupComponent, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      console.log(res)
      if (res && res == 1) {
        this.popupDeleteLoading = true;
        this.http.post(this.api.DELETECARD, { id: id }).subscribe(
          (res: any) => {
            this.getCardDetails();
          }, error => {
            this.popupDeleteLoading = false;
          })
      }

    }, (res) => {
      console.log(res)
    });
    
  }
  changeMakeDefault(e){
  console.log(e.target.value,this.makeDefault)
  }
  changeMakeAuto(e){
    console.log(e.target.value,this.makeAutoPayment)
  }
  makePayment() {
    // this.validOtherAmount = false;
    // 
    // console.log(this.newCardForm.controls.value)
    // debugger;
    if(this.otherAmount && (!this.otherAmountData || (this.otherAmountData && this.otherAmountData<1))) 
     {
        this.validMinOtherAmount =true
      //this.toastr.error("Please enter minimum of $1",'',{ positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
      return
    }
    if(this.otherAmount && this.validOtherAmount )
       return
    this.submitted=true
    if(this.newCard && this.newCardForm.invalid) return
    if (this.newCardForm.valid ||this.selectedCardToken || this.selectedCardToken !== undefined || this.totalAmountPay !== undefined) {
      if(!this.tempcondition){
        this.popupDeleteLoading = false;
        this.toastr.error("Please check Terms of Use",'',{ positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
      return
      }
      if (this.preClosureData) {
        this.totalAmountPay = this.preClosureAmount.amount;
      } else if (this.payInfull) {
        this.totalAmountPay = this.earlyAmount.totalamount;
      } else if (this.payEMI) {
        this.totalAmountPay = this.earlyAmount.emiamount;
      } else if (this.otherAmount) {

        this.totalAmountPay = this.otherAmountData;
      }

      if (typeof (this.totalAmountPay) === 'string') {
        this.transactionAmount = this.totalAmountPay.replace(',', '')
      } else {
        this.transactionAmount = this.totalAmountPay
      }
      let userInfo;
      let getUser = localStorage.getItem('userDetails');
      if (getUser) {
        userInfo = JSON.parse(getUser)
      }
      let randomVal = Math.floor(10000000 + Math.random() * 90000000);
     // let deviceToken = localStorage.getItem('device_token');
     let deviceToken = localStorage.getItem('accessTOKEN')
      // console.log(deviceToken);
      this.popupDeleteLoading = true;
      let payData = {
        "reference_number": randomVal,
        "device_type": "3",
        "device_token": deviceToken,
        "payment_method": '2',
        "currency_code": "JMD",
        "loan_id": this.loanId,
        "amount": this.transactionAmount,
      }
      if(this.selectedCardToken && !this.newCard)
          payData =Object.assign(payData,{"CardToken":this.selectedCardToken})
      console.log('PAYMENT DATA_:', payData);
      this.allPayData = this.EncrDecr.set(JSON.stringify(payData));
      let encryptData = this.EncrDecr.getEncoding(this.allPayData);
      let token;
      token = localStorage.getItem("accessTOKEN");
      console.log('Token', token)
      let httpHeaders = new HttpHeaders({
        "Content-Type": "text/plain",
        "Authorization": token,
        "Accept": "*/*"
      });

      // console.log('Encrypted:', encryptData);
      this.http.post(this.api.MAKEPAYMENT, encryptData, { headers: httpHeaders }).subscribe(
        (res: any) => {
          this.popupDeleteLoading = false;
          this.inputData.push({ name: "ClientId", value: res.data.ClientId })
          this.inputData.push({ name: "TransactionId", value: res.data.TransactionId })
          this.inputData.push({ name: "CurrencyCode", value: "JMD" })
          this.inputData.push({ name: "Amount", value: this.transactionAmount })
          this.inputData.push({ name: "Signature", value: res.data.Signature })
          this.inputData.push({ name: "ReturnToMerchant", value: "Y" })
          this.inputData.push({ name: "AutoRedirect", value: "Y" })
          this.inputData.push({ name: "CustomerReference", value: userInfo?.FirstName })
          this.inputData.push({ name: "BillToEmail", value: userInfo?.Email })
          this.inputData.push({ name: "BillToTelephone", value: "" })
          this.inputData.push({ name: "CustomerInvoice", value: "" })
          this.inputData.push({ name: "BillToAddress", value: "" })
          this.inputData.push({ name: "BillToCountry", value: "" })
          this.inputData.push({ name: "BillToState", value: "" })
          this.inputData.push({ name: "BillToFirstName", value: "" })
          this.inputData.push({ name: "BillToCity", value: "" })
          this.inputData.push({ name: "BillToZipPostCode", value: "" })
          this.inputData.push({ name: "CustomMessage", value: (this.makeDefault?1:0)+"~~"+(this.makeAutoPayment?1:0)+"~~"+this.cardid })

          // // USE CARD DETAILS:
          if (this.newCard) {
            //   window.location.href = "https://paymentdemo.myamberpay.com/gateway/v1/quick-checkout";
            this.inputData.push({ name: "CustomerReference", value: this.newCardForm.value.holderName })
            this.inputData.push({ name: "CardHolderName", value: this.newCardForm.value.holderName })
            this.inputData.push({ name: "CardNumber", value: this.newCardForm.value.cardNumber })
            this.inputData.push({ name: "CVV", value: this.newCardForm.value.cardCvv })
            this.inputData.push({ name: "Month", value: this.newCardForm.value.expiryDate.substring(0,2) })
            this.inputData.push({ name: "Year", value: this.newCardForm.value.expiryDate.substring(2,6) })
            this.inputData.push({ name: "CardTokenize", value: "Y" })
            console.log("Card_data:", this.inputData);
             this.cardCheckout = true;
          }

          // //SELECT CARD
          if ((this.showCards || this.selectedCardToken) && (!this.newCard)) {
            //   window.location.href = "https://paymentdemo.myamberpay.com/gateway/v1/tokenize-checkout";
            this.inputData.push({ name: "3DSFlag", value: "Y" })
            this.inputData.push({ name: "PaymentMethod", value: "" })
            this.inputData.push({ name: "CardToken", value: this.selectedCardToken })
            this.tokenCheckout = true;
            console.log("Token_data:", this.inputData);
          }
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          this.popupDeleteLoading = false;
        }
      )
    }
    else {

      this.popupDeleteLoading = false;
         this.toastr.error("Please select/add card",'',{ positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
    }

  }

  ngAfterViewChecked() {
    if (this.cardCheckout && this.myFormPost) {
      this.myFormPost.nativeElement.submit();
      this.cardCheckout = false;


    } else if (this.tokenCheckout && this.myFormPost) {
      this.myFormPost.nativeElement.submit();
      this.tokenCheckout = false;
    }
  }
  viewComments(id: any) {
    // debugger;
    // alert('')
    // this.router.navigateByUrl('comment-list');
    this.router.navigate(['my-loans', 'comment-list', id]);
  }


  localTime(dateString) {
    return new Date(dateString + ' UTC');
  }
  
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  keyPressAlpha(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z\s-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}

export const isValidDate = (c: FormControl) => {
  if(!c.value) return null
  if ((c.value).match(/^(0[1-9]|1[0-2])\/?([0-9]{4})$/)) {
      // get midnight of first day of the next month
      const expiry = new Date(c.value.substring(2,6),c.value.substring(0,2));
      const current = new Date();

    return expiry.getTime() > current.getTime() ? null: {
      validateDate:true
    } ;
  } else {
    return {
      validateDate:true
    }
  }
}