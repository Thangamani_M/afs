import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { UtilityService } from 'src/app/services/common/utility.service';
import { Location } from '@angular/common'
@Component({
  selector: 'app-view-comment',
  templateUrl: './view-comment.component.html',
  styleUrls: ['./view-comment.component.css']
})
export class ViewCommentComponent implements OnInit {
  loading: boolean = true;
  allComments: any;
  commentId: any;
  viewComment: any;
  loanId: any;

  constructor(private EncrDecr: EncrDecrService,private location: Location, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        this.commentId = paramid["id"]
      }
      if (paramid["lid"]) {
        this.loanId = paramid["lid"]
      }
    })

    const formData = new FormData();
    formData.append("loan_id", this.loanId);
    this.apihttp.post(this.api.LOANCOMMENTS, formData).subscribe(
      (res: any) => {
        this.allComments = res.data;
        console.log("View Comment:", this.allComments)

        this.allComments.find(item => {
          if (item.id == this.commentId) {
            this.viewComment = item;
            console.log(this.viewComment);
          }
        })
        this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        this.loading = false;

      }
    );
  }

  addComment() {
    this.router.navigate(['my-loans', 'add-comment', this.loanId]);
  }

  goBack() {
    this.location.back()
    // this.router.navigate(['my-loans', 'my-loans']);
  }
}
