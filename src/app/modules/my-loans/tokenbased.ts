export class Input {
    name: string;
    value: string;
    multiline?: boolean = false;
  }
  
  export const INPUTS: Input[] = [
    {name: 'A', value: 'a'},
    {name: 'B', value: 'b'},
    {name: 'C', value: 'c'},
    {name: 'D', value: 'd'},
    {name: 'E', value: 'e\ne\ne\ne\ne', multiline: true},
  ];
  