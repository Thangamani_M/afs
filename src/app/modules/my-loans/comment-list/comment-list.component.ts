import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { UtilityService } from 'src/app/services/common/utility.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {
  paginationSettings = { enablePagination: false, length: 0, totalPages: 0, offset: 5 };
  loading: boolean = false;
  allComments: any;
  loanId: any;
  commentId: any;
  @ViewChild('page') page: MatPaginator;
   pageInfo={ page:1, offset: 5};
  constructor(private EncrDecr: EncrDecrService, public activatedRoute: ActivatedRoute, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }


  ngOnInit(): void {
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        this.loanId = paramid["id"]
      }
    });



    this.loading = true;

    const formData = new FormData();
    formData.append("loan_id", this.loanId);
    // this.apihttp.get(this.api.LISTLOANCOMMENTS).subscribe(
    this.apihttp.post(this.api.LOANCOMMENTS, formData).subscribe(
      // this.apihttp.get(this.api.LISTLOANCOMMENTS).subscribe(
      (res: any) => {
        this.allComments = res.data;
        console.log("All Comments:", this.allComments)
        this.loading = false;
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
      }
    );
  }


  getServerData(event: any) {
    if(event.pageIndex <= this.paginationSettings['totalPages'] - 1 && event.pageIndex >= 0) {
      this.page.pageIndex = event.pageIndex;
      this.paginationSettings['offset'] = event.pageSize || this.paginationSettings['offset'];
      this.pageInfo={ page: event.pageIndex+1, offset: this.paginationSettings['offset']};
      // this.viewAllloans()
    } else {
      this.toastr.info('Please enter valid page number', '', { closeButton: true });
    }
  }
  addComment() {
    this.router.navigate(['my-loans', 'add-comment', this.loanId]);
  }
  goBack() {
    this.router.navigate(['my-loans', 'my-loans']);
  }
  viewComment(id: any) {
    this.commentId = id;
    this.router.navigate(['my-loans', 'view-comment', this.commentId, this.loanId]);
  }

}
