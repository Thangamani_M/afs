import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/common/utility.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-emi-schedule',
  templateUrl: './emi-schedule.component.html',
  styleUrls: ['./emi-schedule.component.css']
})
export class EmiScheduleComponent implements OnInit {
  loanId: any;
  emiData: any;
  loading: boolean = false;
  empty: boolean;
  lmsloanid:any;

  constructor(private apihttp: ApiHttpService, private location: Location, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private fb: FormBuilder, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.route.params.subscribe((paramid: Params) => {
      if (paramid["id"]) {
        this.loanId = paramid["id"];
        const formData = new FormData();
        // formData.append("id", "1");
        formData.append("loan_id", this.loanId);

        // debugger;    
        this.http.post(this.api.EMISCHEDULELIST, formData).subscribe(
          (res: any) => {
            this.emiData = res.data;
            this.lmsloanid = res.LMSLoanId
            console.log(this.emiData)
            this.loading = false;
            // this.goTo(1)
          },
          (error: any) => {
            console.log("Error", error);
            // this.toastr.error(error)

          }
        );
      }
    });
  }

  goBack() {
    this.location.back()
  }
}
