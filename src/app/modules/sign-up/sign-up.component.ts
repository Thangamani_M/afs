import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ToastrService } from 'ngx-toastr';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { UtilityService } from 'src/app/services/common/utility.service';
import { NgxOtpInputConfig } from "ngx-otp-input";
import { environment } from 'src/environments/environment';
import { VerifySignUpComponent } from './verify-signup/verify-signup.component';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { MarketinginfoComponent } from '../login/marketingInfo/marketinginfo.component';
import { DatePipe } from '@angular/common'
import { GetotherinfoComponent } from '../login/getotherinfo/getotherinfo.component';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

// import{} from '../../shared/api/'
// import countrycodes from "../../shared/api/country-code.json";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
  providers: [NgbCarouselConfig,SocialAuthService]
})
export class SignUpComponent implements OnInit {
  // myDateValue: Date;

  encPassword: any;
  password: any;
  showPword: boolean = false;
  showCPword: boolean = false;

  closeModal: string;

  loading: boolean = false;
  popupLoading: boolean = false;
  isSumitted = false
  isloading = false
  occupationType = 0;

  socialUser: SocialUser;
  isLoggedin: boolean
  employerDatas: any = [];
  isClickedOnce = true;

  trnloader = false
  tnrerror = ''
  trnnum;
  sitekey=environment.recaptcha.siteKey
  keyword = 'name';
  // public countries = [];
  allEmployers = [];
  jsonData: any;
  allContries: any = [];
  maxDate: Date;
  year_: string;
  scrollBlocks = [
    { title: 'A Faster Way', short: 'A faster way to apply for your personal and business loans.', src: "../../../../assets/img/scroll-1.png" },
    { title: 'Personal Loans', short: 'Personal loans are available for both private and public sector employees.', src: "../../../../assets/img/scroll-2.png" },
    // { title: 'Third Slide', short: 'Third Slide Short', src: "../../../../assets/img/Login-bg.png" }
  ];
  sclEncEmail: string;
  sclEncProfileId: string;
  issocialload=false
  socialstatus='0';
  dropdowndatas = []
  dropdownmarketing = []
  constructor(public datepipe: DatePipe, private router: Router, private fb: FormBuilder, private toastr: ToastrService, private socialAuthService: SocialAuthService, private modalService: NgbModal, private EncrDecr: EncrDecrService, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private util: UtilityService) { }

  public registerForm: FormGroup;
  ngOnInit(): void {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 10)
    this.getCurrentAgeYear18();
    // this.json.getCountryData()
    //   .subscribe((data: any): void => {
    //     console.info(data);
    //     this.jsonData = data;
    //   });

    this.http.get(this.api.COUNTRYDATAS).subscribe(data => {
      console.log('C:', data)
      this.allContries = data;
    });

    this.getForm()
    // this.startTimer();
    // Master Data
    this.issocialload=true
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
      console.log(masterdata);
      
      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      this.issocialload=false
      if (masterd) {
      
        this.dropdowndatas = JSON.parse(atob(masterd));
        this.getSocialStatus("GeneralSettings")
      }
      // this.employerData = masterdata['data'].;  
      masterdata['data'].forEach(element => {
     
        if (element.key == "Employer") {
         
          this.employerDatas = element.value;
          // console.log('Employers', this.employerDatas);
          this.allEmployers = this.employerDatas;
        }
      });
      // localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
    },error=>{
      this.issocialload=false
    })

    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
      if (this.socialUser) {
       
        this.sclEncEmail = this.EncrDecr.set(this.socialUser.email);
        this.sclEncProfileId = this.EncrDecr.set(this.socialUser.id);

        let email = this.EncrDecr.getEncoding(this.sclEncEmail)
        let profileid = this.EncrDecr.getEncoding(this.sclEncProfileId)
        window.scroll(0, 0)
        const formData = new FormData()
        formData.append('name', this.socialUser.firstName)
        // formData.append('email', this.socialUser.email)
        // formData.append('profileid', this.socialUser.id)
        formData.append('lastname', this.socialUser.lastName)
        formData.append('email', email)
        formData.append('profileid', profileid)

        if (localStorage.getItem('socialLogin') && localStorage.getItem('socialLogin') == '1') {
          this.isloading = true
          localStorage.setItem('socialLogin', '0')
          this.http.post(this.api.SOCIALLOGIN, formData, { observe: 'response' as 'body' }).subscribe(
            (res: any) => {
              localStorage.setItem('login', 'social')

              this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
                this.util.signIn(res, 2);
                this.getSocialRes(res)
                // this.isloading = false
                localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
                // this.router.navigateByUrl('/products');

                //   setTimeout(() => {
                //     if (res.body.data.user.show_marketing_info) {
                //     this.modalService.open(MarketinginfoComponent, { size: 'lg' })
                //   }
                //   }, 3000);

                this.deviceUpdate(res.body.data.user.id)
              }, error => {
                this.isloading = false
              })

            },
            (error: any) => {
              this.isloading = false
              console.log("Error", error);
            }
          );
        } else {

          // this.isloading = false
        }
      } else {
        // this.isloading = false
      }

    });
  }

  getCurrentAgeYear18() {
    var date = new Date();
    var intYear = date.getFullYear() - 18;
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    this.year_ = intYear + "-" + month + "-" + day;
    // console.log(new Date(this.year_))
    this.maxDate = new Date(this.year_);
    console.log(this.maxDate)
  }

  deviceUpdate(uid){
    let token =localStorage.getItem('device_token')
    if(token){
    const formData = new FormData();
    formData.append("device_version", '1');
    formData.append("user_id", uid);
    formData.append("device_token",token);
    formData.append("device_type", '3');
    this.http.post(this.api.DEVICEUPDATE,formData).subscribe(masterdata => {
      console.log(masterdata)
    },error=>{
      console.log(error)
    })
    }
  }
  

  getSocialRes(res?) {
    this.isloading = false;
    let user = localStorage.getItem("userDetails");
    if (user) {
      console.log(JSON.parse(user))
      let checkTrn = JSON.parse(user)

      if (!checkTrn.DateOfBirth || checkTrn.DateOfBirth == "" || !checkTrn.trn_number || checkTrn.trn_number == "") {
        this.modalService.open(GetotherinfoComponent, { size: '' }).result.then((data) => {
          if (data == 1) {
            setTimeout(() => {
              this.isloading = false;
            }, 200);
            // this.isloading = false

            this.router.navigateByUrl('/products');
            
              this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
                localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))

                let marketing = localStorage.getItem('marketingdata')
                if (marketing) {
                  this.dropdownmarketing = JSON.parse(atob(marketing));
                }
                 setTimeout(() => {
              if (checkTrn.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
                const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
                modalRef.componentInstance.loginType = 'AfterLogin';
              }
             }, 3000);
              })
              
          } else {
            this.isloading = false
          }
        }).catch(() => {
          this.isloading = false
        })
      } else {
        setTimeout(() => {
          this.isloading = false;
        }, 200);
        this.router.navigateByUrl('/products');
        this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
          localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))

          let marketing = localStorage.getItem('marketingdata')
          if (marketing) {
            this.dropdownmarketing = JSON.parse(atob(marketing));
          }
        setTimeout(() => {
          if (checkTrn.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
            const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
            modalRef.componentInstance.loginType = 'AfterLogin';
          }
         }, 3000);
        })
      }
    }
  }




  getCountryCodes() {
    this.http.get('../../shared/api/country-code.json').subscribe(data => {
      console.log(data);
    });
    // return this.http.get('../../shared/api/country-code.json');
  }
  getForm() {
    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      lastName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      countryCode: ['+1'],
      userPhone: ['', [Validators.required, Validators.minLength(7)]],
      userEmail: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      occupation: ['', [Validators.required]],
      // employerData: ['', [Validators.required]],
      // businessName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      dateOfBirth: ['', [Validators.required]],
      trnNo: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(11)]],
      newPassword: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{5,}'), Validators.minLength(6), Validators.maxLength(12)]],
      // confirmPassword: ['', [Validators.required, this.passwordMatcher.bind(this)]]
      confirmPassword: ['', [Validators.required]],
      token:['',[Validators.required]]
    }, { validators: this.checkPasswords });
    this.registerForm.controls.occupation.valueChanges.subscribe(data => {
      // console.log(data)
      if (data == 1) {
        this.registerForm.addControl('employerData', new FormControl('', [Validators.required]))
        this.occupationType = 1;
        if (this.registerForm.controls.businessName)
          this.registerForm.removeControl('businessName')
        if(this.tnrerror)
          this.registerForm.controls['trnNo'].setErrors({ 'incorrect': true });  
      } else {
        this.registerForm.addControl('businessName', new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]))
        this.occupationType = 2;
        if (this.registerForm.controls.employerData)
          this.registerForm.removeControl('employerData')
        if(this.tnrerror)
          this.registerForm.controls['trnNo'].setErrors({ 'incorrect': true });  
      }
    })
  }
  checkPasswords: ValidatorFn = (group): ValidationErrors | null => {
    let pass = '', confirmPass = '';
    if (this.registerForm) {
      pass = this.registerForm.controls['newPassword'].value;
      confirmPass = this.registerForm.controls['confirmPassword'].value
    }
    if (confirmPass.length > 0)
      return pass === confirmPass ? null : { notSame: true }
    else return null
  }

  changeTnr(e?,type?) {
    if (!this.registerForm.value.trnNo || this.registerForm.value.trnNo == '')
      return;
    if (e && type == 0 && (e.target.value.length > 10)) 
       {
         return;
       }
    if (e) this.trnnum = e.target.value;
    this.trnloader = true;
    const formData = new FormData()
    formData.append('trn', this.trnnum);
    let datOfBirth = this.datepipe.transform(this.registerForm.value.dateOfBirth, 'yyyy-MM-dd')
    formData.append('dob', datOfBirth + '');
    this.http.post(this.api.TNRVALID, formData).subscribe(data => {
      console.log(data)
      if(data && data['data'])
      {
        if(this.EncrDecr.getCryptLibDecoding(data['data'].TRNValidatedEnc)=="Y"){
           this.registerForm.controls['trnNo'].setErrors(null);
        }
         else {
          this.registerForm.controls['trnNo'].setErrors({ 'incorrect': true });
          this.tnrerror = "Verify that your TRN and DOB are correct"
         } 
      } else {
        this.registerForm.controls['trnNo'].setErrors({ 'incorrect': true });
        this.tnrerror = "Verify that your TRN and DOB are correct"
       } 

      this.trnloader = false
    }, error => {
      this.registerForm.controls['trnNo'].setErrors({ 'incorrect': true });
      console.log(error)
      if (error.errorResponse.data) {
        if (typeof error.errorResponse.error == 'string')
          this.tnrerror = error.errorResponse.error
        else if (typeof error.errorResponse.error == 'object') {
          let errorMessage = ''
          Object.keys(error.errorResponse.error).map(function (key, index) {
            errorMessage = errorMessage + error.errorResponse.error[key];
          });
          if (Object.keys(error.errorResponse.error) && Object.keys(error.errorResponse.error).length > 0) {
            this.tnrerror = errorMessage;
          } else {
            this.tnrerror = "Verify that your TRN and DOB are correct"
          }
        }
      }
      this.trnloader = false
    })
    console.log(this.registerForm.value.trnNo)
  }
  changepass() {
    this.passwordMatcher(this.signupcontrols.confirmPassword as FormControl)
  }
  // changeInput(event, controlname) {
  //   var charCode = (event.which) ? event.which : event.keyCode;
  //   // Only Numbers 0-9
  //   if (charCode == 46 || charCode == 8) {
  //     this.fPOtpForm.controls[controlname].reset()
  //     event.currentTarget.previousElementSibling?.focus()
  //     return true;
  //   } else
  //     if ((charCode < 48 || charCode > 57)) {
  //       event.preventDefault();
  //       return false;
  //     } else {
  //       this.fPOtpForm.controls[controlname].setValue(event.key)
  //       event.currentTarget.nextElementSibling?.focus()
  //       return true;
  //     }
  // }
  get signupcontrols() {
    return this.registerForm.controls
  }
  showResponse(e){
    this.registerForm.controls.token.setValue(e?.response)
   console.log(e?.response)
  }
  loginWithGoogle(): void {
    localStorage.setItem('socialLogin', '1')
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  getSocialStatus(key) {
    console.log("this.socialstatus",key)
    let dropdownlist:any=[]
    let filterdata:any = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value'].filter(item => item['name'] == "SocialMedia")
      if(dropdownlist.length>0){
        console.log("this.socialstatus",dropdownlist[0].status)
        this.socialstatus=dropdownlist[0].status
      }
    }

  }
  changeKeyupTnr(e?){
    if(e.target.value.length==11){
      
      this.changeTnr(e,1)
      console.log("==",e.target.value)
    }
     
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdownmarketing.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
       if(dropdownlist.length>0){
         dropdownlist = dropdownlist.filter((item)=>item['ViewType'] == 'AfterLogin')
       }
    }
    return dropdownlist
  }
  loginWithFacebook(): void {
    localStorage.setItem('socialLogin', '1')
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  showPassword() {
    this.showPword = true;
  }
  hidePassword() {
    this.showPword = false;
  }
  showCPassword() {
    this.showCPword = true;
  }
  hideCPassword() {
    this.showCPword = false;
  }

  register(captcha) {
    // debugger;
    // let testphoneNo = this.registerForm.value.countryCode != '' ? this.registerForm.value.countryCode : ('+1' + '' + this.registerForm.value.userPhone);
    // console.log('ccode_no', this.registerForm.value.countryCode);
    // console.log('Mobile_no', testphoneNo);

    // debugger;
    // console.log(this.registerForm.value.employerData.id)

    this.isSumitted = true
    if (this.registerForm.invalid) {
      //captcha?.reset()
     // this.registerForm.controls.token.setValue('')
      return
    }

    this.isClickedOnce = false;

    this.loading = true;

    // debugger;
    // console.log('Occupation_:', this.registerForm.value.occupation)
    // console.log('Employer_:', this.registerForm.value.employerData)
    if (this.registerForm.value.newPassword === this.registerForm.value.confirmPassword) {
      this.encPassword = this.EncrDecr.set(this.registerForm.value.confirmPassword);
      this.password = this.EncrDecr.getEncoding(this.encPassword)
      // console.log("encPwd_:", this.password);
    } else {
      this.toastr.error("Password and confirm password did not match!")
    }
    // let userName = this.registerForm.value.firstName + '' + this.registerForm.value.lastName;

    const formData = new FormData();
    formData.append("FirstName", this.registerForm.value.firstName);
    formData.append("LastName", this.registerForm.value.lastName);
    formData.append("email", this.registerForm.value.userEmail);
    let phoneNo = this.registerForm.value.countryCode != '' ? this.registerForm.value.countryCode + this.registerForm.value.userPhone : ('+1' + this.registerForm.value.userPhone);
    formData.append("mobile", phoneNo);
    formData.append("occupation", this.registerForm.value.occupation);
    if (this.occupationType == 1)
      // formData.append("employer", this.registerForm.value.employerData.id);
      // formData.append("employer", this.registerForm.value.employerData);
      formData.append("employer", this.registerForm.value.employerData.name ? this.registerForm.value.employerData.name : this.registerForm.value.employerData);
    else
      formData.append("business_name", this.registerForm.value.businessName);
    formData.append("password", this.password);
    // formData.append("dob", this.registerForm.value.dateOfBirth);    
    let datOfBirth = this.datepipe.transform(this.registerForm.value.dateOfBirth, 'yyyy-MM-dd')
    console.log('DOB', datOfBirth);
    formData.append('dob', datOfBirth + '');
    formData.append("trn_number", this.trnnum);
    formData.append("grecaptcharesponse", this.registerForm.value.token);
    this.http.post(this.api.REGISTER, formData).subscribe(
      (res: any) => {
        console.log("Status:", res);
        this.loading = false;
        this.isSumitted = false;
        this.isClickedOnce = true;
         this.registerForm.controls.token.setValue('')
         captcha?.reset()
        const modalRef = this.modalService.open(VerifySignUpComponent);
        modalRef.componentInstance.verifyemail = this.registerForm.value.userEmail;
        modalRef.componentInstance.verifymode = '1';
        // this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
        //   this.closeModal = `Closed with: ${res}`;
        // }, (res) => {
        //   this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        // });

      },
      (error: any) => {
        this.loading = false;
        this.isSumitted = false;
        this.isClickedOnce = true;
        console.log("Error", error);
        this.registerForm.controls.token.setValue('')
        captcha?.reset()
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  private passwordMatcher(control: FormControl) {
    if (
      this.registerForm &&
      (control.value !== this.registerForm.controls.newPassword.value)
    ) {
      return { passwordNotMatch: true };
    }
    return null;
  }

  keyPressNumbersWithDecimal(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  keyPressAlphanumeric(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  forTrn(event) {
    console.log(event.target.value.length==11,event.target.value)
    if(event.target.value.length==11){
      
      this.changeTnr(event,1)
    }
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
  keyPressEmail(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z0-9@._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlpha(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z'\s._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaEmployer(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9\s'&-./()]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  login() {
    this.router.navigateByUrl('/login');
  }
  // maxLength:number;
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  phoneForm = new FormGroup({
    phone: new FormControl(undefined, [Validators.required])
  });

  changePreferredCountries() {
    this.preferredCountries = [CountryISO.India, CountryISO.Canada];
  }

  selectEvent(item: any) {
    // alert(item)
    console.log('Employers', item.id);

    // do something with selected item
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e: any) {
    // do something
  }
  onDateChange(newDate: Date) {
    console.log(newDate);
  }
  customFilter = function(allEmployers: any[], query: string): any[] {
    return allEmployers.filter(x => x.name.toLowerCase().startsWith(query.toLowerCase()));
  };
}


