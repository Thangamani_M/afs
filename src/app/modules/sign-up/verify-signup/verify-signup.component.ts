import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { NgxOtpInputComponent, NgxOtpInputConfig } from "ngx-otp-input";
import { HttpClient } from '@angular/common/http';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { MarketinginfoComponent } from '../../login/marketingInfo/marketinginfo.component';
import { UtilityService } from 'src/app/services/common/utility.service';

@Component({
  selector: 'app-verify-signup',
  templateUrl: './verify-signup.component.html',
  styleUrls: ['./verify-signup.component.css']
})
export class VerifySignUpComponent implements OnInit {

  otpVerify: boolean = true;
  fpOtpSuccess: boolean = false;
  closeModal: string;
  resend: boolean = false;
  intervalId = 0;
  message = '';
  seconds = 90;
  loading: boolean = false;
  popupLoading: boolean = false;
  isSumitted = false
  isloading = false
  enterOTP: boolean = false;
  dropdownmarketing = []
  otpInputConfig: NgxOtpInputConfig = {
    otpLength: 6,
    autofocus: true,
    classList: {
      inputBox: "my-super-box-class",
      input: "my-super-class",
      inputFilled: "my-super-filled-class",
      inputDisabled: "my-super-disable-class",
      inputSuccess: "my-super-success-class",
      inputError: "my-super-error-class"
    }
  };
@ViewChild('ngotp') ngotp:NgxOtpInputComponent  


  isClickedOnce = true;
  otpNotValied = false;
  otpValue: any;
 @Input() verifyemail=''
 @Input() verifytype='email'
 @Input() verifymode='0'
 @Input() loginType=""
  constructor(private router: Router, private http: HttpClient, private modalService: NgbModal,private EncrDecr: EncrDecrService, private api: ApiEndpointsService, private toastr: ToastrService, private util: UtilityService) { }


  ngOnInit(): void {
    if(this.verifymode=='1')
       this.startTimer();
      else
        this.getVerify()
  }



  handleFillEvent(value: any): void {
    // console.log(value);
    this.otpValue = value;
    this.otpNotValied = false;
  }
  submitFPOtp() {
    // debugger;
    this.otpNotValied = false;
    this.fpOtpSuccess = false;
    if (this.otpValue == null || this.otpValue == '') {
      this.otpNotValied = false;
      this.enterOTP = true;
    }
    else if (this.otpValue && this.otpValue.length == 6) {
      this.otpNotValied = false;
      this.popupLoading=true
      const formData = new FormData();
      if(this.verifytype=='email'|| this.verifymode=='1')
         formData.append("email", this.verifyemail);
      else
         formData.append('mobile',this.verifyemail)   
      formData.append("otp", this.otpValue);

      this.http.post(`${this.verifymode=='1'? this.api.VERYFYOTP:(this.verifytype=='email'?this.api.FORGOTVERIFYOTP:this.api.VERIFYMOBILEOTP)}`, formData,{ observe: 'response' as 'body' }).subscribe(
        (res: any) => {
          console.log(res)
          
          if (this.EncrDecr.getCryptLibDecoding(res.body.data.verified_enc) == "allow") {
            this.enterOTP = false;
              // this.fPOtpForm.reset();
              if(this.loginType=='AfterLogin'){
                this.util.signIn(res, 1);
                this.modalService.dismissAll();
                let modifiedUser;
                let userid = localStorage.getItem('user')||'';
                if(this.verifytype=='email'){
                localStorage.setItem('EmailVerification', this.EncrDecr.setCryptLibEncode(JSON.parse(userid).userId+"_allow"));
                 modifiedUser =Object.assign(JSON.parse(localStorage.getItem("userDetails")!!),{email_status:this.EncrDecr.setCryptLibEncode(JSON.parse(userid).userId+"_allow")})
                } else {
                localStorage.setItem('PhoneVerification', this.EncrDecr.setCryptLibEncode(JSON.parse(userid).userId+"_allow"));
                modifiedUser =Object.assign(JSON.parse(localStorage.getItem("userDetails")!!),{otp_status:this.EncrDecr.setCryptLibEncode(JSON.parse(userid).userId+"_allow")})
                } 
                
                localStorage.setItem("userDetails",JSON.stringify(modifiedUser))
                let user:any = JSON.parse(localStorage.getItem("userDetails")!!)
                localStorage.setItem("logintime",JSON.stringify({date:new Date()}))
                if (user.LoanCount != 0){
                  this.router.navigateByUrl('/my-loans');
                }
                else{
                  this.http.post(this.api.MARKETINGDATA, {}).subscribe(marketdata=>{
                    localStorage.setItem('marketingdata', btoa(JSON.stringify(marketdata['data'])))
                    let marketing = localStorage.getItem('marketingdata')
                    if (marketing) {
                      this.dropdownmarketing = JSON.parse(atob(marketing));
                    }
                    this.popupLoading=false
                    this.router.navigateByUrl('/products');
                    if(res.body.data.trn_number && res.body.data.DateOfBirth){
                    if (user.show_marketing_info && this.getdropdownList('QuestionName').length>0) {
                      setTimeout(() => {
                        const modalRef = this.modalService.open(MarketinginfoComponent, { size: 'lg' })
                        modalRef.componentInstance.loginType = 'AfterLogin';
                       }, 3000);
                    }
                  } 
                  },err=>{
                    this.popupLoading=false
                  })
                }

                this.deviceUpdate(res.body.data.user.UserId)
              }else {
                this.popupLoading=false
                this.otpVerify = false;
                this.fpOtpSuccess = true;
                setTimeout(() => {
                  this.modalService.dismissAll();
              this.router.navigateByUrl('/login');
            }, 2000);
              }
          

          } else {
            this.popupLoading=false
            this.fpOtpSuccess = false;
            // this.fPOtpForm.reset();
            this.enterOTP = false;
            this.ngotp.clear()
            this.otpNotValied = true;
          }

        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          // this.enterOTP = false;
          this.popupLoading=false
          this.isSumitted = false;
        }
      );
    }
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdownmarketing.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
       if(dropdownlist.length>0){
         dropdownlist = dropdownlist.filter((item)=>item['ViewType'] == 'AfterLogin')
       }
    }
    return dropdownlist
  }
  deviceUpdate(uid) {
    let token = localStorage.getItem('device_token')
    if (token) {
      const formData = new FormData();
      formData.append("device_version", '1');
      formData.append("user_id", uid);
      formData.append("device_token", token);
      formData.append("device_type", '3');
      this.http.post(this.api.DEVICEUPDATE, formData).subscribe(masterdata => {
        console.log(masterdata)
      }, error => {
        console.log(error)
      })
    }
  }
  getVerify() {
    const formData = new FormData();
    if (this.verifytype == 'email' ) {
      formData.append("email", this.verifyemail);
    }
    else {
      localStorage.setItem('phonerefresh','true')
      formData.append('mobile', this.verifyemail)
    }
    this.popupLoading=true
      this.http.post(`${(this.verifytype=='email')?this.api.FORGOTPASSWORD:this.api.GETMOBILEOTP}`, formData).subscribe(
        (res: any) => {
          this.startTimer();
          console.log(res)
          this.isClickedOnce = true;
          this.popupLoading=false
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          this.resend = false;
          this.message = ''
          
        },
        (error: any) => {
          console.log("Error", error);
          this.popupLoading=false
          this.isClickedOnce = true;
        }
      );
    }
  
    



  resendOTP() {
    this.isClickedOnce = false;

    const formData = new FormData();
    if (this.verifytype == 'email' || this.verifymode == '1') {
      formData.append("email", this.verifyemail);
      if(this.verifymode == '1')
         formData.append("mode", 'Register');
      else   
         formData.append("mode", 'ChangePassword');
    }
    else {
      formData.append('mobile', this.verifyemail)
    }
    this.popupLoading=true
    this.http.post(`${(this.verifytype=='email'|| this.verifymode=='1')?this.api.RESENDOTP:this.api.GETMOBILEOTP}`, formData, { observe: 'response' as 'body' }).subscribe(
      (res: any) => {
        console.log(res)
        this.isClickedOnce = true;
        this.popupLoading=false
        // if(this.verifymode=='1')
          this.toastr.success(res.body.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        //  else
        //   this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true }); 
        this.resend = false;
        this.message = ''
        this.startTimer();

      },
      (error: any) => {
        console.log("Error", error);
        this.isClickedOnce = true;
        this.popupLoading=false
      }
    );
  }

  clearTimer() {
    clearInterval(this.intervalId);
  }

  stop() {
    this.clearTimer();
    this.message = `${this.seconds}`;
  }

  startTimer() {
    // debugger;
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.stop();
        this.resend = true;
      } else {
        if (this.seconds < 0) {
          this.resend = false;
          this.seconds = 90;
        } // reset
        this.message = `${this.seconds}`;
      }
    }, 1000);
  }
  
  login() {
    this.router.navigateByUrl('/login');
  }
  closepopup(){
    this.modalService.dismissAll()
  }
}


