import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ToastrService } from 'ngx-toastr';
import { ModalDismissReasons, NgbInputDatepicker, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncrDecrService } from 'src/app/services/common/enc-dec.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { UtilityService } from 'src/app/services/common/utility.service';
import { NgxOtpInputComponent, NgxOtpInputConfig } from "ngx-otp-input";
import { CommonService } from 'src/app/services/common/common.service';
import { ViewCompiler } from '@angular/compiler';
import { AuthLayoutComponent } from 'src/app/shared/layout/auth-layout/auth-layout.component';
import { FileTypeValidator } from 'src/app/services/common/filetype-validation';
// import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
// import { Observable, Subject } from 'rxjs';
// import cryptLib  from '@skavinvarnan/cryptlib';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loading: boolean = false;
  profileloader=false
  isSumitted = false;
  isClickedOnce = true;
  showCPword: boolean = false;
  showNPword: boolean = false;
  showVPword: boolean = false;
  password: string;
  employerData: any;
  isEmailVerified: boolean = true;
  isMobileVerified: boolean = true;

  popupLoading: boolean = false;
  resend: boolean = false;
  intervalId = 0;
  message = '';
  seconds = 90;
  closeModal: string;
  userInfo: any;
  imageloader = false
  imagePath: any = 'assets/img/default-profile.png';
  userFirstName: any;
  userinfoEmail: any;
  imageId: any;
  otpValue: any;
  otpNotValied = false;
  enterOTP: boolean = false;
  fpOtpSuccess: boolean = false;
  otpVerify: boolean = true;
  showPword: boolean = false;
  enterPassword = false;
  occupationType = 0;
  empVerify: boolean = true;
  isEmpVerified: boolean = false;
  fieldname: any = [];
  imageJson: any = [{ id: '', url: '' }];
  fileValidation: boolean = false;
  maxDate: Date;
  year_: string;
  allContries: any = [];
  @ViewChild('datePickerInput') datePicker: NgbInputDatepicker;


  public profileForm = new FormGroup({
    firstName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
    // middleName: new FormControl('', [Validators.minLength(1), Validators.maxLength(20)]),
    middleName: new FormControl(''),
    lastName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
    userPhone: new FormControl('', [Validators.required, Validators.minLength(7)]),
    countryCode:new FormControl('+1'),
    userEmail: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    dateOfBirth: new FormControl('', [Validators.required]),
    occupation: new FormControl('', [Validators.required]),
    employerData: new FormControl('', [Validators.required]),
    trnNumber: new FormControl('', [Validators.required]),
    // confirmPassword: new FormControl('', [Validators.required, this.passwordMatcher.bind(this)])
  })
  public paySlipForm = new FormGroup({
    employerData: new FormControl('', [Validators.required]),
    attachments: new FormControl('', [Validators.required, this.imagvalidator.validate]),
  })

  public changePasswordForm: FormGroup

  otpInputConfig: NgxOtpInputConfig = {
    otpLength: 6,
    autofocus: true,
    classList: {
      inputBox: "my-super-box-class",
      input: "my-super-class",
      inputFilled: "my-super-filled-class",
      inputDisabled: "my-super-disable-class",
      inputSuccess: "my-super-success-class",
      inputError: "my-super-error-class"
    }
  };
  forOtpData: any;
  encPassword: string;
  encEmail: string;
  pwdValidate: any;
  encoldPassword: string;
  encNewPassword: string;
  enConPassword: string;
  proUpdate: boolean = true;
  proUpdateSuccess: boolean = false;
  allEmployers = [];
  keyword = 'name';

  @ViewChild('modalOTPData') modalOTPData: ElementRef;
  @ViewChild('modalValidatePwd') modalValidatePwd: ElementRef;
  @Output() onButtonClick: EventEmitter<string> = new EventEmitter();
  submitForm: boolean = false;
  socialloading: boolean = false;
  paySlipUpload: boolean = false;
  payslipId: any;
  payslipImagePath: any;
  defaultEmployer = '';
  defaultBusiness = '';
  firstTimeEmployer = true;
  // @ViewCompiler(AuthLayoutComponent)
  // public authLayout: AuthLayoutComponent = new AuthLayoutComponent();

  constructor(private imagvalidator: FileTypeValidator, private router: Router, private commonData: CommonService, private toastr: ToastrService, private socialAuthService: SocialAuthService, private modalService: NgbModal, private EncrDecr: EncrDecrService, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService, private util: UtilityService) { }


  ngOnInit(): void {
    // this.datePicker.close();
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 10)
    this.getCurrentAgeYear18();
    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(11)]),
      newPassword: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{5,}'), Validators.minLength(6), Validators.maxLength(11)]),
      confirmPassword: new FormControl('', [Validators.required])
    }, { validators: this.checkPasswords })

    if (this.imagePath == undefined) {
      this.imagePath = false;
      // this.updatedImage = false;
    }
    this.http.get(this.api.COUNTRYDATAS).subscribe(data => {
      console.log('C:', data)
      this.allContries = data;
    });
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
      console.log(masterdata);
      // this.employerData = masterdata['data'].;  
      masterdata['data'].forEach(element => {
        if (element.key == "Employer") {
          this.employerData = element.value;
          this.allEmployers = this.employerData;
          console.log('Employers', this.allEmployers)
        }
      });
    })

    // modalOTPData
    this.profileForm.controls.occupation.valueChanges.subscribe(data => {
      // debugger
      // console.log(data)
      if (data == 1) {
        this.profileForm.addControl('employerData', new FormControl(this.defaultEmployer, [Validators.required]))
        this.profileForm.controls.employerData.valueChanges.subscribe(emp => {
          this.defaultEmployer = emp;
        })
        this.occupationType = 1;
        if (!this.firstTimeEmployer && this.defaultEmployer == '') {
          this.getVerify(this.modalOTPData, 'employer');
        }
        this.firstTimeEmployer = false
        if (this.profileForm.controls.businessName)
          this.profileForm.removeControl('businessName')
      } else {
        this.profileForm.addControl('businessName', new FormControl(this.defaultBusiness, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]))
        this.profileForm.controls.businessName.valueChanges.subscribe(emp => {
          this.defaultBusiness = emp;
        })
        this.occupationType = 2;
        this.firstTimeEmployer = false
        if (this.profileForm.controls.employerData)
          this.profileForm.removeControl('employerData')
      }
    })

    // For User Details
    let getUser = localStorage.getItem('userDetails');
    if (getUser) {
      this.userInfo = JSON.parse(getUser)
    }

   this.getProfile()
  }


getProfile(){
  this.profileloader=true
  this.http.get(this.api.MYPROFILE).subscribe(
    (res: any) => {
      console.log("MY PROFILE",res.data)
        if(res.data){
          this.userInfo=res.data
          const imageFormData = new FormData();
          if(this.userInfo.profile_url){
          imageFormData.append("id", this.userInfo.profile_url);
          this.imageloader = true
          this.http.post(this.api.IMAGEUPLOAD, imageFormData).subscribe(
            (res: any) => {
              this.imagePath = res.data.path;
              this.imageloader = false
            },
            (error: any) => {
              this.imageloader = false
            }
          );
          }
          this.setData()

          this.profileloader=false
        }
    },
    (error: any) => {
      this.profileloader=false
      console.log(error)
    }
  );
}


setData(){
  console.log("this.userInfo",this.userInfo)
  if (this.EncrDecr.getCryptLibDecoding(this.userInfo.email_status) == "allow",'1') {
    this.empVerify = false;
    this.isEmpVerified = true;
  } else {
    this.empVerify = true;
    this.isEmpVerified = false;
  }
  this.userFirstName = this.userInfo.FirstName + ' ' + this.userInfo.LastName;
  this.userinfoEmail = this.userInfo.Email;
  this.profileForm.controls["firstName"].setValue(this.userInfo.FirstName);
  this.profileForm.controls["lastName"].setValue(this.userInfo.LastName);
  this.profileForm.controls["userPhone"].setValue(this.userInfo.mobile.replace("+1",''));
  this.profileForm.controls["userEmail"].setValue(this.userInfo.Email);
  this.profileForm.controls["dateOfBirth"].setValue(this.userInfo.DateOfBirth);
  this.profileForm.controls["occupation"].setValue(this.userInfo.occupation);
  if (this.occupationType == 1)
    this.profileForm.controls["employerData"].setValue(this.userInfo.employer);
  else
    this.profileForm.controls["businessName"].setValue(this.userInfo.business_name);
  this.profileForm.controls["trnNumber"].setValue(this.userInfo.trn_number);
  // this.profileForm.controls["confirmPassword"].setValue(this.userInfo.lastName);

  // For Image Url
  if(this.userInfo.profile_url){
  const imageFormData = new FormData();
  imageFormData.append("id", this.userInfo.profile_url);
  this.imageloader = true
  this.http.post(this.api.IMAGEUPLOAD, imageFormData).subscribe(
    (res: any) => {
      console.log(res)
      this.imageloader = false
      this.imagePath = res.data.path;
      console.log('Image Path:', this.imagePath)
    },
    (error: any) => {
      this.imageloader = false
      console.log("Error", error);
    }
  );
  }


//   const plainText = "this is my plain text";
// const key = "your key";

// // const cryptLib = require('@skavinvarnan/cryptlib');

// const cipherText = cryptLib.encryptPlainTextWithRandomIV(plainText, key);
// console.log('cipherText %s', cipherText);

// const decryptedString = cryptLib.decryptCipherTextWithRandomIV(cipherText, key);
// console.log('decryptedString %s', decryptedString);

  // let jsondata =this.EncrDecr.getDecoding("eyJpdiI6IlFrWndhMDlJVFZvNGJqQnJaMk5qV1E9PSIsInZhbHVlIjoiTE9qbXZDVHlKaXNUZGN0STRQYlNxYm9tMUt1ZGdodjJrODBuZEhNMGgzZz0iLCJtYWMiOiI3NzU5MzAyZWI0YzVlYTFkZWQ0MDBhNjc2OThiMjNmM2Y1MDJhMzBlNWMwZDIzMzNlNjUxYmU5YzNkMmE2MGQzIn0=")
  // console.log("DEcreption!==",jsondata,"TVVSkORQ7kG1Q6LrB+kEOwXZWz7PTMbTivO7bTjFXm0=")
  console.log("DEcreption==",this.EncrDecr.getCryptLibDecoding(this.userInfo.email_status,'1'))
  // if (!this.userInfo.email_verified || this.userInfo.email_verified == "0") {
  //   this.isEmailVerified = false;
  // }
  // if (!this.userInfo.otp_verified || this.userInfo.otp_verified == "0") {
  //   this.isMobileVerified = false;
  // }

  if (this.EncrDecr.getCryptLibDecoding(this.userInfo.email_status,'1') != "allow") {
    this.isEmailVerified = false;
  }
  if (this.EncrDecr.getCryptLibDecoding(this.userInfo.otp_status,'1') != "allow") {
    this.isMobileVerified = false;
  }
}



  submitPayslip() {
    // debugger;
    this.isSumitted = true
    if (this.paySlipForm.invalid) {
      return
    }
    
    this.isSumitted = false
    this.isClickedOnce = false;
    const formData = new FormData();
    let employer = this.paySlipForm.value.employerData.name ? this.paySlipForm.value.employerData.name : this.paySlipForm.value.employerData
    formData.append("employer", employer);
    formData.append("documents", this.payslipId ? this.payslipId : '');

    this.http.post(this.api.USERUPDATEEMPLOYER, formData).subscribe(
      (res: any) => {
        console.log("Status:", res);
       // this.getProfile()
       this.isEmpVerified =false
       this.empVerify =true
        this.profileForm.controls.employerData.setValue(employer);
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
        this.paySlipForm.reset();
        this.isSumitted = false;
        setTimeout(() => {
          this.modalService.dismissAll();
        }, 1000);
        if(this.userInfo.passwordflag !='N'){
        setTimeout(() => {
          this.modalService.dismissAll();
        this.updateProfile(this.modalValidatePwd)
        }, 1000);
      } else{
        this.submitSocial()
      }
      },
      (error: any) => {
        console.log("Error", error);
        // this.toastr.error(error)
        // this.closeModal = `Dismissed ${this.getDismissReason(error)}`;
      }
    );

  }
  showPassword() {
    this.showPword = true;
  }
  hidePassword() {
    this.showPword = false;
  }
  showNPassword() {
    this.showNPword = true;
  }
  hideNPassword() {
    this.showNPword = false;
  }
  showCPassword() {
    this.showCPword = true;
  }

  hideCPassword() {
    this.showCPword = false;
  }
  showVPassword() {
    this.showVPword = true;
  }
  hideVPassword() {
    this.showVPword = false;
  }
  get updatecontrols() {
    return this.profileForm.controls
  }

  get payslipformcontrols() {
    return this.paySlipForm.controls
  }

  get changepasswordcontrols() {
    return this.changePasswordForm.controls
  }

  // Image Upload
  fileChange(event: any) {
    // debugger;
    this.loading = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if (this.checkExtension(file['name'])) {
        this.toastr.error('The image must be a file of type: jpg, jpeg, png', '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        return
      }

      const max_size = 25097152;
      console.log(event.target.files[0].size, (event.target.files[0].size / (1024 * 1024)).toFixed(2))
      if (event.target.files[0].size / (1024 * 1024) > max_size / (1024 * 1024)) {
        console.log(max_size / (1024 * 1024) + 'Mb')
        this.toastr.error('Please upload the file size below 25 MB', '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        return
      }

      const formData = new FormData();
      this.imageloader = true

      formData.append("image", file, file['name']);

      this.apihttp.post(this.api.UPLOADFILE, formData).subscribe(
        (res) => {
          this.loading = false;
          this.imageloader = false
          this.imageId = res['data']['file_name'];
          this.imagePath = res['data']['path'];
          console.log('ImageUrl_:', this.imagePath)
          // this.updatedImage = true;
          // this.toastr.success(res.message);
        },
        (err) => {
          console.log(err);
          this.imageloader = false
          this.loading = false;
          this.imageId = '';
          this.imagePath = 'assets/img/default-profile.png';
        }
      );
    }

  }

  fileChangePayslip(event: any, FormControl) {
    // debugger;
    this.fileValidation = false;

    console.log("FormControl", FormControl)
    // debugger;

    if (event.target.files.length > 0) {
      const formData = new FormData();

      const file = event.target.files[0];
      console.log("Value", this.payslipformcontrols.controls)
      this.payslipformcontrols[FormControl].setValue(file['name'])
      this.payslipformcontrols[FormControl].markAsDirty()
      if (!this.imagvalidator.validate(this.payslipformcontrols[FormControl] as FormControl)) {
        const max_size = 25097152;
        console.log(event.target.files[0].size, (event.target.files[0].size / (1024 * 1024)).toFixed(2))
        if (event.target.files[0].size / (1024 * 1024) > max_size / (1024 * 1024)) {
          console.log(max_size / (1024 * 1024) + 'Mb')
          this.payslipformcontrols[FormControl].setErrors({ 'filleSizeincorrect': true });
          return
        } else {
          this.payslipformcontrols[FormControl].setErrors(null);
        }

        formData.append("image", file, file['name']);

        this.fieldname.push(FormControl)

        this.http.post(this.api.UPLOADFILE, formData).subscribe(
          (res) => {
            this.fileValidation = false;
            this.payslipformcontrols[FormControl].setValue(res['data']['file_name']);
            this.imageJson.push({ id: res['data']['file_name'], url: res['data']['path'] })
            const index = this.fieldname.indexOf(FormControl, 0);
            this.payslipId = res['data']['file_name'];
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }

          },
          (err) => {
            console.log(err);
            this.fileValidation = true;
            const index = this.fieldname.indexOf(FormControl, 0);
            if (index > -1) {
              this.fieldname.splice(index, 1);
            }
            this.payslipformcontrols[FormControl].setValue('');
            this.payslipId ='';
          }
        );
      }
    }
  }

  geturl(id) {
    let url = this.imageJson.filter(item => item.id == id)
    if (url.length > 0)
      return url[0].url
    return false
  }

  private checkExtension(c) {
    let valToLower = c.toLowerCase();
    if (valToLower.split('.').length > 1) {

      let regex = new RegExp("(.*?)\.(jpg|png|jpeg)$"); //add or remove required extensions here

      let regexTest = regex.test(valToLower);
      console.log(valToLower, regexTest)
      return !regexTest ? true : false;
    }
    return true;
  }
  submitPassword() {

    this.enterPassword = false;
    if (this.pwdValidate == undefined || this.pwdValidate == '') {
      this.enterPassword = true;
    } else {
      this.enterPassword = false;

      this.isClickedOnce = false;
      this.loading = true;


      this.encEmail = this.EncrDecr.set(this.profileForm.value.userEmail);
      let email = this.EncrDecr.getEncoding(this.encEmail)

      this.encPassword = this.EncrDecr.set(this.pwdValidate);
      let password = this.EncrDecr.getEncoding(this.encPassword)

      const formData = new FormData();
      formData.append("FirstName", this.profileForm.value.firstName);
      formData.append("LastName", this.profileForm.value.lastName);
      formData.append("email", this.profileForm.value.userEmail);
      formData.append("mobile", this.profileForm.value.countryCode+ this.profileForm.value.userPhone);
      formData.append("DateOfBirth", this.profileForm.value.dateOfBirth);
      formData.append("occupation", this.profileForm.value.occupation);
      if (this.occupationType == 1)
        formData.append("employer", this.profileForm.value.employerData.name ? this.profileForm.value.employerData.name : this.profileForm.value.employerData);
      // formData.append("employer", this.profileForm.value.employerData);
      else
        formData.append("business_name", this.profileForm.value.businessName);
      // formData.append("password", password);
      formData.append("password", this.pwdValidate);
      // formData.append("UserPassword", this.pwdValidate);
      formData.append("trn_number", this.profileForm.value.trnNumber);
      formData.append("profile_url", this.imageId ? this.imageId : '');
   let self =this
      this.http.post(this.api.UPDATEPROFILE, formData).subscribe(
        (res: any) => {
          // debugger;
       
          let getUser = localStorage.getItem('userDetails');
          if (getUser) {
           let userIn = JSON.parse(getUser)
             if(userIn.Email !=this.profileForm.value.userEmail || userIn.mobile!=this.profileForm.value.countryCode+ this.profileForm.value.userPhone)
                {
                  setTimeout(() => {
                  self.logout()
                },2500)
                } else {
                  if (this.EncrDecr.getCryptLibDecoding(res.data.employer_verified_status) == "allow",'1') {
                    this.empVerify = false;
                    this.isEmpVerified = true;
                  }
                  this.getProfile()  
                }
          }
          console.log("Status:", res);
          this.util.updateProfile(res.data)
          this.commonData.callComponentMethod('');
          if (localStorage.getItem('selecttabs')) {
            let tab = JSON.parse(atob(localStorage.getItem('selecttabs')!!))
            if (tab.data) {
              tab.data.forEach((element, i) => {
                if (this.util.getFormArrayStorage(element.id)) {
                  this.util.removeFormArrayStorage(element.id)
                }
              });
            }
          }
          this.loading = false;
          this.isSumitted = false;
          this.isClickedOnce = true;
          this.userFirstName = res.data.FirstName + ' ' + res.data.LastName;
          this.userinfoEmail = res.data.Email;
          // if (!res.data.email_verified || res.data.email_verified == "0") {
          //   this.isEmailVerified = false;
          // }
          // if (!res.data.otp_verified || res.data.otp_verified == "0") {
          //   this.isMobileVerified = false;
          // }
          if (this.EncrDecr.getCryptLibDecoding(this.userInfo.email_status) != "allow",'1') {
            this.isEmailVerified = false;
          }
          if (this.EncrDecr.getCryptLibDecoding(this.userInfo.otp_status) != "allow",'1') {
            this.isMobileVerified = false;
          }
          this.proUpdate = false;
          this.proUpdateSuccess = true;
          if (this.occupationType == 1)
            this.defaultBusiness = '';
          else
            this.defaultEmployer = '';

          setTimeout(() => {
            this.modalService.dismissAll();
          }, 2000);
      
        },
        (error: any) => {
          this.loading = false;
          this.isSumitted = false;
          this.isClickedOnce = true;
          console.log("Error", error);
          // this.toastr.error(error)
          // this.closeModal = `Dismissed ${this.getDismissReason(error)}`;
        }
      );
    }
  }

  getCurrentAgeYear18() {
    var date = new Date();
    var intYear = date.getFullYear() - 18;
    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    this.year_ = intYear + "-" + month + "-" + day;
    // console.log(new Date(this.year_))
    this.maxDate = new Date(this.year_);
    console.log(this.maxDate)
  }

  submitSocial() {
    this.isSumitted = true;
    // if (this.profileForm.controls.) {
    //  return 
    // }
    console.log(this.profileForm.controls)
    this.isClickedOnce = false;
    this.socialloading = true;
    const formData = new FormData();

    formData.append("occupation", this.profileForm.value.occupation);
    if (this.occupationType == 1)
      // formData.append("employer", this.profileForm.value.employerData.name);
      formData.append("employer", this.profileForm.value.employerData);
    else
      formData.append("business_name", this.profileForm.value.businessName);
      formData.append("mobilenumber", this.profileForm.value.countryCode+ this.profileForm.value.userPhone);
      formData.append("FirstName", this.profileForm.value.firstName);
      formData.append("LastName", this.profileForm.value.lastName);
      formData.append("profile_url", this.imageId ? this.imageId : '');
    this.http.post(this.api.SOCIALPROFILE, formData).subscribe(
      (res: any) => {
        console.log("Status:", res);
        // this.util.updateProfile(res.data)
        let user = localStorage.getItem("userDetails");
        this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
        if (user) {
          let checkTrn = JSON.parse(user)
          let updateUser = checkTrn
          if (this.occupationType == 1)
            updateUser = Object.assign(checkTrn, { occupation: this.profileForm.value.occupation, employer: this.profileForm.value.employerData,FirstName:this.profileForm.value.firstName,LastName:this.profileForm.value.lastName,profile_url:this.imageId ? this.imageId : '' })
          else
            updateUser = Object.assign(checkTrn, { occupation: this.profileForm.value.occupation, business_name: this.profileForm.value.businessName,FirstName:this.profileForm.value.firstName,LastName:this.profileForm.value.lastName,profile_url:this.imageId ? this.imageId : '' })
          localStorage.setItem("userDetails", JSON.stringify(updateUser))
        }
        this.getProfile()
        this.commonData.callComponentMethod('');
        this.socialloading = false;
        this.isSumitted = false;
        this.isClickedOnce = true;

      },
      (error: any) => {
        this.socialloading = false;
        this.isSumitted = false;
        this.isClickedOnce = true;
        console.log("Error", error);
        // this.toastr.error(error)
        // this.closeModal = `Dismissed ${this.getDismissReason(error)}`;
      }
    );

  }

  updateProfile(content?) {

    this.isSumitted = true;
    if (this.profileForm.invalid) {
      return
    }
    this.isClickedOnce = false;
    this.proUpdate = true;
    this.proUpdateSuccess = false;
    // this.authLayout.fetchRequestsStatus();
    // this.onButtonClick.emit();
    this.pwdValidate = '';
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  goBack() {
    this.router.navigateByUrl('/products');
  }
  getVerify(content?, otptype?) {
    this.isClickedOnce = false;
    this.profileloader=true
    this.fpOtpSuccess = false;
    this.enterOTP=false
    this.forOtpData = otptype;
    if (otptype == "email") {
      const formData = new FormData();
      formData.append("email", this.profileForm.value.userEmail);

      this.http.post(this.api.FORGOTPASSWORD, formData).subscribe(
        (res: any) => {
          console.log(res)
          // this.isClickedOnce = true;
       this.profileloader=false
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          this.loading = false;
          this.paySlipUpload = false;
          this.otpVerify = true;
          // this.intervalId = 0;
          // this.seconds = 60;
          this.message = ''
          this.startTimer();
          this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
            this.closeModal = `Closed with: ${res}`;
          }, (res) => {
            this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
          });
          // this.modalService.dismissAll()
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          this.loading = false;
          this.isClickedOnce = true;
          this.profileloader=false
        }
      );
    }
    else if (otptype == "mobile") {
      // alert("2")

      const formData = new FormData();
      formData.append("mobile",this.profileForm.value.countryCode+this.profileForm.value.userPhone);

      this.http.post(this.api.GETMOBILEOTP, formData).subscribe(
        (res: any) => {
          console.log(res)
          this.isClickedOnce = true;
          this.profileloader=false
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          this.loading = false;
          this.paySlipUpload = false;
          this.otpVerify = true;
          // this.intervalId = 0;
          // this.seconds = 60;
          this.message = ''
          this.startTimer();
          this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
            this.closeModal = `Closed with: ${res}`;
          }, (res) => {
            this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
          });
          // this.modalService.dismissAll()
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          this.profileloader=false
          this.loading = false;
          this.isClickedOnce = true;
        }
      );
    }
    else if (otptype == "employer") {
      this.isSumitted = false;
      this.paySlipForm.reset();
      this.isClickedOnce = true;
      // alert("2")
      this.profileloader=false
      this.loading = false;
      this.otpVerify = false;
      this.paySlipUpload = true;
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
        this.closeModal = `Closed with: ${res}`;
      }, (res) => {
        this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  logout() {
    let token =localStorage.getItem('device_token')
    if(!token){
      this.util.logoutAndNavigate();
      return
    }
    this.http.post(`${this.api.LOGOT}`,{device_token:token}).subscribe((data)=>{
      this.util.logoutAndNavigate();
    },error=>{
      this.util.logoutAndNavigate();
    })
    
  }


  private passwordMatcher(control: FormControl) {
    if (
      this.changePasswordForm &&
      (control.value !== this.changePasswordForm.controls.newPassword.value)
    ) {
      return { passwordNotMatch: true };
    }
    return null;
  }

  keyPressNumbersWithDecimal(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  forTrn(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphanumeric(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaEmployer(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9\s'&-./()]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
  keyPressAlpha(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z'._-\s]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  // OTP Verification

  startTimer() {
    // debugger;
    this.resend = false;
    this.clearTimer();
    this.seconds = 90
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.stop();
        this.resend = true;
      } else {
        if (this.seconds < 0) {
          this.resend = false;
          this.seconds = 90;
        } // reset
        this.message = `${this.seconds}`;
      }
    }, 1000);
  }


  clearTimer() {
    clearInterval(this.intervalId);
  }

  stop() {
    this.clearTimer();
    this.message = `${this.seconds}`;
  }
  resendOTP() {
    // this.ngxotp.clear();
    this.isClickedOnce = false;
    this.popupLoading =true
    if (this.forOtpData == "email") {
      const formData = new FormData();
      formData.append("email", this.profileForm.value.userEmail);
      formData.append("mode", 'ChangePassword');

      this.http.post(this.api.RESENDOTP, formData).subscribe(
        (res: any) => {
          console.log(res)
          this.isClickedOnce = true;
          this.popupLoading =false
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          this.resend = false;
          this.message = ''
          this.startTimer();
          // this.modalService.dismissAll()
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          // this.loading = false;
          this.isClickedOnce = true;
          this.popupLoading =false
        }
      );
    } else if (this.forOtpData == "mobile") {
      const formData = new FormData();
      formData.append("mobile",this.profileForm.value.countryCode+ this.profileForm.value.userPhone);

      this.http.post(this.api.GETMOBILEOTP, formData).subscribe(
        (res: any) => {
          console.log(res)
          this.isClickedOnce = true;
          this.popupLoading =false
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          this.message = ''
          this.startTimer();
          // this.modalService.dismissAll()
        },
        (error: any) => {
          console.log("Error", error);
          // this.toastr.error(error)
          // this.loading = false;
          this.isClickedOnce = true;
          this.popupLoading =false
        }
      );
    }

  }
  handleFillEvent(value: any): void {
    console.log(value);
    this.otpValue = value;
    this.otpNotValied = false;
  }
  handeOtpChange(value: string[]): void {
    console.log(value);
  }
  submitEmailOTP() {
    this.otpNotValied = false;
    this.fpOtpSuccess = false;
    // alert(this.forOtpData)
    if (this.forOtpData == "email") {
      // debugger
      if (this.otpValue == null || this.otpValue == '') {
        this.otpNotValied = false;
        this.enterOTP = true;
      }
      else if (this.otpValue && this.otpValue.length == 6) {
        this.popupLoading =true
        this.otpNotValied = false;
        const formData = new FormData();
        formData.append("email", this.profileForm.value.userEmail);
        formData.append("otp", this.otpValue);

        this.http.post(this.api.FORGOTVERIFYOTP, formData).subscribe(
          (res: any) => {
            console.log(res)
            // this.fPOtpForm.reset();
            // debugger;
            let user = localStorage.getItem('user')||'';
            this.isSumitted = false;
            this.popupLoading =false
            if (this.EncrDecr.getCryptLibDecoding(res.data.verified_enc)=="allow") {
              this.enterOTP = false;
              this.otpVerify = false;
              this.fpOtpSuccess = true;
              this.isEmailVerified = true;
              let emailVerify = Object.assign(this.userInfo, { email_status:this.EncrDecr.setCryptLibEncode(JSON.parse(user).userId+"_allow")})
              this.util.updateProfile(emailVerify)
              //  this.commonData.callComponentMethod('');
              setTimeout(() => {
                this.modalService.dismissAll();
                // this.fPOtpForm.reset();
              }, 2000);
            } else {
              this.fpOtpSuccess = false;
              // this.fPOtpForm.reset();
              this.enterOTP = false;
              this.otpNotValied = true;
              this.isEmailVerified = false;
              setTimeout(() => {
                this.otpVerify = true;
                // this.fPOtpForm.reset();
              }, 2000);
              // this.fpOtpFailure = false;
            }

          },
          (error: any) => {
            console.log("Error", error);
            this.fpOtpSuccess = false;
            this.isEmailVerified = false;
            this.popupLoading =false;
            this.isSumitted = false;
            // this.fPOtpForm.reset();
            // this.toastr.error(error)
          }
        );
      }
    } else if (this.forOtpData == "mobile") {

      if (this.otpValue == null || this.otpValue == '') {
        this.otpNotValied = false;
        this.enterOTP = true;
      }
      else if (this.otpValue && this.otpValue.length == 6) {
        this.popupLoading =true
        this.otpNotValied = false;
        const formData = new FormData();
        formData.append("mobile", this.profileForm.value.countryCode + this.profileForm.value.userPhone);
        formData.append("otp", this.otpValue);

        this.http.post(this.api.VERIFYMOBILEOTP, formData).subscribe(
          (res: any) => {
            console.log(res)
            // this.fPOtpForm.reset();
            // debugger;
            let user = localStorage.getItem('user')||'';
            this.popupLoading =false
            this.isSumitted = false;
            if (this.EncrDecr.getCryptLibDecoding(res.data.verified_enc) == "allow") {
              this.enterOTP = false;
              this.otpVerify = false;
              this.fpOtpSuccess = true;
              this.isMobileVerified = true;
              let mobileVerify = Object.assign(this.userInfo, { otp_status: this.EncrDecr.setCryptLibEncode(JSON.parse(user).userId+"_allow") })
              this.util.updateProfile(mobileVerify)
              // this.commonData.callComponentMethod('');
              setTimeout(() => {
                this.modalService.dismissAll();
                // this.fPOtpForm.reset();
              }, 2000);
            } else {
              this.fpOtpSuccess = false;
              // this.fPOtpForm.reset();
              this.enterOTP = false;
              this.otpNotValied = true;
              this.isMobileVerified = false;
              setTimeout(() => {
                this.otpVerify = true;
                // this.fPOtpForm.reset();
              }, 2000);
              // this.fpOtpFailure = false;
            }

          },
          (error: any) => {
            console.log("Error", error);
            this.fpOtpSuccess = false;
            this.isMobileVerified = false;
            this.popupLoading =false
            this.isSumitted = false;
            // this.fPOtpForm.reset();
            // this.toastr.error(error)
          }
        );
      }

    }

  }
  updatePassword(content?) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  changePwd() {
    // debugger;
    this.isSumitted = true
    console.log(this.changePasswordForm.value)
    if (this.changePasswordForm.invalid) {
      return
    }
    this.isSumitted = false
    this.isClickedOnce = false;
    this.loading =true
    this.encoldPassword = this.EncrDecr.set(this.changePasswordForm.value.currentPassword);
    let encOldPwd = this.EncrDecr.getEncoding(this.encoldPassword);

    this.encNewPassword = this.EncrDecr.set(this.changePasswordForm.value.newPassword);
    let encNewPwd = this.EncrDecr.getEncoding(this.encNewPassword);

    this.enConPassword = this.EncrDecr.set(this.changePasswordForm.value.confirmPassword);
    let encCnPwd = this.EncrDecr.getEncoding(this.enConPassword);


    const formData = new FormData();
    formData.append("old_password", encOldPwd);
    formData.append("new_password", encNewPwd);
    formData.append("confirm_password", encCnPwd);

    this.http.post(this.api.PROFILECHANGEPASSWORD, formData).subscribe(
      (res: any) => {
        console.log(res)
        this.isClickedOnce = true;
        this.loading =false
        if (res.status == 400) {
          this.toastr.error(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
        } else if (res.status == 200) {
          // this.modalService.dismissAll();
          this.toastr.success(res.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true })
          
          // setTimeout(() => {
            this.modalService.dismissAll();
            //this.changePasswordForm.reset();
            // this.fPOtpForm.reset();
            this.logout()
          // }, 2000);
 
        }
      },
      (error: any) => {
        console.log("Error", error);
        this.isClickedOnce = true;
        this.loading =false
        this.changePasswordForm.reset();
      }
    );
  }
  selectEvent(item: any) {
    // alert(item)
    console.log('Employers', item.id);

    // do something with selected item
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e: any) {
    // do something
  }

  checkPasswords: ValidatorFn = (group): ValidationErrors | null => {
    let pass = '', confirmPass = '';
    if (this.changePasswordForm) {
      pass = this.changePasswordForm.controls['newPassword'].value;
      confirmPass = this.changePasswordForm.controls['confirmPassword'].value
    }
    if (confirmPass.length > 0)
      return pass === confirmPass ? null : { notSame: true }
    else return null
  }

  keyPressEmail(event) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9@._-]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  customFilter = function(allEmployers: any[], query: string): any[] {
    return allEmployers.filter(x => x.name.toLowerCase().startsWith(query.toLowerCase()));
  };
  // For Web Camera

  // toggle webcam on/off
  // public showWebcam = false;
  // public allowCameraSwitch = true;
  // public multipleWebcamsAvailable = false;
  // public deviceId: string;
  // public videoOptions: MediaTrackConstraints = {
  //   // width: {ideal: 1024},
  //   // height: {ideal: 576}
  // };
  // public errors: WebcamInitError[] = [];

  // // latest snapshot
  // public webcamImage?: WebcamImage;

  // // webcam snapshot trigger
  // private trigger: Subject<void> = new Subject<void>();
  // // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  // private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  // public triggerSnapshot(): void {
  //   this.trigger.next();
  // }

  // public toggleWebcam(): void {
  //   this.showWebcam = !this.showWebcam;
  // }

  // public handleInitError(error: WebcamInitError): void {
  //   this.errors.push(error);
  // }

  // public showNextWebcam(directionOrDeviceId: boolean | string): void {
  //   // true => move forward through devices
  //   // false => move backwards through devices
  //   // string => move to device with given deviceId
  //   this.nextWebcam.next(directionOrDeviceId);
  // }

  // public handleImage(webcamImage: WebcamImage): void {
  //   console.info('received webcam image', webcamImage);
  //   this.webcamImage = webcamImage;
  // }

  // public cameraWasSwitched(deviceId: string): void {
  //   console.log('active device: ' + deviceId);
  //   this.deviceId = deviceId;
  // }

  // public get triggerObservable(): Observable<void> {
  //   return this.trigger.asObservable();
  // }

  // public get nextWebcamObservable(): Observable<boolean | string> {
  //   return this.nextWebcam.asObservable();
  // }

}
