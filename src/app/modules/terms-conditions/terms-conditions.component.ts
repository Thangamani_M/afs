import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent implements OnInit {
  loading: boolean = true;
  descrption: any;

  constructor(private location: Location, private apihttp: ApiHttpService, private http: HttpClient, private api: ApiEndpointsService) {
    // try{
    //   window["$zoho"].salesiq.ready=function()
    //   {

    //     window["$zoho"].salesiq.floatbutton.visible("hide");
    //   }
    //   }catch(e){console.log(e);}
   }

  ngOnInit(): void {
  //  let zohostyle= document.getElementsByClassName('zsiq_floatmain')
  //  zohostyle[0]?.style.display = 'none'
  //  const elements = document.querySelectorAll('.zsiq_floatmain');
  //   if(elements.length){
  //     elements.forEach((item:any) => {
  //       item.style.visibility = 'hidden !important';
  //     })
  //   }
    // try{
    //   window["$zoho"].salesiq.ready=function()
    //   {
    //     window["$zoho"].salesiq.floatbutton.visible("hide");
    //   }
    //   }catch(e){
    //     console.log(e);
    //   }
    this.apihttp.get(this.api.TERMSANDCONDITIONS).subscribe(
      (res: any) => {
        this.loading = false;
        this.descrption = this.decodeEntities(res.data.InfoPageSettingValue);
        console.log("Data:", res.data);
      },
      (error: any) => {
        this.loading = false;
        console.log("Error", error);
      }
    );
    
  }

  goBack() {
    this.location.back()
  }

  decodeEntities(str: any) {
    const element = document.createElement('div');
    if (str && typeof str === 'string') {
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }
    return str;
  }
}
