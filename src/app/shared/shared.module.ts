import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';

import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { OverlayModule } from '@angular/cdk/overlay';

import { MatStepperModule } from '@angular/material/stepper';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { TabsModule } from 'ngx-bootstrap/tabs';
import {NgxMatTimepickerModule} from 'ngx-mat-timepicker';


import { MatRadioModule } from '@angular/material/radio';
//import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';


import { MatSliderModule } from '@angular/material/slider';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';
import { CommonService } from '../services/common/common.service';
import { DeletePopupComponent } from './components/delete-popup/delete-popup.component';

// const lang = "en-US";
let declarations = [
  // AuthLayoutComponent, 
  DeletePopupComponent
];

let modules = [
  RouterModule,
  ReactiveFormsModule,
  FormsModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSidenavModule,
  MatTableModule,
  MatSortModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatTabsModule,
  MatFormFieldModule,
  MatChipsModule,
  MatRippleModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatMenuModule,
  MatSlideToggleModule,
  OverlayModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  //NgDynamicBreadcrumbModule,
  TabsModule,
  // NgxMatTimepickerModule.setLocale("en-US"),
  NgxMatTimepickerModule,
  MatRadioModule,
  MatSliderModule
];

@NgModule({
  declarations: declarations,
  imports: [
    CommonModule,
    ...modules
  ],
  exports: [
    ...modules,
    ...declarations
  ],
  providers: [{ provide: LOCALE_ID, useValue: "en-US" }]
})
export class SharedModule { }

