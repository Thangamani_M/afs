import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MarketinginfoComponent } from 'src/app/modules/login/marketingInfo/marketinginfo.component';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ApiHttpService } from 'src/app/services/common/api-http.service';


// declare const encrypt: any;


@Component({
  selector: 'app-delete-popup',
  templateUrl: './delete-popup.component.html',
  styleUrls: ['./delete-popup.component.css']
})
export class DeletePopupComponent implements OnInit {
 


  constructor(private apihttp: ApiHttpService,private toastr: ToastrService, public activeModal: NgbActiveModal, private api: ApiEndpointsService, private modalService: NgbModal, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
 
  }
  done() {
    this.activeModal.close(1);
  }
 
  cancelpop() {
    this.modalService.dismissAll()
  }

  
}

