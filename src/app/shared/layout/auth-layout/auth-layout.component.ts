import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpService } from 'src/app/services/common/api-http.service';
import { ApiEndpointsService } from 'src/app/services/common/api-endpoints.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from '../../../services/common/utility.service';
import { ToastrService } from 'ngx-toastr';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { initializeApp } from "firebase/app";

import { getAnalytics, setUserId } from "firebase/analytics";
import { SocialAuthService } from 'angularx-social-login';
import { CommonService } from 'src/app/services/common/common.service';
import { UserIdleService } from 'angular-user-idle';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.css'],
  providers: [CommonService]
})
export class AuthLayoutComponent implements OnInit {
  userInfo: any;
  imagePath: any;
  getUserAllInfo: any;
  userAllInfo: any;
  notificationAlert: any;
  imageloader = false;
  dropdowndatas: any = [];

  constructor(private apihttp: ApiHttpService,private modalService: NgbModal, private userIdle: UserIdleService, private commonData: CommonService, private socialAuthService: SocialAuthService, private http: HttpClient, private api: ApiEndpointsService, private router: Router, private util: UtilityService, private toastr: ToastrService, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.commonData.customObservable.subscribe((res) => {
      this.getUserData()
    }
    );
    this.getUserData()
    this.getNotification();
    this.userIdle.startWatching();
    console.log('START', "count")
    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {

      console.log('count', count)
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      // localStorage.clear();
      // sessionStorage.clear();
      // this.router.navigateByUrl('login');
      this.modalService.dismissAll()
      this.logout()
      this.userIdle.resetTimer()
      this.userIdle.stopTimer();
      this.userIdle.stopWatching();
      console.log('Time is up!')
    });
    let masterdata = localStorage.getItem('masterdata')

    if (masterdata) {
      this.dropdowndatas = JSON.parse(atob(masterdata));
      // console.log(this.dropdowndatas)
    }
    
    this.http.get(this.api.MASTERDATA).subscribe(masterdata => {

      localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
      let masterd = localStorage.getItem('masterdata')
      if (masterd) {
        this.dropdowndatas = JSON.parse(atob(masterd));
        // console.log(this.dropdowndatas)
      }
      })
      this.http.get(this.api.MASTERDATA).subscribe(masterdata => {
  
        localStorage.setItem('masterdata', btoa(JSON.stringify(masterdata['data'])))
        let masterd = localStorage.getItem('masterdata')
        if (masterd) {
          this.dropdowndatas = JSON.parse(atob(masterd));
          // console.log(this.dropdowndatas)
        }
        })
  }
  getdropdownList(key) {
    let dropdownlist = []
    let filterdata = this.dropdowndatas.filter(item => item['key'] == key)
    if (filterdata.length > 0) {
      dropdownlist = filterdata[0]['value']
    }
    return dropdownlist
  }

  // ngOnChanges() {
  //   setTimeout(() => {
  //     localStorage.clear();
  //     sessionStorage.clear();
  //     this.router.navigateByUrl('login');
  //   }, 10000)
  // }
  getUserData() {
    let getUser = localStorage.getItem('userDetails');
    if (getUser) {
      this.userInfo = JSON.parse(getUser);
      // this.commonData.updateGlobalVar(this.userInfo)
    }
    const imageFormData = new FormData();
    if(this.userInfo.profile_url){
    imageFormData.append("id", this.userInfo.profile_url);
    this.imageloader = true
    this.http.post(this.api.IMAGEUPLOAD, imageFormData).subscribe(
      (res: any) => {
        // console.log(res)
        this.imageloader = false
        this.imagePath = res.data.path;
        // console.log('Image Path:', this.imagePath)

      },
      (error: any) => {
        this.imageloader = false
        console.log("Error", error);
      }
    );
    }
  }
  fetchRequestsStatus() {
    console.log("123")
  }

  logout() {
    let token =localStorage.getItem('device_token')
    if(!token){
      this.util.logoutAndNavigate();
      return
    }
    this.http.post(`${this.api.LOGOT}`,{device_token:token}).subscribe((data)=>{
      this.util.logoutAndNavigate();
    },error=>{
      this.util.logoutAndNavigate();
    })
    
  }
  activities() {
    this.router.navigateByUrl('activities');
  }
  notification() {
    // alert('')
    this.router.navigateByUrl('notifications');
    this.notificationAlert=0
  }
  editProfile() {
    this.router.navigateByUrl('profile');
  }
  getNotification() {
    const formData = new FormData();
    // imageFormData.append("id", this.userInfo.profile_url);
    this.http.post(this.api.BADGES, formData).subscribe(
      (res: any) => {
        this.notificationAlert = res.data.notification_count;
        // console.log('NC', this.notificationAlert)
        let passwordupdate = new Date(res.data.password_updatedby)
        let logindate = localStorage.getItem('logintime')
        if(logindate){
         let ltdate=JSON.parse(logindate)
           if(new Date(ltdate.date).getTime()<passwordupdate.getTime()){
             this.logout()
           }
        }
        
      },
      (error: any) => {
        console.log("Error", error);
      }
    );
  }
}
