import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ActivitiesComponent } from './modules/activities/activities.component';
import { CallbackComponent } from './modules/callback/callback.component';
import { ChatOrCallComponent } from './modules/chat-or-call/chat-or-call.component';
import { FaqComponent } from './modules/faq/faq.component';
import { FeedbackComponent } from './modules/feedback/feedback.component';
import { ForgotPasswordComponent } from './modules/forgot-password/forgot-password.component';
import { LoginComponent } from './modules/login/login.component';
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { OffersComponent } from './modules/offers/offers.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { ProfileComponent } from './modules/profile/profile.component';
import { SignUpComponent } from './modules/sign-up/sign-up.component';
import { TermsConditionsComponent } from './modules/terms-conditions/terms-conditions.component';
import { AuthGuard } from './services/auth/auth.guard';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
// import { PreLoaderComponent } from './shared/components/pre-loader/pre-loader.component';
import { AuthLayoutComponent } from './shared/layout/auth-layout/auth-layout.component';

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "sign-up", component: SignUpComponent },
  { path: "forgot-password", component: ForgotPasswordComponent },
  { path: 'terms-conditions', component: TermsConditionsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  {
    path: "", component: AuthLayoutComponent, canActivateChild: [AuthGuard], children: [

      {
        path: 'products', loadChildren: () => import('../app/modules/products/product.module').then(m => m.ProductModule), data: {
          breadcrumb: [
            { label: 'Products', url: 'products' }]
        }
      },
      {
        path: "my-loans", loadChildren: () => import('../app/modules/my-loans/myloan.module').then(m => m.MyloanModule), data: {
          breadcrumb: [
            { label: 'My Loans', url: 'my-loans' }]
        }
      },
      {
        path: "loan-calculator", loadChildren: () => import('../app/modules/loan-calculator/loan-calculator.module').then(m => m.LoanCalculatorModule), data: {
          breadcrumb: [
            { label: 'Loan calculator', url: 'loan-calculator' }]
        }
      },
      {
        path: "services", loadChildren: () => import('../app/modules/services/services.module').then(m => m.ServicesModule), data: {
          breadcrumb: [
            { label: 'Services', url: 'services' }]
        }
      },
      {
        path: "trackrequest", loadChildren: () => import('../app/modules/trackrequest/trackrequest.module').then(m => m.TrackRequestModule), data: {
          breadcrumb: [
            { label: 'Track Request/Enquiry', url: 'trackrequest' }]
        }
      },
      { path: "offers", component: OffersComponent },
      { path: "notifications", component: NotificationsComponent },
      { path: "activities", component: ActivitiesComponent },
      { path: "profile", component: ProfileComponent },
      { path: "feedback", component: FeedbackComponent },
      { path: "chat-or-call", component: ChatOrCallComponent },
      { path: "faq", component: FaqComponent },
      { path: 'callback', component: CallbackComponent },
      { path: '**', redirectTo: 'my-loans' },

      // { path: '404', component: LoginComponent },
      // { path: '**', redirectTo: '/404' }
    ]
  },

  { path: "login", component: LoginComponent },
  { path: "", redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login' }
  // { path: "page-not-found", component: PageNotFoundComponent },
  // { path: "", redirectTo: 'page-not-found', pathMatch: 'full' },
  // { path: '**', redirectTo: 'page-not-found' }
];


@NgModule({
  // imports: [RouterModule.forRoot(routes, { useHash: true })],
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    useHash: true,
    enableTracing: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
