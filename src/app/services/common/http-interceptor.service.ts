import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  constructor(public router: Router, private toastr: ToastrService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string | null = localStorage.getItem('accessTOKEN');
    // set custom headers for requests
    let headers: any = {};

    if (!(req.body instanceof FormData)) {
      headers['Accept'] = 'application/json',
        headers['Content-Type'] = 'application/json'
    }

    if (token) {
      headers["Authorization"] = token;
    }


    req = req.clone({
      headers: new HttpHeaders(headers)
    });

    // handle error responses
    return next.handle(req)
      .pipe(
        catchError((error: HttpErrorResponse) => {

          let err = {
            status: error?.status,
            message: error?.error?.message,
            errorResponse: error?.error,
          }

          if (error.status === 400) {

            if ((req.url).substr((req.url).lastIndexOf("/") + 1, req.url.length - 1) == 'track-request-enquiry'
                ||(req.url).substr((req.url).lastIndexOf("/") + 1, req.url.length - 1) == 'mastertabfields'
                ||(req.url).substr((req.url).lastIndexOf("/") + 1, req.url.length - 1) == 'validatetrn'
                ||(req.url).substr((req.url).lastIndexOf("/") + 1, req.url.length - 1) == 'place-autocomplete'
                ||(req.url).substr((req.url).lastIndexOf("/") + 1, req.url.length - 1) == 'place-details') {
              return throwError(err)
            }
            // console.log(Object.keys(error?.error?.error))
            if (this.isArray(error?.error?.error.ErrorHandle)) {
              return throwError(err);
            } else
              if (typeof (error?.error?.error) === 'object') {

                if (Object.keys(error?.error?.error).length > 0) {
                  let errorMessage = ''
                  Object.keys(error?.error?.error).map(function (key, index) {
                    errorMessage = errorMessage + error?.error?.error[key] + '<br>';
                  });

                  //err.message = err.message + '<br>' + errorMessage;
                  err.message = errorMessage;
                } else {
                  err.message = error?.error?.message;
                }
              } else if (typeof (error?.error?.error) === 'string') {
                //  err.message = err.message + '<br>' + error?.error?.error;
                err.message = error?.error?.error;
              } else {
                err.message = error?.error?.message;
              }

          } else
            if (error.status === 401) {
              err.message = "Your session has expired. Please login again to continue.";
              localStorage.clear();
              sessionStorage.clear();
              this.router.navigateByUrl('/login');
            } else if (error.status == 413) {
              // err.message = "Something went wrong, please try again";
              console.log("err.message", error)
              if(error?.error?.message)
                 err.message = error?.error?.message;
               else
                 err.message = 'Something went wrong, please try again';  
             // err.message = "File is too larges";
              // this.router.navigateByUrl('/login');
            } else if (error.status == 0) {
              // err.message = "Something went wrong, please try again";
              console.log("err.message", error)
              if(error?.error?.message)
                  err.message = error?.error?.message;
               else
                  err.message = 'Network error';  
              // this.router.navigateByUrl('/login');
            } else if (error.status == 500) {
              err.message = error?.error?.message;
              console.log("err.message", error)
            } else {
              if (!err.message) {
                err.message = 'Something went wrong, please try again.'
              }
            }
          this.toastr.error(err.message, '', { positionClass: 'toast-top-center', closeButton: true, enableHtml: true });
          // handle all error messagess 
          return throwError(err);
        })
      )
  }
  isArray(arr) {
    if (typeof arr === 'string') {
      return JSON.parse(arr) instanceof Array;
    } else {
      return arr instanceof Array;
    }
  }
}