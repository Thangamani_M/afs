import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpService {

  constructor(private http: HttpClient) { }

  generateQueryParams(queryParams: any) {
    const queryParamsString = new HttpParams({ fromObject: queryParams }).toString();
    return '?' + queryParamsString;
  }
  public get(url: string, qp = null, params?: any) {
    let queryParams;
    if (qp) {
      queryParams = this.generateQueryParams(qp);
    }
    if (params) {
      let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: params };
      return this.http.get(`${url}${(qp) ? queryParams : ''}`, options);
    }
    return this.http.get(`${url}${(qp) ? queryParams : ''}`);
  }
  public post(url: string, data: any, qp = null) {
    if (qp) {
      const queryParams = this.generateQueryParams(qp);
      return this.http.post(url + queryParams, data);
    }
    return this.http.post(url, data);
  }
  public postText(url: string, data: any, qp = null) {
    if (qp) {
      const queryParams = this.generateQueryParams(qp);
      return this.http.post(url + queryParams, data);
    }
    return this.http.post(url, data, { responseType: 'text' as const });
  }
  public put(url: string, data: any, qp = null, options?: any) {
    if (qp) {
      const queryParams = this.generateQueryParams(qp);
      return this.http.put(url + queryParams, data, options);
    }
    return this.http.put(url, data, options);
  }

  public delete(url: string, qp = null, body?: any) {
    let options: any;
    if (qp) {
      const queryParams = this.generateQueryParams(qp);
      return this.http.put(url + queryParams, options);
    }
    if (body) {
      options = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: body
      }
    }
    return this.http.delete(url, options);
  }
  public postUpload(url: any, fdata: any, qp = null) {
    let options = { headers: new HttpHeaders({}) };
    return this.http.post(url, fdata, options);
  }
}