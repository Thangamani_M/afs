import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilityService } from './utility.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	private requests: HttpRequest<any>[] = [];
	constructor(private util: UtilityService) { }
	removeRequest(req: HttpRequest<any>) {
		for (let i = 0; i < this.requests.length; i++) {
			const item = this.requests[i];
			if (item.url == req.url && item.method == req.method) {
				this.requests.splice(i, 1);
				break;
			}
		}
		this.util.isLoading.next(this.requests.length > 0);
	}
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		this.requests.push(request);
		this.util.isLoading.next(true);
		return new Observable(observer => {
			const subscription = next.handle(request)
				.subscribe(event => {
					if (event instanceof HttpResponse) {
						this.removeRequest(request);
						observer.next(event);
					}
				}, err => {
					this.removeRequest(request);
					observer.error(err);
				}, () => {
					this.removeRequest(request);
					observer.complete();
				});
			return () => {
				this.removeRequest(request);
				subscription.unsubscribe();
			};
		});
	}
}