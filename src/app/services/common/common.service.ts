import { Injectable } from '@angular/core';
import { Observable, Observer, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  userInfo: any;
  globalVar:string;
  globalVarUpdate:Observable<string>;
  globalVarObserver:Observer<string>;
  constructor() {
    let getUser = localStorage.getItem('userDetails');
    if (getUser) {
      this.userInfo = JSON.parse(getUser);
    }
    this.globalVarUpdate = Observable.create((observer:Observer<string>) => {
      this.globalVarObserver = observer;
    },error=>{
      console.log(error);
    });
    // console.log(this.userInfo);
  }

  updateGlobalVar(newValue:string) {
    this.userInfo = newValue
    this.globalVar = newValue;
    // this.globalVarObserver.next(this.globalVar);
  }
  getValue() {
    return this.userInfo ;
}

private customSubject = new Subject<any>();
customObservable = this.customSubject.asObservable();
// Service message commands
callComponentMethod(val) {
  this.customSubject.next(val);
}
}
