// import { NgxMatDateFormats } from '@angular-material-components/datetime-picker';
import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

import { SocialAuthService } from 'angularx-social-login';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { initializeApp } from 'firebase/app';

import { getAnalytics, setUserId, setUserProperties } from 'firebase/analytics';
import { environment } from 'src/environments/environment';
import { CommonService } from './common.service';
import { MarketinginfoComponent } from 'src/app/modules/login/marketingInfo/marketinginfo.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ApiEndpointsService } from './api-endpoints.service';
const app = initializeApp(environment.firebaseConfig);
const analytics = getAnalytics(app);
@Injectable({
  providedIn: 'root',
})
export class UtilityService {
  private profileObs$: BehaviorSubject<any> = new BehaviorSubject(null);
  public isLoading = new BehaviorSubject(false);
  public configObs$ = new Subject<any>();
  public tableRowSelected$ = new Subject<boolean>();
  public dataSync$ = new Subject<any>();
  public appConfigLoaded$ = new Subject<any>();
  private selectedSendgridData = new BehaviorSubject({});
  selectedSendgridInfo = this.selectedSendgridData.asObservable();
  private previousUrl;
  private currentUrl: string;
  constructor(
    private router: Router,
    private http: HttpClient,
    private api: ApiEndpointsService,
    private socialAuthService: SocialAuthService,
    private modalService: NgbModal,
    private commonData: CommonService
  ) {
    this.currentUrl = this.router.url;
    this.previousUrl = null;

    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event) => {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event['urlAfterRedirects'];
      });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }

  public tableRowSelected(val: boolean) {
    this.tableRowSelected$.next(val);
  }
  public configOperation(op: any) {
    this.configObs$.next(op);
  }
  public dataSync(val: any) {
    this.dataSync$.next(val);
  }
  public logoutAndNavigate() {
    if (localStorage.getItem('login')) {
      this.socialAuthService
        .signOut()
        .then((res) => {
          this.logoutUser();
        })
        .catch((err) => {
          this.logoutUser();
        });
    } else {
      this.logoutUser();
    }
  }
  public socicalAndNavigate() {
    this.router.navigate(['/login']);
  }
  logoutUser() {
    localStorage.clear();
    sessionStorage.clear();
    try {
      window['$zoho'].salesiq.ready = function () {
        window['$zoho'].salesiq.reset();
      };
    } catch (e) {
      console.log(e);
    }
    this.router.navigate(['/login']);
  }
  public getToken() {
    let token = localStorage.getItem('EmailVerification');
    if (localStorage.getItem('phonerefresh'))
          token = localStorage.getItem('PhoneVerification');
    if (!token) {
      token = '';
    }
    if (localStorage.getItem('phonerefresh') && token != '') {
      localStorage.removeItem('phonerefresh');
    }
    return token;
  }
  public clearToken() {
    localStorage.clear();
  }
  public getProfileObs(): Observable<any> {
    return this.profileObs$.asObservable();
  }
  public setProfileObs(profile: any) {
    this.profileObs$.next(profile);
  }
  public signIn(loginDetails: any, type: any) {
    let userDetails = {
      // timestamp: loginDetails.timestamp,
      // roleID: loginDetails.roleID,
      // roleName: loginDetails.roleName,
      userId: loginDetails.body.data.user.UserId,
      userName: loginDetails.body.data.user.FirstName,
      // accessToken:''
      // menuData: (menuData) ? menuData : '',
      // sessionTimeout: (menuData) ? loginDetails.sessionTimeout : ''
    };
    setUserId(analytics, loginDetails.body.data.user.UserId);
    //setUserProperties(analytics,{gender:''})
    //	localStorage.setItem('TOKEN', loginDetails.body.data.user.mail_token);
    localStorage.setItem('TOKEN', loginDetails.body.data.user.mail_token_status);
    localStorage.setItem(
      'EmailVerification',
      loginDetails.body.data.user.email_status
    );
    localStorage.setItem(
      'PhoneVerification',
      loginDetails.body.data.user.otp_status
    );
    localStorage.setItem('user_id', loginDetails.body.data.user.id);
    localStorage.setItem(
      'accessTOKEN',
      loginDetails.headers.get('Authorization')
    );
    localStorage.setItem('user', JSON.stringify(userDetails));
    localStorage.setItem(
      'userDetails',
      JSON.stringify(Object.assign(loginDetails.body.data.user))
    );
    // if (loginDetails.body.data.user.marketingflag == "Y") {
    // 	setTimeout(() => {
    // 		this.modalService.open(MarketinginfoComponent, { size: 'lg' })
    // 	}, 300);

    // }
    console.log("User",loginDetails.body.data.user)
    console.log("loginDetails",loginDetails.headers.get('Authorization'))
  }

  setMasterData() {
    this.http.get(this.api.MASTERDATA).subscribe((masterdata) => {
      localStorage.setItem(
        'masterdata',
        btoa(JSON.stringify(masterdata['data']))
      );
    });
  }

  public updateProfile(loginDetails: any) {
    localStorage.setItem('userDetails', JSON.stringify(loginDetails));
  }

  getDataKey(data: any) {
    for (const key in data) {
      if (typeof data[key] === 'object') {
        return key;
      }
    }
    return false;
  }
  public setFormArrayStorage(id, form) {
    localStorage.setItem('taparr' + id, form);
  }
  public getFormArrayStorage(id) {
    return localStorage.getItem('taparr' + id);
  }
  public removeFormArrayStorage(id) {
    localStorage.removeItem('taparr' + id);
  }

  public setFormGroupStorage(id, form) {
    localStorage.setItem('tapform' + id, form);
  }
  public getFormGroupStorage(id) {
    return localStorage.getItem('tapform' + id);
  }
  public removeFormGroupStorage(id) {
    localStorage.removeItem('tapform' + id);
  }

  // getPermission(menu: string){
  // 	let setMenu = this.getMenuName(menu);
  // 	let sdata = JSON.parse(localStorage.getItem('user'));
  // 	let access = { create: false, edit: false, delete: false, read: false }
  // 	if(sdata) {
  // 		const menuPermission = sdata?.menuData?.filter(val => val?.menuName?.toLowerCase() == setMenu?.toLowerCase());
  // 		if(menuPermission?.length > 0) {
  // 			access.create = menuPermission[0].create;
  // 			access.edit = menuPermission[0].edit;
  // 			access.delete = menuPermission[0].delete;
  // 			access.read = menuPermission[0].read
  // 		}
  // 	}
  // 	return access;
  // }

  // getMenuName(menu:any){
  // 	switch(menu?.toLowerCase()) {
  // 		case 'roles':
  // 			return 'roles';
  // 		case 'users':
  // 			return 'users';
  // 		case 'dashboard':
  // 			return 'dashboard';
  // 		case 'applications':
  // 			return 'applications';
  // 		case 'channels':
  // 			return 'channels';
  // 		case 'send grid':
  // 			return 'send grid';
  // 		case 'sms sender':
  // 			return 'sms sender';
  // 		case 'customer 360':
  // 			return 'customer 360';
  // 		case 'usergroup':
  // 			return 'usergroup';
  // 		case 'campaign':
  // 			return 'campaign';
  // 		case 'audit log':
  // 			return 'audit log';
  // 		default:
  // 			return menu?.toLowerCase();
  // 	}
  // }

  // get menuData() {
  // 	let menu = JSON.parse(localStorage.getItem('user'));
  // 	let menuDetails = [];
  // 	if(menu?.menuData) {
  // 		menuDetails = menu.menuData;
  // 	}
  // 	return menuDetails;
  // }
}

//server error
export function getServerError(form: FormGroup, err) {
  const validationErrors = err.errors;
  var errorMsg = {};
  Object.keys(validationErrors).forEach((prop) => {
    const formControl = form.get(validationErrors[prop].split(':')[0]);
    if (formControl) {
      if (errorMsg[`${validationErrors[prop].split(':')[0]}`]?.length > 0) {
        errorMsg[`${validationErrors[prop].split(':')[0]}`].push(
          validationErrors[prop].split(':')[1].trim()
        );
      } else {
        errorMsg[`${validationErrors[prop].split(':')[0]}`] = [
          validationErrors[prop].split(':')[1].trim(),
        ];
      }
      formControl.setErrors({
        serverError: errorMsg[`${validationErrors[prop].split(':')[0]}`],
      });
    }
  });
}

// export function autoCompleteValidation(): ValidatorFn {
// 	return (c: AbstractControl): { [key: string]: boolean } | null => {
// 		let selectboxValue = c.value;
// 		if (typeof selectboxValue === 'object' || !selectboxValue) {
// 			return null;
// 		} else {
// 			return { match: true };
// 		}
// 	};
// }
// export type FormGroupControls = { [key: string]: AbstractControl };

// export const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
// 	parse: {
// 	  dateInput: "l, LTS"
// 	},
// 	display: {
// 	  dateInput: "MM-DD-YYYY HH:mm:ss",
// 	  monthYearLabel: "MMM YYYY",
// 	  dateA11yLabel: "LL",
// 	  monthYearA11yLabel: "MMMM YYYY"
// 	}
// };
