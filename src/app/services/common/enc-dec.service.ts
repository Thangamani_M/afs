
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import * as crypto from 'crypto';
// import { keys } from 'lodash-es';
import cryptLib from '@skavinvarnan/cryptlib';

import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class EncrDecrService {
  constructor() { }
   ivStr = "BFpkOHMZ8n0kgccY" //this.randomString(16, environment.RANDOM)
  //The set method is use for encrypt the value.
  set(value: any) {
    var key = CryptoJS.enc.Utf8.parse(environment.LOGIN_KEY);
    var iv = CryptoJS.enc.Utf8.parse(this.ivStr);
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value), key,
      {
        keySize:  128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
    return encrypted.toString()
  }

  //The get method is use for decrypt the value.
  get(ivStr1 : string, value : string)  {
  const key = cryptLib.getHashSha256(environment.LOGIN_KEY, 32); 
      try {
        const decryptedString = cryptLib.decrypt(value,key, ivStr1);
        const str = decryptedString.split('_').length>1?decryptedString.split('_')[1]:decryptedString;
        if (str.length > 0) {
          console.log("ivSt^^",str,decryptedString.split('_')[0] )
          return str;
        } else {
          return 'error 1';
        } 
      } catch (e) {
        console.log(e)
        return 'error 2';
      }
   
  }


  getLogin(ivStr1 : string, value : string)  {
    const key = cryptLib.getHashSha256(environment.LOGIN_KEY, 32); 
     let user = localStorage.getItem('user');
        try {
          const decryptedString = cryptLib.decrypt(value,key, ivStr1);
          const str = decryptedString.split('_').length>1?decryptedString.split('_')[1]:decryptedString;
          if (user && str.length > 0 && decryptedString.split('_')[0] == JSON.parse(user).userId) {
            console.log("ivSt^^",str,decryptedString.split('_')[0] )
            return str;
          } else {
            return 'error 1';
          } 
        } catch (e) {
          console.log(e)
          return 'error 2';
        }
     
    }



  _arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }
  getEncoding(data) {
    let iv64 = btoa(this.ivStr)
    let conCodData = iv64 + data
    let mac = this.hmac(environment.LOGIN_KEY, conCodData)
    let jsonres = { iv: iv64, value: data, mac: mac }
    return btoa(JSON.stringify(jsonres))
  }
  hmac(key, data) {
    const bytes = CryptoJS.HmacSHA256(data, key);
    return bytes.toString(CryptoJS.enc.Hex);
  }

  getCryptLibDecoding(data,type?) {
       
    //return this.get("BFpkOHMZ8n0kgccY",data)
    try{
    if(data){
       let decriptdata = JSON.parse(atob(data))
       if(type && type =='1')
         return this.getLogin(decriptdata.iv,decriptdata.value)
        else 
         return this.get(decriptdata.iv,decriptdata.value)
    }
    else{
    return ""
    }
  }catch(e){
    return ""
  }
  }



  

  setCryptLibEncode(value : string)  {
     //let iv64 = atob(ivStr)
     const iv = "BFpkOHMZ8n0kgccY"; //16 bytes = 128 bit
     const key = cryptLib.getHashSha256(environment.LOGIN_KEY, 32); //32 bytes = 256 bits
     const cipherText = cryptLib.encrypt(value, key, iv);
     let encode = btoa(JSON.stringify({iv:iv,value:cipherText}))
     return encode
  }
getCDecoding(value){
    //console.log("getCDecoding",JSON.parse(atob(value)))
    console.log(this.get(JSON.parse(atob(value)).iv,JSON.parse(atob(value)).value))
}


  randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }
}