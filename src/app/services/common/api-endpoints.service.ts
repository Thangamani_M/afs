import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
// import {} from '../../shared/api/'

@Injectable({
	providedIn: 'root'
})
export class ApiEndpointsService {
	get getEnvironment() { return environment.apiURL; }
	public apiUrl = this.getEnvironment;
	// public readonly GETUSER: string = `${this.getEnvironment}/user/getusername`;
	// public readonly LOGIN: string = `${this.getEnvironment}/user/login`;
	public readonly LOGIN: string = `${this.getEnvironment}/user/newlogin`;
	public readonly REGISTER: string = `${this.getEnvironment}/user/register`;
	//public readonly VERYFYOTP: string = `${this.getEnvironment}/user/registerverify_otp`;
	public readonly VERYFYOTP: string = `${this.getEnvironment}/user/newregisterverify_otp`;
	// public readonly SOCIALREGISTER: string = `${this.getEnvironment}/user/socialregister`;
	public readonly SOCIALREGISTER: string = `${this.getEnvironment}/user/social_register`;
	public readonly FORGOTPASSWORD: string = `${this.getEnvironment}/user/forgot_password`;
	public readonly FORGOTVERIFYOTP: string = `${this.getEnvironment}/user/verify_otp`;
	public readonly RESENDOTP: string = `${this.getEnvironment}/user/resend-otp`;
	public readonly PROFILECHANGEPASSWORD: string = `${this.getEnvironment}/user/changepassword`;
	public readonly CHANGEPASSWORD: string = `${this.getEnvironment}/user/change_password`;
	public readonly TYPESOFLOANS: string = `${this.getEnvironment}/user/masterloantypes`;
	public readonly LOANMASTERTABS: string = `${this.getEnvironment}/user/mastertabfields`;
	public readonly CREATEFORMUI: string = `${this.getEnvironment}/user/loanform`;
	public readonly LOANDRAFTINSERT: string = `${this.getEnvironment}/user/loandraftinsert`;
	public readonly LOANDRAFTUPDATE: string = `${this.getEnvironment}/user/loandraftupdate`;
	public readonly LOANINSERTFINAL: string = `${this.getEnvironment}/user/loaninsert`;
	public readonly UPLOADFILE: string = `${this.getEnvironment}/user/uploadfile`;
	public readonly LOANDRAFTLIST: string = `${this.getEnvironment}/user/loandraftlist`;
	public readonly LOANDETAILS: string = `${this.getEnvironment}/user/loan-details`;
	public readonly EMISCHEDULELIST: string = `${this.getEnvironment}/user/loanschedulelist`;
	public readonly GETCARDDETAILSBYUSERID: string = `${this.getEnvironment}/user/get-card-detals-by-loan-id`;
	public readonly LOANPRECLOSURE: string = `${this.getEnvironment}/user/preclosure`;
	public readonly TRANSTACTIONS: string = `${this.getEnvironment}/user/get-transaction`;
	public readonly GETALLTRANSTACTIONS: string = `${this.getEnvironment}/user/get-all-transactions`;
	public readonly DOWNLOADRECEIPT: string = `${this.getEnvironment}/user/get-all-transactions-mail`;
	public readonly MAKEPAYMENT: string = `${this.getEnvironment}/user/createtransaction`;
	public readonly PRECLOSURESUCESS: string = `${this.getEnvironment}/user/preclosure-success`;
	public readonly DELETECARD: string = `${this.getEnvironment}/user/delete-card`;
	public readonly MASTERDATA: string = `${this.getEnvironment}/user/masterdata`;
	public readonly DEVICEUPDATE: string = `${this.getEnvironment}/user/deviceupdate`;
	public readonly PLACEAUTOCOMPLETE: string = `${this.getEnvironment}/user/place-autocomplete`;
	public readonly PLACEDETAILS: string = `${this.getEnvironment}/user/place-details`;
	// public readonly SOCIALLOGIN: string = `${this.getEnvironment}/user/socialregister`;
	public readonly SOCIALLOGIN: string = `${this.getEnvironment}/user/social_register`;
	public readonly IMAGEUPLOAD: string = `${this.getEnvironment}/user/fetchfile`;
	public readonly IMAGEMULTIUPLOAD: string = `${this.getEnvironment}/user/multifetchfile`;
	public readonly LOANCALCULATOR: string = `${this.getEnvironment}/user/loancalculator`;
	public readonly CALCULATORVALUE: string = `${this.getEnvironment}/user/calculatorvalue`;
	public readonly GETLOANSTATUS: string = `${this.getEnvironment}/user/get-loan-status-by-loan-id`;
	public readonly LISTLOANCOMMENTS: string = `${this.getEnvironment}/user/list-loan-comments`;
	public readonly LOANCOMMENTS: string = `${this.getEnvironment}/user/list-loan-comments-user-by-loanid`;
	public readonly ADDLOANCOMMENTS: string = `${this.getEnvironment}/user/add-loan-comments`;
	public readonly UPLOADCOMMENTIMAGE: string = `${this.getEnvironment}/user/upload-loan-comments-images`;
	public readonly ACTIVITIES: string = `${this.getEnvironment}/user/user-activity-lists`;
	public readonly NOTIFICATIONLISTS: string = `${this.getEnvironment}/user/user-notification-lists`;
	public readonly UPDATEPROFILE: string = `${this.getEnvironment}/user/profileupdate`;
	public readonly GETAPPOINTMENT: string = `${this.getEnvironment}/user/getappointmentlist`;
	public readonly INSERTAPPOINTMENT: string = `${this.getEnvironment}/user/insertappointment`;
	public readonly REPAYMENTMASTERDATA: string = `${this.getEnvironment}/user/repayment-masterdata`;
	public readonly REPAYMENTCURRENT: string = `${this.getEnvironment}/user/repayment-current-value`;
	public readonly REPAYMENTINSERT: string = `${this.getEnvironment}/user/repayment-insert`;
	public readonly UPDATEAPPOINTMENT: string = `${this.getEnvironment}/user/updateappointment`;
	public readonly BILLINGINSERT: string = `${this.getEnvironment}/user/billing-insert`;
	public readonly ENQUIRYINSERT: string = `${this.getEnvironment}/user/enquiry-insert`;
	public readonly GETTRACKREQUESTENQUIRY: string = `${this.getEnvironment}/user/track-request-enquiry`;
	public readonly INSERTFEEDBACK: string = `${this.getEnvironment}/user/feedback-insert`;
	public readonly GETMOBILEOTP: string = `${this.getEnvironment}/user/send-mobile-otp`;
	public readonly VERIFYMOBILEOTP: string = `${this.getEnvironment}/user/verify_mobileotp`;
	public readonly FAQLIST: string = `${this.getEnvironment}/user/faq-list`;
	public readonly TNRVALID: string = `${this.getEnvironment}/user/validatetrn`;
	public readonly MARKETINGINFO: string = `${this.getEnvironment}/user/add-marketing-user-answers`;
	public readonly BADGES: string = `${this.getEnvironment}/user/get-notification-count`;
	public readonly ALLOFFERS: string = `${this.getEnvironment}/user/promotions-list`;
	public readonly PRECLOSUREAMOUNT: string = `${this.getEnvironment}/user/pre-closure`;
	public readonly PRECLOSUREINSERT: string = `${this.getEnvironment}/user/preclosure-insert`;
	public readonly MYPROFILE: string = `${this.getEnvironment}/user/my_profile`;
	public readonly SOCIALPROFILE: string = `${this.getEnvironment}/user/socialprofileupdate`;
	public readonly LIVENESSUPDATE: string = `${this.getEnvironment}/user/live-update`;
	public readonly UPDATETRN: string = `${this.getEnvironment}/user/update-trn`;
	public readonly PRIVACYPOLICY: string = `${this.getEnvironment}/user/get-privacy-policy`;
	public readonly LOGOT: string = `${this.getEnvironment}/user/logout`;
	public readonly MARKETINGDATA: string = `${this.getEnvironment}/user/get-marketing-questions`;
	public readonly TERMSANDCONDITIONS: string = `${this.getEnvironment}/user/get-terms-conditions`;
	public readonly USERUPDATEEMPLOYER: string = `${this.getEnvironment}/user/user-update-employer`;
	public readonly LOANDELETE: string = `${this.getEnvironment}/user/loan-delete`;
	public readonly COUNTRYDATAS: string = `assets/api/country-code.json`;
}
