import { AbstractControl, FormControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

@Directive({
  selector: '[FileTypeValidator]',
  providers: [
    {
      provide: NG_VALIDATORS, useExisting: FileTypeValidator, multi: true
    }
  ]
})
export class FileTypeValidator implements Validator {

  static validate(c: AbstractControl): any {
    if (c.value) {
      if (c.value[0]) {
        return FileTypeValidator.checkExtension(c);
      };
    }
  }

  private static checkExtension(c: AbstractControl) {
    let valToLower = c.value.toLowerCase();
    if (valToLower.split('.').length > 1) {

      let regex = new RegExp("(.*?)\.(jpg|png|jpeg|pdf)$"); //add or remove required extensions here
      // let regex = new RegExp("(.*?)\.(jpg|png|jpeg)$"); //add or remove required extensions here

      let regexTest = regex.test(valToLower);
      console.log(valToLower, regexTest)
      return !regexTest ? { "notSupportedFileType": true } : null;
    }
    return null;
  }

  validate(c: AbstractControl): { [key: string]: any } {
    return FileTypeValidator.validate(c);
  }

  static validateImage(c: AbstractControl): any {
    if (c.value) {
      if (c.value[0]) {
        return FileTypeValidator.checkImageExtension(c);
      };
    }
  }

  private static checkImageExtension(c: AbstractControl) {
    // debugger;
    let valToLower = c.value.toLowerCase();
    if (valToLower.split('.').length > 1) {
      let regex = new RegExp("(.*?)\.(jpg|png|jpeg)$"); //add or remove required extensions here

      let regexTest = regex.test(valToLower);
      console.log(valToLower, regexTest)
      return !regexTest ? { "notSupportedFileType": true } : null;
    }
    return null;
  }

  validateImage(c: AbstractControl): { [key: string]: any } {
    return FileTypeValidator.validateImage(c);
  }
}