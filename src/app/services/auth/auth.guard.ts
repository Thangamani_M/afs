import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EncrDecrService } from '../common/enc-dec.service';
import { UtilityService } from '../common/utility.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(private util: UtilityService, private router: Router,private EncrDecr: EncrDecrService,) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		if (this.EncrDecr.getCryptLibDecoding(this.util.getToken()) == "allow" ) {
			console.log("first")
			return true
		} else {
			console.log("second22")
			this.util.logoutAndNavigate();
			return false;
		}
	}
	canActivateChild(next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		if (this.EncrDecr.getCryptLibDecoding(this.util.getToken(),'1') == "allow") {
			let user = localStorage.getItem("userDetails");
			if (user) {
				console.log(JSON.parse(user))
				let checkTrn = JSON.parse(user)

				if (!checkTrn.DateOfBirth || checkTrn.DateOfBirth == "" || !checkTrn.trn_number || checkTrn.trn_number == "") {
					this.util.socicalAndNavigate()
					return false
				}
			}
			return true
		} else {
			console.log("second21")
			localStorage.clear();
			sessionStorage.clear()
			this.util.logoutAndNavigate();
			return false;
		}
	}

}
