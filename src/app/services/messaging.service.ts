import { Injectable, NgZone } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { BehaviorSubject, Observable } from 'rxjs'
import { getMessaging, onMessage } from "firebase/messaging";
import { Router } from '@angular/router';

// const messaging = getMessaging();
// onMessage(messaging, (payload) => {
//   console.log('Message received. ', payload);
//   // ...
// });
@Injectable()
export class MessagingService {
currentMessage = new BehaviorSubject(null);
messages$:Observable<any>;

constructor(private angularFireMessaging: AngularFireMessaging,private router: Router, private zone: NgZone) {
  try {
this.angularFireMessaging.messages.subscribe(
  (_messaging:any) => {
    console.log(_messaging)
  //_messaging.onMessage = _messaging.onMessage.bind(_messaging);
  _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
},error=>{
  console.log("error",error)
})
// navigator.serviceWorker.addEventListener('message',this.onReceiveMessage.bind(this))
} catch (err) {
  console.log(err)
}
}
  requestPermission() {
    let self =this
    try {

      Notification.requestPermission().then(function (status) {
        if (status === 'denied') {
            //
        } else if (status === 'granted') {
            
          self.angularFireMessaging.requestToken.subscribe(
            (token) => {
              console.log(token);
              if (token)
                localStorage.setItem('device_token', token)
              else
                localStorage.setItem('device_token', '')
            },
            (err) => {
              console.log('Unable to get permission to notify.', err);
            }
          );
        }
      })
   
    } catch (err) {
      console.log(err)
    }
  }

  onReceiveMessage(event) {
    console.log(event)
  }
  receiveMessage() {
    try {
      this.angularFireMessaging.messages.subscribe(
        (payload: any) => {
          console.log("new message received. ", payload);
          this.currentMessage.next(payload);
          //this.router.navigate(['/notifications']);
        }, (err) => {
          console.log("new message error. ", err);
        })
    } catch (err) {
      console.log(err)
    }
  }
}