import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { UtilityService } from './services/common/utility.service';
import { initializeApp } from "firebase/app";
import { environment } from 'src/environments/environment';
import { getAnalytics } from "firebase/analytics";
import { MessagingService } from './services/messaging.service';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { Router } from '@angular/router';
const app = initializeApp(environment.firebaseConfig);
const analytics = getAnalytics(app);
// var messaging = app.messaging();
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'AFS-web';
  isLoading: boolean = false;
  loader: Subscription;
  message: any;
  @ViewChild(BsDatepickerDirective, { static: false })
  datepicker: BsDatepickerDirective;
  
  constructor(private util: UtilityService,private messagingService: MessagingService,private router: Router, private zone: NgZone){
    this.loader = this.util.isLoading.subscribe((val) => {
      setTimeout(() => { this.isLoading = val; }, 0);
    });
    window.addEventListener('scroll', this.hideDialog, true);
  //  let notification:any = new Notification('', {body: 'test'});
  //   notification.onclick = () => {
  //     this.zone.run(() => {
  //         console.log('onclick');
  //         this.router.navigate(['/notifications']);
  //     });
  // };
  }
  
  hideDialog(): void {
    const d = document.querySelector('bs-datepicker-container');
    if (d) {
      this.datepicker?.hide();
    }
  }
 async ngOnInit() {
    await this.messagingService.requestPermission()
     this.messagingService.receiveMessage()
   //this.message = this.messagingService.currentMessage
   
  }


  ngOnDestroy(){
    this.loader.unsubscribe();
  }
}
